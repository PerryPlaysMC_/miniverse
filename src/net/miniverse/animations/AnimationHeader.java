package net.miniverse.animations;

import net.miniverse.util.animate.Animation;

public class AnimationHeader extends Animation {

    public AnimationHeader(double speed) {
        super(speed,
                "[pc]C[c][c]heatCode SMP",
                "[c]C[pc]h[c]eatCode SMP",
                "[c]Ch[pc]e[c]atCode SMP",
                "[c]Che[pc]a[c]tCode SMP",
                "[c]Chea[pc]t[c]Code SMP",
                "[c]Cheat[pc]C[c]ode SMP",
                "[c]CheatC[pc]o[c]de SMP",
                "[c]CheatCo[pc]d[c]e SMP",
                "[c]CheatCod[pc]e[c] SMP",
                "[c]CheatCode [pc]S[c]MP",
                "[c]CheatCode S[pc]M[c]P",
                "[c]CheatCode SM[pc]P[c]",
                "[pc]CheatCode SMP",
                "[c]CheatCode SMP" ,
                "[pc]CheatCode SMP",
                "[c]CheatCode SMP" ,
                "[pc]CheatCode SMP",
                "[c]CheatCode SMP" ,
                "[pc]CheatCode SMP",
                "[c]CheatCode SMP" ,
                "[pc]CheatCode SMP",
                "[c]CheatCode SMP" ,
                "[c]CheatCode S[pc]M[c]P",
                "[c]CheatCode [pc]S[c]MP",
                "[c]CheatCod[pc]e[c] SMP",
                "[c]CheatCo[pc]d[c]e SMP",
                "[c]CheatC[pc]o[c]de SMP",
                "[c]Cheat[pc]C[c]ode SMP",
                "[c]Chea[pc]t[c]Code SMP",
                "[c]Che[pc]a[c]tCode SMP",
                "[c]Ch[pc]e[c]atCode SMP",
                "[c]C[pc]h[c]eatCode SMP",
                "[pc]C[c][c]heatCode SMP"
        );
    }
}