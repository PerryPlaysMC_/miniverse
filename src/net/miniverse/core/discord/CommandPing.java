package net.miniverse.core.discord;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

public class CommandPing extends net.miniverse.core.discord.command.DiscCommand
{
    public CommandPing()
    {
        super("ping");
    }

    public void run(GuildMessageReceivedEvent e, TextChannel c, Message msg, User s, String cl, String[] args) {
        sendSuccess("Pong!", "Ping: " + s.getJDA().getPing());
    }
}
