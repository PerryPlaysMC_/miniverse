package net.miniverse.core.discord;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.utils.PermissionUtil;
import net.miniverse.core.discord.command.DiscCommand;

public class CommandClearChat extends DiscCommand {
    public CommandClearChat()
    {
        super("clearchat");
    }

    public void run(GuildMessageReceivedEvent e, TextChannel c, Message msg, User s, String cl, String[] args) {
        if (!s.getJDA().getRoles().contains(DiscordUtils.getRole(c.getGuild(),"moderator")))
            if (!PermissionUtil.checkPermission(c, c.getGuild().getMember(s), Permission.ADMINISTRATOR)) {
                sendFail("You do not have permission for this command.");
                return;
            }
        if (args.length == 0) {
            sendError("Please specify a channel!");
            return;
        }
        if (args.length >= 2) {
            sendError("Too many arguments!");
            return;
        }
        TextChannel ca = DiscordUtils.getChannel(e.getGuild(), args[0]);
        if (ca == null) {
            sendError("Invalid channel"); return;
        }
        for(Message ms : ca.getHistory().retrievePast(100).complete())ms.delete().queue();
        DiscordUtils.sendSuccess(ca, "Cleared chat", "By: " + s.getAsMention());
    }
}
