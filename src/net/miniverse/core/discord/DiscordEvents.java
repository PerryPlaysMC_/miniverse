package net.miniverse.core.discord;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.managers.GuildController;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.discord.command.DiscCommandHandler;
import net.miniverse.core.events.user.UserChatEvent;
import net.miniverse.core.events.user.UserJoinEvent;
import net.miniverse.core.events.user.UserQuitEvent;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

public class DiscordEvents extends ListenerAdapter implements Listener {

    private JDA jda = MiniverseBot.getBot();
    List<User> users = new ArrayList<>();
    List<User> users2 = new ArrayList<>();
    List<OfflineUser> users3 = new ArrayList<>();


    @EventHandler(priority = EventPriority.LOWEST)
    void onChat(UserChatEvent e) {
        if(e.isCancelled())return;
        if(!e.isCancelled()) {
            if(users2.contains(e.getUser())) return;
            else users2.add(e.getUser());
            if(e.getMessage().equalsIgnoreCase("!list")) {
                List<Member> names = DiscordUtils.getMembers(DiscordUtils.getGuild("CheatCode SMP"));
                StringBuilder online = new StringBuilder();
                StringBuilder offline = new StringBuilder();
                StringBuilder other = new StringBuilder();
                for(int i = 0; i < names.size(); i++) {
                    Member m = names.get(i);
                    if(m.getOnlineStatus() == OnlineStatus.ONLINE) {
                        String a = m.getEffectiveName() + "-[c]&l" + m.getOnlineStatus();
                        if(i == names.size()+-1) {
                            online.append("[pc]").append(a);
                            continue;
                        }
                        online.append("[pc]").append(a).append("[c],").append(" ");
                        continue;
                    }
                    if(m.getOnlineStatus() == OnlineStatus.OFFLINE) {
                        String a = m.getEffectiveName() + "-&4&l" + m.getOnlineStatus();
                        if(i == names.size()+-1) {
                            offline.append("[pc]").append(a);
                            continue;
                        }
                        offline.append("[pc]").append(a).append("[c],").append(" ");
                        continue;
                    }
                    String a = m.getEffectiveName() + "-&e&l" + m.getOnlineStatus();
                    if(i == names.size()+-1) {
                        other.append("[pc]").append(a);
                        continue;
                    }
                    other.append("[pc]").append(a).append("[c],").append(" ");
                }
                if(!online.toString().isEmpty() && !online.toString().trim().isEmpty()) {
                    e.getUser().sendMessage("[c]Online Discord Users: ", online.toString().trim());
                }
                if(!offline.toString().isEmpty() && !offline.toString().trim().isEmpty()) {
                    e.getUser().sendMessage("[c]Offline Discord Users: ", offline.toString().trim());
                }
                if(!other.toString().isEmpty() && !other.toString().trim().isEmpty()) {
                    e.getUser().sendMessage("[c]Other Discord Users: ", other.toString().trim());
                }
                e.setCancelled(true);
                return;
            }
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MiniverseCore.getAPI(), () -> { users2.remove(e.getUser()); }, 1);
            TextChannel channel = DiscordUtils.getChannel("minecraft");
            if(channel == null) return;
            DiscordUtils.sendMinecraftFormatted(channel, "**"+e.getUser().getPrefix() + " " + e.getUser().getName() + ": **", e.getMessage());
            MiniverseBot.log(LogType.DEBUG, "Sending message to #minecraft-(Chat) " + e.getUser().getName() + " " + e.getMessage());
        }
    }
    @EventHandler
    void onJoin(UserJoinEvent e) {
        if(users.contains(e.getUser())) return;
        else users.add(e.getUser());
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MiniverseCore.getAPI(), () -> { users.remove(e.getUser()); }, 2);
        TextChannel channel = DiscordUtils.getChannel("minecraft");
        if(channel == null) return;
        channel.sendMessage(Miniverse.format("Events.Discord.MinecraftJoin", e.getUser().getName(), StringUtils.stripColor(e.getUser().getPrefix()),
                StringUtils.stripColor( e.getMessage()))).queue();
        MiniverseBot.log(LogType.DEBUG, "Sending message to #minecraft-(Join) " + e.getUser().getName() + " " + e.getMessage());
    }
    @EventHandler
    void onQuit(UserQuitEvent e) {
        if(users3.contains(e.getUser())) return;
        else users3.add(e.getUser());
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MiniverseCore.getAPI(), () -> { users3.remove(e.getUser()); }, 2);
        TextChannel channel = DiscordUtils.getChannel("minecraft");
        if(channel == null) return;
        channel.sendMessage(Miniverse.format("Events.Discord.MinecraftQuit", e.getUser().getName(), StringUtils.stripColor(e.getUser().getPrefix()),
                StringUtils.stripColor( e.getMessage()))).queue();
        MiniverseBot.log(LogType.INFO, "Sending message to #minecraft-(Quit) " + e.getMessage());
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent e) {
        TextChannel c = DiscordUtils.getChannel("welcome");
        if(c==null)return;
        c.sendMessage("Welcome **" + e.getUser().getAsMention() + "** to the CheatCode SMP Discord Server!").queue();
        GuildController g = new GuildController(e.getGuild());
        if(!e.getMember().getRoles().contains(DiscordUtils.getRole(e.getGuild(), "SMP"))) {
            g.addSingleRoleToMember(e.getMember(), DiscordUtils.getRole(e.getGuild(), "SMP")).queue();
        }
    }


    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(e.getAuthor().isBot() || e.getAuthor().isFake() || e.getMessage().getContentRaw().isEmpty()) return;
        if(!e.getChannel().getName().equalsIgnoreCase("minecraft")) return;
        String prefix = Miniverse.getSettings().isSet("Discord.prefix") ? Miniverse.getSettings().getString("Discord.prefix") : "!";
        if(DiscCommandHandler.getCommand(e.getMessage().getContentRaw().split(" ")[0].replace(prefix, "")) != null) return;
        if(e.getMessage().getContentRaw().equalsIgnoreCase("/list")) {
            List<String> names = new ArrayList<>();
            for(User u : Miniverse.getOnlineUsers()) {
                if(!names.contains(u.getName()) && !names.contains(u.getName()+"-Afk")) {
                    if(u.isAFK()) names.add(u.getName()+"-Afk");
                    else names.add(u.getName());
                }
            }
            StringBuilder sb = new StringBuilder("Online users:\n");
            for(int i = 0; i < names.size(); i++) {
                String a = names.get(i);
                if(i == names.size()+-1) {
                    sb.append(a);
                    continue;
                }
                sb.append(a).append(",").append(" ");
            }
            e.getChannel().sendMessage(sb.toString().trim()).queue();
            e.getMessage().delete().queue();
            return;
        }
        Miniverse.broadCast(Miniverse.format("Events.Discord.DiscordToMinecraft", e.getMessage().getAuthor().getName(), e.getMessage().getContentRaw()));
        MiniverseBot.log(LogType.DEBUG, "Sending message to Minecraft " + e.getMessage().getAuthor().getName() + " " + e.getMessage().getContentRaw());
    }
}
