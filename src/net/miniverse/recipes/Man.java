package net.miniverse.recipes;

import net.miniverse.core.MiniverseCore;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class Man {

    protected static List<MiniRecipe> recipeList;

    static {
        if(recipeList == null) {
            recipeList = new ArrayList<>();
        }
    }

   public static Recipe createRecipe(ItemStack result, ItemStack... ingredients) {
       MiniRecipe recipe = new MiniRecipe(result);
       for (ItemStack i : ingredients) {
           recipe.addItem(i);
       }
       recipe.addRecipe(recipe.getRecipe());
       return recipe.getRecipe();
   }

    public static Recipe createFurnaceRecipe(ItemStack result, ItemStack ingredient) {
        //FurnaceRecipe(NamespacedKey key, ItemStack result, Material source, float experience, int cookingTime)
        FurnaceRecipe r = new FurnaceRecipe(result, ingredient.getData());
        r.setCookingTime(100);
        r.setExperience(20);
        Bukkit.addRecipe(r);
        return r;
    }

    private static MiniRecipe getR(UUID uuid) {
        for(MiniRecipe r : getRecipes()) {
            if(r.getKey().getKey().equalsIgnoreCase(uuid.toString())) return r;
        }
        return null;
    }

    public static Recipe createRecipe(ItemStack result, Material... ingredients) {
        UUID uuid = UUID.randomUUID();
        while(getR(uuid)!=null) uuid = UUID.randomUUID();
        MiniRecipe recipe = new MiniRecipe(new NamespacedKey(MiniverseCore.getAPI(), uuid + ""), result);
        for (Material i : ingredients) {
            recipe.addItem(i);
        }
        recipe.addRecipe(recipe.getRecipe());
        return recipe.getRecipe();
    }

    public static Recipe createRecipe(String key, ItemStack result, Material... ingredients) {
        UUID uuid = UUID.randomUUID();
        while(getR(uuid)!=null) uuid = UUID.randomUUID();
        MiniRecipe recipe = new MiniRecipe(new NamespacedKey(MiniverseCore.getAPI(), key), result);
        for (Material i : ingredients) {
            recipe.addItem(i);
        }
        recipe.addRecipe(recipe.getRecipe());
        return recipe.getRecipe();
    }
    public static Recipe createRecipe(String key, ItemStack result, ItemStack... ingredients) {
        UUID uuid = UUID.randomUUID();
        while(getR(uuid)!=null) uuid = UUID.randomUUID();
        MiniRecipe recipe = new MiniRecipe(new NamespacedKey(MiniverseCore.getAPI(), key), result);
        for (ItemStack i : ingredients) {
            recipe.addItem(i);
        }
        recipe.addRecipe(recipe.getRecipe());
        return recipe.getRecipe();
    }


    public static List<MiniRecipe> getRecipes() {
        return recipeList;
    }

    public static void add(MiniRecipe m) {
        if(!recipeList.contains(m)) {
            recipeList.add(m);
        }
    }



}
