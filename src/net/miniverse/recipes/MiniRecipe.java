//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)p
package net.miniverse.recipes;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_14_R1.inventory.*;
import org.bukkit.inventory.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class MiniRecipe {
    private ShapelessRecipe recipe;
    private final NamespacedKey key;
    private final ItemStack output;
    private final List<ItemStack> ingredients = new ArrayList();

    public MiniRecipe(ItemStack result) {
        this.key = new NamespacedKey("miniverse", UUID.randomUUID()+"");
        this.output = new ItemStack(result);
        recipe = new ShapelessRecipe(key, output);
    }

    public MiniRecipe(NamespacedKey key, ItemStack result) {
        this.key = key;
        this.output = new ItemStack(result);
        recipe = new ShapelessRecipe(key, output);
    }

    public ShapelessRecipe getRecipe() {
        return recipe;
    }




    public boolean addRecipe(Recipe recipe) {
        Object toAdd;
        if (recipe instanceof CraftRecipe) {
            toAdd = recipe;
        } else if (recipe instanceof ShapedRecipe) {
            toAdd = CraftShapedRecipe.fromBukkitRecipe((ShapedRecipe)recipe);
        } else if (recipe instanceof ShapelessRecipe) {
            toAdd = CraftShapelessRecipe.fromBukkitRecipe((ShapelessRecipe) recipe);
        }else if(recipe instanceof  MiniRecipe) {
            toAdd = CraftShapelessRecipe.fromBukkitRecipe(this.recipe);

        } else {
            if (!(recipe instanceof FurnaceRecipe)) {
                return false;
            }

            toAdd = CraftFurnaceRecipe.fromBukkitRecipe((FurnaceRecipe)recipe);
        }

        ((CraftRecipe)toAdd).addToCraftingManager();
        return true;
    }

    public MiniRecipe addItem(Material item) {
        ingredients.add(new ItemStack(item));
        recipe.addIngredient(item);
        return this;
    }

    public MiniRecipe addItem(ItemStack item) {
        ingredients.add(item);
        recipe.addIngredient(item.getData());
        return this;
    }


    public ItemStack getResult() {
        return this.output.clone();
    }

    public List<ItemStack> getIngredientList() {
        ArrayList<ItemStack> result = new ArrayList<>(this.ingredients.size());
        Iterator var3 = this.ingredients.iterator();

        while(var3.hasNext()) {
            ItemStack ingredient = (ItemStack)var3.next();
            result.add(ingredient);
        }

        return result;
    }

    public NamespacedKey getKey() {
        return this.key;
    }
}
