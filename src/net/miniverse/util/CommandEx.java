package net.miniverse.util;

import com.mojang.brigadier.ImmutableStringReader;
import com.mojang.brigadier.Message;
import com.mojang.brigadier.exceptions.CommandExceptionType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

public class CommandEx implements CommandExceptionType {

    private final Message message;

    public CommandEx(String MSG) {
        this.message = () -> MSG;
    }

    public CommandSyntaxException create() {
        return new CommandSyntaxException(this, this.message);
    }

    public CommandSyntaxException createWithContext(ImmutableStringReader var1) {
        return new CommandSyntaxException(this, this.message, var1.getString(), var1.getCursor());
    }

    public String toString() {
        return this.message.getString();
    }


}
