package net.miniverse.util;

import net.miniverse.MiniversePlugin;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.scheduler.BukkitRunnable;

public class Run extends BukkitRunnable {

    TNTPrimed tnt;

    public Run(TNTPrimed tnt, int i) {
        this.tnt = tnt;
        runTaskTimer(MiniversePlugin.getInstance(), 0, i);
    }



    @Override
    public void run() {
        if(tnt.isOnGround()) {
            tnt.setFuseTicks(1);
            //tnt.remove();
            cancel();
        }
    }
}
