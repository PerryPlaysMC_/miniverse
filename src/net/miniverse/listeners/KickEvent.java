package net.miniverse.listeners;

import net.miniverse.core.events.user.UserKickEvent;

public class KickEvent implements net.miniverse.util.Listener
{
  public KickEvent() {}
  
  @org.bukkit.event.EventHandler
  void onKick(UserKickEvent e) {
    e.setCancelled(false);
  }
}
