package net.miniverse.listeners.signshop;

import net.miniverse.chat.Message;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.gui.shops.ItemValues;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.user.User;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.text.DecimalFormat;
import java.util.HashMap;

@SuppressWarnings("all")
public class SellWandEvents implements Listener {


    @EventHandler
    void onSellWand(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK && (e.getItem() != null && e.getItem().getType() == Material.DIAMOND_HOE)) {
            if(e.getClickedBlock().getState() instanceof Container) {
                if(e.getPlayer().isSneaking()) {
                    double amt = 0;
                    int items = 0;
                    User u = Miniverse.getUser(e.getPlayer());
                    HashMap<Material, Integer> values = new HashMap<>();
                    HashMap<String, Integer> value = new HashMap<>();

                    for(Block b : Miniverse.getNearbyContainers(-1, e.getClickedBlock())) {
                        Container c = (Container) b.getState();
                        (new BukkitRunnable() {
                            @Override
                            public void run() {
                                u.getBase().closeInventory();
                            }
                        }).runTaskLater(MiniverseCore.getAPI(), 1);
                        e.setCancelled(true);
                        int hr = 0, ct = 0, br = 0;
                        if(c instanceof org.bukkit.block.Chest) {
                            Chest t = (Chest) c;
                            InventoryHolder holder = t.getInventory().getHolder();
                            if(holder instanceof DoubleChest) {
                                DoubleChest doubleChest = ((DoubleChest) holder);
                                Chest c1 = (Chest) doubleChest.getRightSide();
                                Chest c2 = (Chest) doubleChest.getLeftSide();
                                boolean hasSold = false;
                                for(int z = 0; z < c1.getSnapshotInventory().getContents().length; z++) {
                                    ItemStack i = c1.getSnapshotInventory().getItem(z);
                                    if(i == null || i.getType().name().contains("AIR")) continue;
                                    ItemValues v = MiniShopHandler.getValue(i);
                                    if(v != null) {
                                        hasSold = true;
                                        amt+=v.getSell()*i.getAmount();
                                        items+=i.getAmount();
                                        values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                        c1.getSnapshotInventory().setItem(z, null);
                                    }
                                }
                                for(int z = 0; z < c2.getSnapshotInventory().getContents().length; z++) {
                                    ItemStack i = c2.getSnapshotInventory().getItem(z);
                                    if(i == null || i.getType().name().contains("AIR")) continue;
                                    ItemValues v = MiniShopHandler.getValue(i);
                                    if(v != null) {
                                        hasSold = true;
                                        amt+=v.getSell()*i.getAmount();
                                        items+=i.getAmount();
                                        values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                        c2.getSnapshotInventory().setItem(z, null);
                                    }
                                }
                                c2.update();
                                c2.update(true);
                                c1.update();
                                c1.update(true);
                                if(hasSold) {
                                    int amx = value.containsKey("Double Chests") ? value.get("Double Chests") : 0;
                                    value.put("Double Chests", amx + 1);
                                }
                                continue;
                            }
                        }
                        int ItemAmt = c.getSnapshotInventory().getContents().length;
                        boolean hasItems = false;
                        for(int z = 0; z < c.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            hasItems = true;
                            break;
                        }
                        for(int z = 0; z < c.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                c.getSnapshotInventory().setItem(z, null);
                            }
                        }

                        if(hasItems) {
                            int amx = value.containsKey(StringUtils.getNameFromEnum(c.getType())+"s") ?value.get(StringUtils.getNameFromEnum(c.getType())+"s") : 0;
                            value.put(StringUtils.getNameFromEnum(c.getType())+"s", amx+1);
                        }
                        c.update();
                        c.update(true);
                    }
                    if(amt == 0){
                        u.sendMessage("[c]Sold nothing");
                        return;
                    }
                    StringBuilder itemS = new StringBuilder(values.keySet().size() > 0 ? "[c]Items Sold" : "");
                    StringBuilder containers = new StringBuilder(value.keySet().size() > 0 ? "[c]Container Types" : "");
                    for(String i : value.keySet()) {
                        containers.append("\n[c] -" + i + " x" + value.get(i));
                    }

                    int item = 0;
                    for(Material i : values.keySet()) {
                        item+=values.get(i);
                    }
                    int stacks1 = item/64, leftOver1 = item +- stacks1*64;
                    itemS.append("\n -" + (stacks1 > 0 ? stacks1
                            + " Stacks " + (leftOver1 > 0 ? "and " + leftOver1 + " Items" : "") : (leftOver1 > 0 ? leftOver1 + " Items" : "Nothing")));
                    for(Material i : values.keySet()) {
                        itemS.append("\n [c]-[pc]" + StringUtils.getNameFromEnum(i) + " [c]x[pc]" + values.get(i) + " [c]Price Per: $[pc]" + NumberUtil.format(MiniShopHandler.getValue(i).getSell()));
                    }
                    u.depositMoney(NumberUtil.getMoney(amt+""));
                    int stacks = items/64, leftOver = items +- stacks*64;
                    String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                    Message msg = new Message("[c]Sold " + items + " items for " + amount + " &7&o[Hover for more info!]");
                    String toolTip = "";
                    if(containers.toString().length() > 0)
                        toolTip+=(containers.toString());
                    if(itemS.toString().length() > 0)
                        toolTip+="\n"+(itemS.toString());
                    msg.tooltip(toolTip);
                    msg.send(u);
                    return;
                }
                Container c = (Container) e.getClickedBlock().getState();
                User u = Miniverse.getUser(e.getPlayer());
                double amt = 0;
                int items = 0;
                (new BukkitRunnable() {
                    @Override
                    public void run() {
                        u.getBase().closeInventory();
                    }
                }).runTaskLater(MiniverseCore.getAPI(), 1);
                e.setCancelled(true);
                if(c instanceof Chest) {
                    Chest t = (Chest) c;
                    InventoryHolder holder = t.getInventory().getHolder();
                    if(holder instanceof DoubleChest) {
                        DoubleChest doubleChest = ((DoubleChest) holder);
                        Chest c1 = (Chest) doubleChest.getRightSide();
                        Chest c2 = (Chest) doubleChest.getLeftSide();
                        for(int z = 0; z < c1.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c1.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                c1.getSnapshotInventory().setItem(z, null);
                            }
                        }
                        for(int z = 0; z < c2.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c2.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                c2.getSnapshotInventory().setItem(z, null);
                            }
                        }
                        c2.update();
                        c2.update(true);
                        c1.update();
                        c1.update(true);
                        if(amt == 0){
                            u.sendMessage("[c]Sold nothing");
                            return;
                        }
                        u.depositMoney(amt);
                        int stacks = items/64, leftOver = items +- stacks*64;
                        String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                        String str = (stacks > 0 ? stacks + " Stacks " + (leftOver > 0 ? "and " + leftOver + " Items left" : "") : (leftOver > 0 ? leftOver + " items" : "Nothing")) + " for " + amount;
                        u.sendMessage("[c]Double Chest Sold " + str);
                        return;
                    }
                }

                for(int z = 0; z < c.getSnapshotInventory().getContents().length; z++) {
                    ItemStack i = c.getSnapshotInventory().getItem(z);
                    if(i == null || i.getType().name().contains("AIR")) continue;
                    ItemValues v = MiniShopHandler.getValue(i);
                    if(v != null) {
                        amt+=v.getSell()*i.getAmount();
                        items+=i.getAmount();
                        c.getSnapshotInventory().setItem(z, null);
                    }
                }
                c.update();
                c.update(true);
                if(amt == 0){
                    u.sendMessage("[c]Sold nothing");
                    return;
                }
                u.depositMoney(amt);
                int stacks = items/64, leftOver = items +- stacks*64;
                String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                u.sendMessage("[c]Sold " + (stacks > 0 ? stacks + " Stacks " + (leftOver > 0 ? "and " + leftOver + " Items left" : "") : (leftOver > 0 ? leftOver + " items" : "Nothing")) + " for " + amount);
            }
        }
    }




}
