package net.miniverse.listeners.signshop;

import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserActionEvent;
import net.miniverse.core.events.user.UserActionEvent.Action;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.itemutils.MiniItem;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class ChestShopOpen implements Listener {

    HashMap<User, Chest> users = new HashMap<>();
    HashMap<User, Sign> s = new HashMap<>();

    OfflineUser getUser(Location loc) {
        for(OfflineUser u : Miniverse.getAllUsers()) {
            if(loc != null && u!=null && u.getShop(loc) != null) return u;
        }
        return null;
    }

    @EventHandler
    void onDamage(BlockDamageEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(e.getBlock().getType() == Material.CHEST) {
            for(Block b : Miniverse.getNearbyBlocks(e.getBlock().getLocation(), 1)) {
                if(!b.getType().name().contains("SIGN"))continue;
                if(u.getShop(b.getLocation()) != null) {
                    users.put(Miniverse.getUser(e.getPlayer()), (Chest) e.getBlock().getState());
                    if(Miniverse.getUser(e.getPlayer()).getShop(b.getLocation()) != null) {
                        s.put(u, (Sign)b.getState());
                        users.put(u, (Chest)e.getBlock().getState());
                    }
                    e.getBlock().getState().update(true);
                    break;
                }
                if(getUser(b.getLocation()) != null) {
                    if(!u.hasPermission("chestshop.bypass")) {
                        e.setCancelled(true);
                        Miniverse.getUser(e.getPlayer()).sendMessage("[c]That is someones shop, you may not edit it.");
                        e.setCancelled(true);
                        (new BukkitRunnable() {
                            @Override
                            public void run() {
                                e.getPlayer().closeInventory();
                            }
                        }).runTaskLater(MiniverseCore.getAPI(), 1L);
                    }else {
                        s.put(u, (Sign)b.getState());
                        users.put(u, (Chest)e.getBlock().getState());
                    }
                    e.getBlock().getState().update(true);
                    break;
                }
            }
        }
    }

    @EventHandler
    void onClick(UserActionEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if((e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) && e.getBlock().getType() == Material.CHEST) {
            for(Block b : Miniverse.getNearbyBlocks(e.getBlock().getLocation(), 1)) {
                if(!b.getType().name().contains("SIGN"))continue;
                if(getUser(b.getLocation()) != null && getUser(b.getLocation()).getName() != u.getName()) {
                    if(!u.hasPermission("chestshop.bypass")) {
                        System.out.println("HEY!");
                        e.setCancelled(true);
                        Miniverse.getUser(e.getPlayer()).sendMessage("[c]That is someones shop, you may not edit it.");
                        e.setCancelled(true);
                        e.getPlayer().closeInventory();
                        e.getPlayer().closeInventory();
                        e.getPlayer().closeInventory();
                    }else {
                        s.put(u, (Sign)b.getState());
                        users.put(u, (Chest)e.getBlock().getState());
                    }
                    break;
                }
                if(u.getShop(b.getLocation()) != null) {
                    users.put(Miniverse.getUser(e.getPlayer()), (Chest) e.getBlock().getState());
                    if(Miniverse.getUser(e.getPlayer()).getShop(b.getLocation()) != null) {
                        s.put(u, (Sign)b.getState());
                        users.put(u, (Chest)e.getBlock().getState());
                    }
                    break;
                }
            }
        }
    }

    @EventHandler
    void onClick(InventoryClickEvent e) {
        User u = Miniverse.getUser(e.getWhoClicked().getName());
        if(!users.containsKey(u))return;
        if(e.getWhoClicked().getOpenInventory().getTopInventory()!=null
           && e.getWhoClicked().getOpenInventory().getTopInventory().getType() == InventoryType.CHEST
           && s.containsKey(u)) {
            try {
                Sign a = s.get(u);
                SignShopClickEvent b = new SignShopClickEvent();
                ItemStack i = MiniItem.getItem(s.get(u).getLine(2));
                i.setAmount(Integer.parseInt(s.get(u).getLine(1)));
                if(b.hasItem(e.getWhoClicked().getOpenInventory().getTopInventory(), i)) {
                    a.setLine(0, "§2" + ChatColor.stripColor(a.getLine(0)));
                }else {
                    a.setLine(0, "§4" + ChatColor.stripColor(a.getLine(0)));
                }
                a.update(true);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
    @EventHandler
    void onClose(InventoryCloseEvent e) {
        User u = Miniverse.getUser(e.getPlayer().getName());
        if(!users.containsKey(u))return;
        if(e.getPlayer().getOpenInventory().getTopInventory()!=null
                && e.getPlayer().getOpenInventory().getTopInventory().getType() == InventoryType.CHEST
                && s.containsKey(u)) {
            try {
                Sign a = s.get(u);
                SignShopClickEvent b = new SignShopClickEvent();
                ItemStack i = MiniItem.getItem(s.get(u).getLine(2));
                i.setAmount(Integer.parseInt(s.get(u).getLine(1)));
                if(b.hasItem(e.getPlayer().getOpenInventory().getTopInventory(), i)) {
                    a.setLine(0, "§2" + ChatColor.stripColor(a.getLine(0)));
                }else {
                    a.setLine(0, "§4" + ChatColor.stripColor(a.getLine(0)));
                }
                a.update(true);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
