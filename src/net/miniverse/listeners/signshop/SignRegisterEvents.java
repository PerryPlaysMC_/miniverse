package net.miniverse.listeners.signshop;

import net.miniverse.util.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.inventory.ItemStack;

import net.miniverse.core.Miniverse;
import net.miniverse.user.User;
import net.miniverse.util.itemutils.MiniItem;

public class SignRegisterEvents implements Listener {

    @EventHandler
    @SuppressWarnings("all")
    void onSignRegisterEvent(SignChangeEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        int x = e.getBlock().getFace(e.getBlock()).getOppositeFace().getModX(),
                y = e.getBlock().getFace(e.getBlock()).getOppositeFace().getModY(),
                z = e.getBlock().getFace(e.getBlock()).getOppositeFace().getModZ();
        Block back = e.getBlock().getWorld().getBlockAt(
                new Location(e.getBlock().getWorld(), x,y,z));
        if(back.getType()==Material.CHEST)return;
        if(e.getLine(0).equalsIgnoreCase("[aSell]") || e.getLine(0).startsWith("[aSell]")
                || e.getLine(0).equalsIgnoreCase("&1[Sell]") || e.getLine(0).startsWith("&1[Sell]")
                || e.getLine(0).equalsIgnoreCase("[aBuy]") || e.getLine(0).startsWith("[aBuy]")
                || e.getLine(0).equalsIgnoreCase("&1[Buy]") || e.getLine(0).startsWith("&1[Buy]")) {
            if(u.hasPermission("sign.shop.create", "shop.create")) {
                ItemStack item = null;
                String a = "["+(e.getLine(0).charAt(2)+"").toUpperCase() + e.getLine(0).substring(3).toLowerCase();
                e.setLine(0, "§1" + a);
                try {
                    item = MiniItem.getItem(e.getLine(2));
                } catch (Exception e1) {
                    e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
                    e.setLine(2, "§c" + e.getLine(2));
                }
                try {
                    Integer.parseInt(e.getLine(1));
                } catch (Exception e1) {
                    e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
                    e.setLine(1, "§c" + e.getLine(1));
                }
                if(item != null) {
                    @SuppressWarnings("deprecation")
                    String fullName = StringUtils.getNameFromEnum(item.getType());
                    e.setLine(2, fullName);
                }
                if(!e.getLine(3).startsWith("$")) {
                    e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
                    e.setLine(3, "§c" + e.getLine(3));
                }
                try {
                    Double.parseDouble(e.getLine(3).substring(1));
                } catch (Exception e1) {
                    e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
                    e.setLine(3, "§c" + e.getLine(3));
                }
            }
            return;
        }
        if(u.hasPermission("sign.color")) {
            for(int i = 0; i < e.getLines().length; i++) {
                e.setLine(i, ChatColor.translateAlternateColorCodes('&', e.getLines()[i]));
            }
        }
    }

    @EventHandler
    void onRegisterPlayerShop(SignChangeEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        Sign a = (Sign)e.getBlock().getState();
        String f = a.getBlockData().getAsString();
        Block back = e.getBlock().getRelative(BlockFace.valueOf(f.split("facing=")[1].toUpperCase().split(",WATERLOGGED")[0]).getOppositeFace());
        if(!e.getLine(0).equalsIgnoreCase("[Sell]") && !e.getLine(0).equalsIgnoreCase("[Buy]"))return;
        if(back.getType() != Material.CHEST && !u.hasPermission("sign.shop.create", "shop.create")) {
            e.setLine(0, "Must have chest behind");
            e.setLine(1, "Or will not work.");
            e.setLine(2, "");
            e.setLine(3, "");
            return;
        }
        ItemStack item = null;
        try {
            item = MiniItem.getItem(e.getLine(2));
        } catch (Exception e1) {
            e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
            e.setLine(2, "§c" + e.getLine(2));
        }
        try {
            Integer.parseInt(e.getLine(1));
        } catch (Exception e1) {
            e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
            e.setLine(1, "§c" + e.getLine(1));
        }
        if(item != null) {
            String name = ((item.getType().name().charAt(0)) + item.getType().name().substring(1).toLowerCase()), fullName = name.split("_")[0];
            if(name.contains("_")) {
                for(int i = 1; i < name.split("_").length;i++) {
                    fullName+=" "+name.split("_")[i];
                }
            }
            e.setLine(2, fullName);
        }
        if(!e.getLine(3).startsWith("$")) {
            e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
            e.setLine(3, "§c" + e.getLine(3));
        }
        try {
            Double.parseDouble(e.getLine(3).substring(1));
        } catch (Exception e1) {
            e.setLine(0, "§c" + e.getLine(0).replace("§1", ""));
            e.setLine(3, "§c" + e.getLine(3));
            return;
        }
        if(e.getLine(0).equalsIgnoreCase("[Sell]")) {
            u.addSell(e.getBlock().getLocation());
            e.setLine(0, "§2" + e.getLine(0));
            if(!u.hasEnough(Double.parseDouble(e.getLine(3).substring(1)))) {
                e.setLine(0, "§4" + e.getLine(0));
            }
        }
        if(e.getLine(0).equalsIgnoreCase("[Buy]")) {
            u.addBuy(e.getBlock().getLocation());
            e.setLine(0, "§2" + e.getLine(0));
            Chest c = (Chest)back.getState();
            if(!c.getInventory().contains(item)) {
                e.setLine(0, "§4" + e.getLine(0));
            }
        }
    }

}
