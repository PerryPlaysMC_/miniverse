package net.miniverse.listeners.signshop;

import net.miniverse.core.Miniverse;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import net.miniverse.util.economy.EconomyHelper;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.user.shops.Shop;
import net.miniverse.user.shops.ShopType;
import net.miniverse.util.itemutils.MiniItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.miniverse.util.economy.EconomyHelper.addItem;

public class SignShopClickEvent implements Listener {



    @EventHandler
    void onBreak(BlockBreakEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(e.getBlock().getType().name().contains("SIGN")) {
            if(Miniverse.getUser(e.getPlayer()).getShop(e.getBlock().getLocation())!=null){
                Shop s = u.getShop(e.getBlock().getLocation());
                if(s.getType() == ShopType.BUY) {
                    u.removeBuy(e.getBlock().getLocation());
                }else{
                    u.removeSell(e.getBlock().getLocation());
                }
                return;
            }
            List<OfflineUser> users = Miniverse.getAllUsers();
            users.remove(u);
            for(OfflineUser u2 : users) {
                if(u2!=null &&u2.getShop(e.getBlock().getLocation())!=null) {
                    e.setCancelled(true);
                    Miniverse.getUser(e.getPlayer()).sendMessage("[c]That is someones shop, you may not break it.");
                    break;
                }
            }
        }
        if(e.getBlock().getType() == Material.CHEST) {
            boolean findSign = false;

            for(Block b : Miniverse.getNearbyBlocks(e.getBlock().getLocation(), 1)) {
                if(b.getType().name().contains("SIGN")){
                    findSign=true;
                    break;
                }
            }
            if(findSign)
                for(Block b : Miniverse.getNearbyBlocks(e.getBlock().getLocation(), 1)) {
                    if(Miniverse.getUser(e.getPlayer()).getShop(b.getLocation())!=null) {
                        Shop s = u.getShop(e.getBlock().getLocation());
                        if(s.getType() == ShopType.BUY) {
                            u.removeBuy(e.getBlock().getLocation());
                        }else{
                            u.removeSell(e.getBlock().getLocation());
                        }
                        break;
                    }
                    List<OfflineUser> users = Miniverse.getAllUsers();
                    users.remove(u);
                    for(OfflineUser u2 : users) {
                        if(u2.getShop(b.getLocation())!=null) {
                            e.setCancelled(true);
                            Miniverse.getUser(e.getPlayer()).sendMessage("[c]That is someones shop, you may not break it.");
                            break;
                        }
                    }
                }
        }
    }

    @EventHandler
    @SuppressWarnings("all")
    void adminShpClick(PlayerInteractEvent e) {
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if(!(e.getClickedBlock().getState() instanceof Sign)) return;
        Sign s = (Sign) e.getClickedBlock().getState();
        if(s.getLine(0).equalsIgnoreCase("§1[Sell]") || s.getLine(0).equalsIgnoreCase("§1[Buy]")) {
            ItemStack item = null;
            try {
                item = MiniItem.getItem(s.getLine(2));
            } catch (Exception e1) {
                s.setLine(0, "");
                s.setLine(1, "Error occurred whilst");
                s.setLine(2, "Parsing item");
                s.setLine(3, "Check console");
                e1.printStackTrace();
            }
            if(item != null) {
                if(s.getLine(0).equalsIgnoreCase("§1[Sell]"))
                    EconomyHelper
                            .sellItem(Miniverse.getUser(e.getPlayer()), Integer.parseInt(s.getLine(1)), Double.parseDouble(s.getLine(3).substring(1)), item.getType());
                if(s.getLine(0).equalsIgnoreCase("§1[Buy]"))
                    EconomyHelper.buyItem(Miniverse.getUser(e.getPlayer()), Integer.parseInt(s.getLine(1)), Double.parseDouble(s.getLine(3).substring(1)), item.getType());
            }
        }
    }


    @EventHandler
    @SuppressWarnings("all")
    void playerShop(PlayerInteractEvent e) {
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if(!(e.getClickedBlock().getState() instanceof Sign)) return;
        Sign s = (Sign) e.getClickedBlock().getState();
        if(s.getLine(0).toLowerCase().contains("[sell]") || s.getLine(0).toLowerCase().contains("[buy]")) {
            ItemStack item = null;
            try {
                item = MiniItem.getItem(s.getLine(2));
            } catch (Exception e1) {
                s.setLine(0, "");
                s.setLine(1, "Error occurred whilst");
                s.setLine(2, "Parsing item");
                s.setLine(3, "Check console");
                e1.printStackTrace();
            }
            User u = Miniverse.getUser(e.getPlayer());
            String f = s.getBlockData().getAsString();
            Block back = e.getClickedBlock().getRelative(BlockFace.valueOf(f.split("facing=")[1].toUpperCase().split(",WATERLOGGED")[0]).getOppositeFace());
            if(item != null) {
                if(!(back.getState() instanceof Chest)) return;
                if(s.getLine(0).toLowerCase().contains("[sell]")) {
                    OfflineUser s2 = null;
                    for (OfflineUser o : Miniverse.getAllUsers()) {
                        if(o.getShop(e.getClickedBlock().getLocation()) != null) {
                            s2 = o;
                            break;
                        }
                    }
                    Chest c = (Chest) back.getState();
                    if(s.getLine(0).equalsIgnoreCase("§4[Sell]")) {
                        s.setLine(0, "§2" + ChatColor.stripColor(s.getLine(0)));
                        if(!s2.hasEnough(Double.parseDouble(s.getLine(3).substring(1)))) {
//                            u.getInventory().addItem(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
//                            c.getInventory().remove(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
                            c.update();
                            s.setLine(0, "§4" + ChatColor.stripColor(s.getLine(0)));
                            s.update(true);
                            u.sendMessage("[pc]" + s2.getName() + "[c] Doesn't have enough money");
                            return;
                        }
                        Inventory i = Bukkit.createInventory(null, c.getInventory().getSize(), "PlaceHolder");
                        i.setContents(c.getInventory().getContents());
                        HashMap<Integer, ItemStack> excess = i.addItem(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
                        if(excess.get(0) != null) {
                            s.setLine(0, "§4" + ChatColor.stripColor(s.getLine(0)));
                            u.getInventory().addItem(excess.get(0));
                            u.sendMessage("[c]Chest is full");
                            s.update(true);
                            c.update();
                            return;
                        }
                        s.update(true);
                        c.update();
                    }
                    Inventory i = Bukkit.createInventory(null, c.getInventory().getSize(), "PlaceHolder");
                    i.setContents(c.getInventory().getContents());
                    HashMap<Integer, ItemStack> excess = i.addItem(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
                    if(excess.get(0) != null) {
                        u.getInventory().addItem(excess.get(0));
                        u.sendMessage("[c]Chest is full");
                        c.update();
                        return;
                    } else {
                        s.setLine(0, "§2" + ChatColor.stripColor(s.getLine(0)));
                        s.update(true);
                    }
                    c.update();
                    if(!s2.hasEnough(Double.parseDouble(s.getLine(3).substring(1)))) {
//                        u.getInventory().addItem(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
//                        c.getInventory().remove(new ItemStack(item.getType(), Integer.parseInt(s.getLine(1))));
                        s.setLine(0, "§4" + ChatColor.stripColor(s.getLine(0)));
                        s.update(true);
                        u.sendMessage("[pc]" + s2.getName() + "[c] Doesn't have enough money");
                        c.update();
                        return;
                    } else {
                        s.setLine(0, "§2" + ChatColor.stripColor(s.getLine(0)));
                        s.update(true);
                    }
                    sellItem(s, (Chest) back.getState(), Miniverse.getUser(e.getPlayer()), Integer.parseInt(s.getLine(1)), Double.parseDouble(s.getLine(3).substring(1)), item.getType());
                }
                else if(s.getLine(0).toLowerCase().contains("[buy]")) {
                    Chest c = (Chest) back.getState();
                    Material mat = item.getType();
                    int amount = Integer.parseInt(s.getLine(1));
                    if(hasItem(c.getInventory(), new ItemStack(mat, amount))) {
                        s.setLine(0, "§2" + ChatColor.stripColor(s.getLine(0)));
                        s.update(true);
                    }
                    if(!hasItem(c.getInventory(), new ItemStack(mat, amount))) {
                        s.setLine(0, "§4" + ChatColor.stripColor(s.getLine(0)));
                        u.sendMessage("&cChest is out of stock!");
                        s.update(true);
                        return;
                    }
                    buyItem((Chest) back.getState(), s, Miniverse.getUser(e.getPlayer()), Integer.parseInt(s.getLine(1)), Double.parseDouble(s.getLine(3).substring(1)), item.getType());
                }
            }
        }
    }

    boolean hasItem(Inventory inv, ItemStack i) {
        for(ItemStack a : inv.getContents()) {
            if(a!=null&&a.getType()!=Material.AIR) {
                if(a.isSimilar(i)) {
                    if(a.getAmount()>= i.getAmount()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void removeItem(Inventory inv, ItemStack i) {
        for(ItemStack a : inv.getContents()) {
            if(a!=null&&a.getType()!=Material.AIR) {
                if(a.isSimilar(i)) {
                    if(a.getAmount() > i.getAmount()) {
                        a.setAmount(a.getAmount()+-i.getAmount());
                        break;
                    }
                    if(a.getAmount()==i.getAmount()) {
                        inv.remove(a);
                        break;
                    }
                }
            }
        }
    }

    public void buyItem(Chest c, Sign s, User user, int amount, double worth, Material mat) {
        String format = (StringUtils.getNameFromEnum(mat));
        if(!user.hasEnough(worth)) {
            user.sendMessage(Miniverse.getSettings(), "Shop.Buy.not-Enough-Money", amount, format, worth);
            return;
        }
        if(hasItem(c.getInventory(), new ItemStack(mat, amount))) {
            s.setLine(0, "§2" + ChatColor.stripColor(s.getLine(0)));
            s.update(true);
        }
        if(!hasItem(c.getInventory(), new ItemStack(mat, amount))) {
            s.setLine(0, "§4" + ChatColor.stripColor(s.getLine(0)));
            user.sendMessage("§cChest is out of stock!");
            s.update(true);
            return;
        }
        OfflineUser o = null;
        for(OfflineUser o2 : Miniverse.getAllUsers()) {
            if(o2.getShop(s.getLocation())!= null) {
                o = o2;
                o.depositMoney(worth);
                o.sendMessage("[pc]" + user.getName() + "[c] Has bought x[pc]" + amount
                        + " " + format + "[c] From you for " + NumberUtil.format(worth));
                break;
            }
        }
        removeItem(c.getInventory(), new ItemStack(mat, amount));
        addItem(user, o, c, amount, worth, mat);
    }

    public void sellItem(Sign loc, Chest c, User user, int amount, double worth, Material m) {
        User u = user;
        String format = (StringUtils.getNameFromEnum(m));
        if(EconomyHelper.getItems(user, m) >= amount && amount >0 || EconomyHelper.getFreeItems(c.getInventory()) ==0) {
            worth = worth*amount +- 5%5;
            if(user.balanceTooBig(worth)) {
                String f = NumberUtil.format(worth);
                user.sendMessage(Miniverse.getSettings(), "Shop.Sell.exceeds-Balance", amount, format, f, worth);
                return;
            }else {
                EconomyHelper.removeItem(user, amount, m);
                HashMap<Integer, ItemStack> excess = c.getInventory().addItem(new ItemStack(m, amount));
                double money;
                for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
                    String f;
                    money = worth;
                    f = NumberUtil.format(money);
                    amount = amount +- me.getValue().getAmount();
                    u.withDrawMoney(money);
                    for(OfflineUser o : Miniverse.getAllUsers()) {
                        if(o.getShop(loc.getLocation())!= null) {
                            o.depositMoney(money);
                            loc.setLine(0, "§4[Sell]");
                            break;
                        }
                    }
                    u.sendMessage(Miniverse.getSettings(), "Shop.Buy.not-enough-space", me.getValue().getAmount(), format, f, money);
                }
                OfflineUser s = null;
                for(OfflineUser o : Miniverse.getAllUsers()) {
                    if(o.getShop(loc.getLocation())!= null) {
                        s=o;
                        break;
                    }
                }
                if(s == null) {
                    user.getInventory().addItem(new ItemStack(m, amount));
                    c.getInventory().remove(new ItemStack(m, amount));
                    c.update();
                    return;
                }
                if(!s.hasEnough(worth)) {
                    user.getInventory().addItem(new ItemStack(m, amount));
                    c.getInventory().remove(new ItemStack(m, amount));
                    c.update();
                    loc.setLine(0, "§4[Sell]");
                    loc.update(true);
                    u.sendMessage("[pc]"+s.getName() + "[c] Doesn't have enough money");
                    return;
                }
                if(u.balanceTooBig(worth)) {
                    u.sendMessage("&cSelling this will exceed your balance");
                    return;
                }
                s.withDrawMoney(worth);
                s.sendMessage("[pc]" + u.getName() + "[c] Sold an item to you for $" + worth + "!");
                user.depositMoney(worth);
                String f = NumberUtil.format(worth);
                user.sendMessage(Miniverse.getSettings(), "Shop.Sell.success", amount, format, f, worth);
            }
        }else {
            String f = NumberUtil.format(worth);

            user.sendMessage(Miniverse.getSettings(), "Shop.Sell.fail", amount, format, f, worth);
        }
    }

    @EventHandler
    void breakSign(BlockBreakEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(!(e.getBlock().getState() instanceof Sign)) return;
        Sign s = (Sign) e.getBlock().getState();
        if(s.getLine(0).equalsIgnoreCase("§1[Sell]") || s.getLine(0).equalsIgnoreCase("§1[Buy]")) {
            if(!u.hasPermission("signshop.break")) {
                e.setCancelled(true);
            }
        }
    }



}
