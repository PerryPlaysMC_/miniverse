package net.miniverse.listeners;

import com.google.common.collect.Lists;
import io.netty.buffer.ByteBuf;
import java.text.DecimalFormat;
import java.util.*;

import net.minecraft.server.v1_14_R1.*;
import net.miniverse.MiniversePlugin;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommandHandler;
import net.miniverse.commands.admin.server.CommandPlugins;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserJoinEvent;
import net.miniverse.core.events.user.UserQuitEvent;
import net.miniverse.gui.shops.ItemValues;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.ItemUtil;
import net.miniverse.util.plugin.Manager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.command.Command;
import org.bukkit.entity.*;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.inventory.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

@SuppressWarnings("all")
public class BasicEvents implements Listener {


    @EventHandler
    void onCommandTab(TabCompleteEvent e) {
        String cmd = e.getBuffer().toLowerCase().split(" ")[0].substring(1);
        String[] argss = e.getBuffer().split(" ");
        String[] args = Arrays.copyOfRange(argss, 1, argss.length);
        if((cmd.equals("pl") || cmd.equals("plugins"))) {
            Manager m = Miniverse.getManager();
            List<String> f = Lists.newArrayList();
            List<String> arguments = Arrays.asList("reload", "unload", "load");
            for(Plugin pl : m.getPlugins()) {
                f.add(pl.getName());
            }
            e.setCompletions(f);
            if(args.length == 1) {
                if(e.getBuffer().endsWith(" ")) {f = Lists.newArrayList();
                    for(String a : arguments) {
                        f.add(a);
                    }
                    e.setCompletions(f);
                }else {
                    for(Plugin pl : m.getPlugins()) {
                        if(pl.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                            f.add(pl.getName());
                        }
                    }
                    e.setCompletions(f);
                    return;
                }
            }
            if(args.length == 2) {
                for(String a : arguments) {f = Lists.newArrayList();
                    if(a.toLowerCase().startsWith(args[1].toLowerCase())) {
                        f.add(a);
                    }
                }
                e.setCompletions(f);
                return;
            }
            e.setCompletions(f);
        }
    }


    @EventHandler
    public void Event(UserJoinEvent e) {
        User u = e.getUser();
        String msg = Miniverse.format(Miniverse.getSettings(), "Events.Connection.LogIn", u.getName(), e.getMessage(), u.getBalance()).replace("%balance%", u.getBalance()).replace("%player%", u.getName()).replace("%message%", e.getMessage()).replace("%rank%", u.getPrefix());
        e.setMessage(msg);
    }


    @EventHandler
    public void onTab(TabCompleteEvent e) {
        CommandSource s = Miniverse.getSource(e.getSender().getName());
        if(Miniverse.parseCmdFull(e.getBuffer()) != null) {
            Command cmd = Miniverse.parseCmdFull(e.getBuffer());
            if(!s.hasPermission(cmd.getPermission())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void Event(UserQuitEvent e) {
        OfflineUser u = e.getUser();
        String msg = Miniverse.format(Miniverse.getSettings(), "Events.Connection.LogOut");
        if(msg.contains("%balance%")) msg = msg.replace("%balance%", u.getBalance());
        if(msg.contains("%player%")) msg = msg.replace("%player%", u.getName());
        if(msg.contains("%message%")) msg = msg.replace("%message%", e.getMessage());
        if(msg.contains("%rank%")) msg = msg.replace("%rank%", u.getPrefix());
        e.setMessage(msg);
    }

    @EventHandler
    void onland(ProjectileHitEvent e) {
        if((e.getHitEntity() == null) && (e.getHitBlock() != null)) e.getEntity().setBounce(true);
    }

    @EventHandler
    void onHit(EntityDamageByEntityEvent e) {
        if((((e.getDamager() instanceof org.bukkit.entity.Snowball)) || ((e.getDamager() instanceof Projectile))) &&
                (e.getEntity() != null) && (
                (e.isCancelled()) || (e.getDamage() == 0.0D))) {
            e.setCancelled(false);
            e.setDamage(0.001D);
        }
    }


    public void showBook(ItemStack book, User p) {
        int slot = p.getInventory().getHeldItemSlot();
        ItemStack old = p.getInventory().getItem(slot);
        p.getInventory().setItem(slot, book);

        ByteBuf buf = io.netty.buffer.Unpooled.buffer(256);
        buf.setByte(0, 0);
        buf.writerIndex(1);
        PacketPlayOutCustomPayload packet =
                new PacketPlayOutCustomPayload(new MinecraftKey("minecraft:book_open"), new PacketDataSerializer(buf));
        p.sendPacket(packet);
        if((old == null) || (old.getType() == Material.AIR)) {
            p.getInventory().setItem(slot, null);
        } else {
            p.getInventory().setItem(slot, old);
        }
    }

    HashMap<Player, Material> sleep = new HashMap<>();
    HashMap<User, Boolean> click = new HashMap<>();

    @EventHandler
    void onClick(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final User u = Miniverse.getUser(e.getPlayer());
            if((u.getItemInHand() != null) &&
                    (u.getItemInHand().getType() == Material.BONE_MEAL)) {
                Block b = e.getClickedBlock();
                if((b.getType() == Material.SUGAR_CANE) &&
                        (!click.containsKey(u))) {
                    Location ea = b.getLocation();
                    while (ea.getBlock().getType() == Material.SUGAR_CANE) {
                        ea = ea.add(0.0D, 1.0D, 0.0D);
                    }
                    if(ea.getBlock().getType() == Material.AIR) {
                        ea.getBlock().setType(Material.SUGAR_CANE);
                        ea.getBlock().getState().update();
                    }
                    click.put(u, true);
                    final Location a = ea;
                    new BukkitRunnable()
                    {
                        public void run() {
                            click.remove(u);
                            if(u.getGameMode() == org.bukkit.GameMode.CREATIVE) {
                                return;
                            }
                            if(a.getY() <= 255.0D) {
                                u.getItemInHand().setAmount(u.getItemInHand().getAmount() + -1);
                            }
                        }
                    }.runTaskLater(MiniverseCore.getAPI(), 2L);
                }
            }
        }
    }




    List<String> sleepers = new ArrayList<>();

    @EventHandler
    void onBreak(BlockBreakEvent e) {
        if(e.getBlock().getType().name().contains("SIGN")) {
            List<String> locations = MiniversePlugin.getLocations().getStringList("ChunkLoaders");
            if(!locations.contains(StringUtils.locationToString(e.getBlock().getLocation())))
                return;
            locations.remove(StringUtils.locationToString(e.getBlock().getLocation()));
            MiniversePlugin.getLocations().set("ChunkLoaders", locations);
            e.setDropItems(false);
            ItemStack cL = ItemUtil.setItem(new ItemStack(Material.OAK_SIGN)).setDisplayName(true, "[c]Chunk Loader").finish();
            Item i = e.getBlock().getLocation().getWorld().dropItemNaturally(e.getBlock().getLocation().add(0.5D, 0.5D, 0.5D), cL);
            i.setVelocity(new org.bukkit.util.Vector(0, 0, 0));
        }
    }

    @EventHandler
    void overrideShulker(final InventoryClickEvent e) {
        if(e.getSlot() < 0) return;
        if(e.getClickedInventory() == null) return;
        if(e.getClickedInventory().getType() != InventoryType.SHULKER_BOX) return;
        if(e.getCursor() == null) return;
        if((!e.getCursor().getType().name().contains("SHULKER_BOX")) && (!e.getCurrentItem().getType().name().contains("SHULKER_BOX"))) return;
        ItemStack c = e.getCursor(), n = e.getClickedInventory().getItem(e.getSlot()), ci = e.getCurrentItem();
        Inventory i = e.getWhoClicked().getOpenInventory().getTopInventory()
                , i2 = e.getWhoClicked().getOpenInventory().getBottomInventory();
        if((e.getClick() == ClickType.SHIFT_LEFT) && (i != null) && (i2 != null) && (i.firstEmpty() != -1) && (i.getType() == InventoryType.SHULKER_BOX)) {
            new BukkitRunnable()
            {
                public void run()
                {
                    i.setItem(i.firstEmpty(), ci);
                    e.getClickedInventory().setItem(e.getSlot(), new ItemStack(Material.AIR));
                }
            }.runTaskLater(MiniversePlugin.getInstance(), 1);
            return;
        }
        if((e.getCursor() == null) || (e.getCursor().getType() == Material.AIR)) return;
        if(e.getClickedInventory().getType() != InventoryType.SHULKER_BOX) return;
        (new BukkitRunnable() {
            public void run() {
                e.setCurrentItem(c);
                e.setCursor(ci);
            }
        }).runTaskLater(MiniversePlugin.getInstance(), 1L);
    }/*

    @EventHandler
    void onSellWand(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK && (e.getItem() != null && e.getItem().getType() == Material.DIAMOND_HOE)) {
            if(e.getClickedBlock().getState() instanceof Container) {
                if(e.getPlayer().isSneaking()) {
                    double amt = 0;
                    int items = 0;
                    User u = Miniverse.getUser(e.getPlayer());
                    StringBuilder itemS = new StringBuilder("[c]Items Sold");
                    StringBuilder containers = new StringBuilder("[c]Container Types");
                    int doubleChests = 0;
                    int Hopper = 0;
                    int Chest = 0;
                    HashMap<Material, Integer> values = new HashMap<>();


                    for(Block b : Miniverse.getNearbyContainers(-1, e.getClickedBlock())) {
                        Container c = (Container) b.getState();
                        (new BukkitRunnable() {
                            @Override
                            public void run() {
                                u.getBase().closeInventory();
                            }
                        }).runTaskLater(MiniverseCore.getAPI(), 1);
                        e.setCancelled(true);
                        if(c instanceof Chest) {
                            Chest t = (Chest) c;
                            InventoryHolder holder = t.getInventory().getHolder();
                            if(holder instanceof DoubleChest) {
                                DoubleChest doubleChest = ((DoubleChest) holder);
                                Chest c1 = (Chest) doubleChest.getRightSide();
                                Chest c2 = (Chest) doubleChest.getLeftSide();
                                int c1c = c1.getSnapshotInventory().getContents().length;
                                int c2c = c2.getSnapshotInventory().getContents().length;
                                for(int z = 0; z < c1.getSnapshotInventory().getContents().length; z++) {
                                    ItemStack i = c1.getSnapshotInventory().getItem(z);
                                    if(i == null || i.getType().name().contains("AIR")) continue;
                                    ItemValues v = MiniShopHandler.getValue(i);
                                    if(v != null) {
                                        amt+=v.getSell()*i.getAmount();
                                        items+=i.getAmount();
                                        values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                        c1.getSnapshotInventory().setItem(z, null);
                                    }
                                }
                                for(int z = 0; z < c2.getSnapshotInventory().getContents().length; z++) {
                                    ItemStack i = c2.getSnapshotInventory().getItem(z);
                                    if(i == null || i.getType().name().contains("AIR")) continue;
                                    ItemValues v = MiniShopHandler.getValue(i);
                                    if(v != null) {
                                        amt+=v.getSell()*i.getAmount();
                                        items+=i.getAmount();
                                        values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                        c2.getSnapshotInventory().setItem(z, null);
                                    }
                                }
                                c2.update();
                                c2.update(true);
                                c1.update();
                                c1.update(true);
                                if(c1c > 0 || c2c > 0)
                                    doubleChests++;
                                continue;
                            }
                        }
                        int ItemAmt = c.getSnapshotInventory().getContents().length;
                        for(int z = 0; z < c.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                values.put(i.getType(), values.containsKey(i.getType()) ? values.get(i.getType()) + i.getAmount() : i.getAmount());
                                c.getSnapshotInventory().setItem(z, null);
                            }
                        }
                        if(ItemAmt > 0) {
                            if(c instanceof Hopper)
                                Hopper++;
                            if(c instanceof Chest)
                                Chest++;
                        }
                        c.update();
                        c.update(true);
                    }
                    if(amt == 0){
                        u.sendMessage("[c]Sold nothing");
                        return;
                    }
                    if(doubleChests > 0) containers.append("\n[c] -Double Chests x" + doubleChests);
                    if(Hopper > 0) containers.append("\n[c] -Hoppers x" + Hopper);
                    if(Chest > 0) containers.append("\n[c] -Chests x" + doubleChests);
                    for(Material i : values.keySet()) {
                        int item = values.get(i);
                        int stacks = item/64, leftOver = item +- stacks*64;
                        itemS.append("\n -" + StringUtils.getNameFromEnum(i) + " x" + values.get(i));
                        itemS.append("\n  -" + (stacks > 0 ? stacks
                                + " Stacks " + (leftOver > 0 ? "and " + leftOver + " Items left" : "") : (leftOver > 0 ? leftOver + " items" : "Nothing")));
                    }
                    u.depositMoney(NumberUtil.getMoney(amt+""));
                    int stacks = items/64, leftOver = items +- stacks*64;
                    String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                    Message msg = new Message("[c]Sold " + items + " items for " + amount + " &7&o[Hover for more info!]");
                    String toolTip = "";
                    if(containers.toString().length() > 0)
                        toolTip+=(containers.toString());
                    if(itemS.toString().length() > 0)
                        toolTip+="\n"+(itemS.toString());
                    msg.tooltip(toolTip);
                    msg.send(u);
                    return;
                }
                Container c = (Container) e.getClickedBlock().getState();
                User u = Miniverse.getUser(e.getPlayer());
                double amt = 0;
                int items = 0;
                (new BukkitRunnable() {
                    @Override
                    public void run() {
                        u.getBase().closeInventory();
                    }
                }).runTaskLater(MiniverseCore.getAPI(), 1);
                e.setCancelled(true);
                if(c instanceof Chest) {
                    Chest t = (Chest) c;
                    InventoryHolder holder = t.getInventory().getHolder();
                    if(holder instanceof DoubleChest) {
                        DoubleChest doubleChest = ((DoubleChest) holder);
                        Chest c1 = (Chest) doubleChest.getRightSide();
                        Chest c2 = (Chest) doubleChest.getLeftSide();
                        for(int z = 0; z < c1.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c1.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                c1.getSnapshotInventory().setItem(z, null);
                            }
                        }
                        for(int z = 0; z < c2.getSnapshotInventory().getContents().length; z++) {
                            ItemStack i = c2.getSnapshotInventory().getItem(z);
                            if(i == null || i.getType().name().contains("AIR")) continue;
                            ItemValues v = MiniShopHandler.getValue(i);
                            if(v != null) {
                                amt+=v.getSell()*i.getAmount();
                                items+=i.getAmount();
                                c2.getSnapshotInventory().setItem(z, null);
                            }
                        }
                        c2.update();
                        c2.update(true);
                        c1.update();
                        c1.update(true);
                        if(amt == 0){
                            u.sendMessage("[c]Sold nothing");
                            return;
                        }
                        u.depositMoney(amt);
                        int stacks = items/64, leftOver = items +- stacks*64;
                        String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                        String str = (stacks > 0 ? stacks + " Stacks " + (leftOver > 0 ? "and " + leftOver + " Items left" : "") : (leftOver > 0 ? leftOver + " items" : "Nothing")) + " for " + amount;
                        u.sendMessage("[c]Double Chest Sold " + str);
                        return;
                    }
                }

                for(int z = 0; z < c.getSnapshotInventory().getContents().length; z++) {
                    ItemStack i = c.getSnapshotInventory().getItem(z);
                    if(i == null || i.getType().name().contains("AIR")) continue;
                    ItemValues v = MiniShopHandler.getValue(i);
                    if(v != null) {
                        amt+=v.getSell()*i.getAmount();
                        items+=i.getAmount();
                        c.getSnapshotInventory().setItem(z, null);
                    }
                }
                c.update();
                c.update(true);
                if(amt == 0){
                    u.sendMessage("[c]Sold nothing");
                    return;
                }
                u.depositMoney(amt);
                int stacks = items/64, leftOver = items +- stacks*64;
                String amount = "$" + NumberUtil.format(NumberUtil.getMoney(amt+""));
                u.sendMessage("[c]Sold " + (stacks > 0 ? stacks + " Stacks " + (leftOver > 0 ? "and " + leftOver + " Items left" : "") : (leftOver > 0 ? leftOver + " items" : "Nothing")) + " for " + amount);
            }
        }
    }
*/
    @EventHandler(priority= EventPriority.HIGHEST)
    public void onKill(EntityDeathEvent e) {
        if (e.getEntity() instanceof Blaze && e.getEntity().getKiller() == null) {
            ItemStack i = new ItemStack(Material.BLAZE_ROD, new Random().nextInt(3)+1);
            e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), i);
        }
    }

    @EventHandler
    void onBlockPlace(final BlockPlaceEvent e) {
        if((e.getBlockPlaced().getType().name().contains("SIGN")) &&
                (e.getItemInHand().hasItemMeta()) &&
                (e.getItemInHand().getItemMeta().hasDisplayName()) &&
                (e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(StringUtils.translate("[c]Chunk Loader")))) {
            List<String> locations = MiniversePlugin.getLocations().getStringList("ChunkLoaders");
            if(!locations.contains(StringUtils.locationToString(e.getBlockPlaced().getLocation())))
                locations.add(StringUtils.locationToString(e.getBlockPlaced().getLocation()));
            MiniversePlugin.getLocations().set("ChunkLoaders", locations);
            final Sign s = (Sign)e.getBlockPlaced().getState();
            new BukkitRunnable()
            {
                public void run() {
                    e.getPlayer().closeInventory();
                    s.setLine(0, StringUtils.translate("[pc]ChunkLoader"));
                    s.setLine(1, StringUtils.translate("[c]Keeps this chunk"));
                    s.setLine(2, StringUtils.translate("[c]loaded ;D"));
                    s.update();
                    s.update(true);
                }
            }.runTaskLater(MiniverseCore.getAPI(), 2L);
        }
    }

    @EventHandler
    void sleep(final PlayerBedEnterEvent e) {
        if(e.isCancelled()) return;
        sleepers.add(e.getPlayer().getName());
        new BukkitRunnable()
        {
            public void run() {
                if(!sleepers.contains(e.getPlayer().getName())) {
                    return;
                }
                org.bukkit.World w = e.getPlayer().getWorld();
                w.setStorm(false);
                w.setThundering(false);
                e.getPlayer().getWorld().setWeatherDuration(0);
                e.getPlayer().getWorld().setTime(8L);
                Miniverse.broadCast("[pc]" + e.getPlayer().getName() + "[c] Went to sleep, &o&7Sweet dreams");
                sleepers.remove(e.getPlayer().getName());
            }

        }.runTaskLater(MiniverseCore.getAPI(), 100L);
    }

    @EventHandler
    void leaveBed(PlayerBedLeaveEvent e) {
        sleepers.remove(e.getPlayer().getName());
    }


    List<User> users = new ArrayList<>();

    @EventHandler
    void onWithdraw(PlayerInteractEvent e) { final User u = Miniverse.getUser(e.getPlayer());
        if((!e.getAction().name().contains("RIGHT")) || (u.getItemInHand().getType() != Material.PAPER)) return;
        if(!users.contains(u))
            users.add(u); else {
            return;
        }



        new BukkitRunnable()
        {
            public void run() { users.remove(u); }
        }.runTaskLater(MiniverseCore.getAPI(), 5L);
        ItemStack paper = new ItemStack(Material.PAPER);
        ItemUtil util = ItemUtil.setItem(u.getItemInHand());
        if(!util.hasKey("money")) return;
        ItemUtil u2 = ItemUtil.setItem(paper);
        u2.setDisplayName("§a§lBankNote §e§oRight Click!")
                .setLore("&cOwner: &e&l" + util.getString("owner"), "&cWorth: &l$&a" + NumberUtil.format(NumberUtil.getMoney(""+util.getDouble("money"))));
        paper = u2.finish();
        if(checkNameAndLore(u.getItemInHand(), paper)) {
            if(net.miniverse.util.config.Economy.isBalanceGreaterThanMax(u, util.getDouble("money"))) {
                u.sendMessage(Miniverse.getSettings(), "Events.Withdraw.exceeds-maxBalance", Double.valueOf(util.getDouble("money")));
                return;
            }
            u.depositMoney(util.getDouble("money"));
            Miniverse.removeItem(u, u.getItemInHand(), 1);
            u.sendMessage(Miniverse.getSettings(), "Events.Withdraw.click", NumberUtil.format(NumberUtil.getMoney(""+util.getDouble("money"))), util.getString("owner"));
        }
    }

    @EventHandler
    void onClickEvent(PlayerInteractEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(u.getItemInHand() == null || u.getItemInHand().getType().name().contains("AIR")) return;
        if(u.isSet(u.getItemInHand().getType())) {
            //u.sendMessage("[c]Performing command '[pc]/" + u.getCommand(u.getItemInHand().getType()) + "[c]'");
            u.getBase().chat("/" + u.getCommand(u.getItemInHand().getType()));
            e.setCancelled(true);
        }
    }

    boolean checkNameAndLore(ItemStack i1, ItemStack i2) {
        if((!i1.hasItemMeta()) || (!i2.hasItemMeta())) return false;
        if((!i1.getItemMeta().hasLore()) || (!i2.getItemMeta().hasLore())) return false;
        if((!i1.getItemMeta().hasDisplayName()) || (!i2.getItemMeta().hasDisplayName())) return false;
        ItemMeta im = i1.getItemMeta();
        ItemMeta im2 = i2.getItemMeta();
        boolean lore = true;
        if(im.getLore().size() == im2.getLore().size()) {
            for (int i = 0; i < im.getLore().size(); i++) {
                if(!StringUtils.stripColor(im.getLore().get(i)).equalsIgnoreCase(StringUtils.stripColor(im2.getLore().get(i)))) {
                    lore = false;
                }
            }
        }
        if(!lore) return false;
        return StringUtils.stripColor(im.getDisplayName()).equalsIgnoreCase(StringUtils.stripColor(im2.getDisplayName()));
    }
}
