package net.miniverse.listeners;

import net.miniverse.core.events.user.UserChatEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class onChat implements Listener {

    @EventHandler
    void onChat(UserChatEvent e) {
        String msg = e.getMessage().toLowerCase();
        String removedPeriods = msg.replace(".", "").replace(",", "");
        String noSpaces = removedPeriods.replaceAll(" ", "");
        String fixed = noSpaces.replaceAll("(\\w\\w)\\1+", "$1");
        e.setMessage(e.getMessage().replace("[shrug]", "¯\\_(ツ)_/¯").replace(":eyez:", "👀"));
    }
}
