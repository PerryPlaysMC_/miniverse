package net.miniverse.listeners;

import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.economy.EconomyHelper;
import net.miniverse.core.events.ShopInteract;
import net.miniverse.gui.shops.MiniShop;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.user.User;
import net.miniverse.util.itemutils.MiniItem;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerShopEvents implements Listener {
    private HashMap<User, MiniShop> sets = new HashMap<>();
    private List<User> users = new ArrayList<>();
    private Inventory main = ShopInteract.inv;
    private HashMap<User, List<Inventory>> prev = new HashMap<>();

    @EventHandler
    void onClose(InventoryCloseEvent e) {
        if(e.getView().getTitle().equalsIgnoreCase("§a§lSell/Buy.")) sets.remove(Miniverse.getUser(e.getPlayer().getName()));
    }

    @EventHandler
    void onClick(InventoryClickEvent e) {
        double sell = 0, buy = 0;
        if(e.getSlot() <=-1
                || e.getClickedInventory()==null
                ||e.getClickedInventory().getType() == InventoryType.CREATIVE
                || e.getClickedInventory().getType() == InventoryType.PLAYER
                || e.getCurrentItem() == null
                || !e.getCurrentItem().hasItemMeta()) return;
        User u = Miniverse.getUser(e.getWhoClicked().getName());
        if(u == null) return;

        MiniShop gui1 = MiniShopHandler.getShop(e.getView().getTitle());
        if(!sets.containsKey(u)&&gui1==null) return;

        if(users.contains(u)) return;
        if(!users.contains(u)) users.add(u);
        (new BukkitRunnable() {
            @Override
            public void run() {
                users.remove(u);
            }
        }).runTaskLater(MiniverseCore.getAPI(), 1);
        if(sets.containsKey(u)) {
            if(e.getSlot() >= e.getClickedInventory().getSize()+-9)e.setCancelled(true);
            MiniShop i = sets.get(u);
            Config cfg = i.getConfig();
            if(!e.getCurrentItem().hasItemMeta())return;
            if(cfg.getSection("Items") != null) {
                for (String im : cfg.getSection("Items").getKeys(false)) {
                    ItemStack item = e.getCurrentItem();
                    String display = item.getItemMeta().getDisplayName();
                    Material mat = MiniItem.getItem(im).getType();
                    if(mat == null) continue;
                    String format = StringUtils.getNameFromEnum(mat);
                    List<String> lore;
                    if(item.hasItemMeta() && item.getItemMeta().hasLore())
                        lore = item.getItemMeta().getLore();
                    else lore = new ArrayList<>();
                    sell = cfg.getDouble("Items." + im + ".sellCost");
                    buy = cfg.getDouble("Items." + im + ".buyCost");
                    if(cfg.get("Items." + im + ".buyCost") == null || cfg.get("Items." + im + ".buyCost") == "") {
                        buy = sell * 2;
                    }
                    if(cfg.get("Items." + im + ".sellCost") == null || cfg.get("Items." + im + ".sellCost") == "") {
                        sell = buy / 2;
                    }
                    if(cfg.get("Items." + im + ".buyCost") == null || cfg.get("Items." + im + ".buyCost") == ""
                            && cfg.get("Items." + im + ".sellCost") == null || cfg.get("Items." + im + ".sellCost") == "") {
                        buy = 100;
                        sell = buy / 2;
                    }
                    if(e.getSlot() >= e.getClickedInventory().getSize()+-9 &&
                            (!display.contains(StringUtils.translate("[c]&lBack")) || !display.contains(StringUtils.translate("[c]&lNext")))){
                        e.setCancelled(true);
                        return;
                    }
                    e.setCancelled(true);
                    boolean isBuy = !lore.contains("§cNot Buyable"),
                            isSell = !lore.contains("§cNot Sellable");

                    if(mat == EconomyHelper.Center.getType()) {
                        if(display.contains(StringUtils.translate("[c]&lBack"))) {
                            if(i == null || i.getInventory() == null || !sets.containsKey(u)) {
                                u.getBase().openInventory(ShopInteract.inv);
                                u.getBase().openInventory(ShopInteract.inv);
                                u.getBase().openInventory(ShopInteract.inv);
                                e.setCancelled(true);
                                break;
                            }
                            u.getBase().openInventory(i.getInventory());
                            u.getBase().openInventory(i.getInventory());
                            u.getBase().openInventory(i.getInventory());
                            e.setCancelled(true);
                            sets.remove(u);
                            break;
                        } else if(display.contains(StringUtils.translate("[c]Buy 1 [pc]" + format))) {
                            EconomyHelper.buyItem(u, 1, buy, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            break;
                        } else if(display.contains(StringUtils.translate("[c]Sell 1 [pc]" + format))) {
                            EconomyHelper.sellItem(u, 1, sell, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            break;
                        } else if(display.contains(StringUtils.translate("[c]Buy 32 [pc]" + format))) {
                            EconomyHelper.buyItem(u, 32, buy, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            break;

                        } else if(display.contains(StringUtils.translate("[c]Sell 32 [pc]" + format))) {
                            EconomyHelper.sellItem(u, 32, sell, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            e.setCancelled(true);
                            break;

                        } else if(display.equalsIgnoreCase(StringUtils.translate("[c]Buy 64 [pc]" + format))) {
                            EconomyHelper.buyItem(u, 64, buy, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            break;
                        } else if(display.contains(StringUtils.translate("[c]Sell 64 [pc]" + format))) {
                            EconomyHelper.sellItem(u, 64, sell, mat);
                            EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                            e.setCancelled(true);
                            break;

                        } else if(display.contains(StringUtils.translate("[c]Sell/Buy all [pc]" + format))) {
                            if(e.isRightClick() && isBuy) {
                                EconomyHelper.buyAll(u, sell, mat);
                                EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                                e.setCancelled(true);
                                break;
                            } else if(e.isLeftClick() && isSell) {
                                EconomyHelper.sellAll(u, sell, mat);
                                EconomyHelper.update(e.getClickedInventory(), u, isSell, isBuy, mat, sell, buy);
                                e.setCancelled(true);
                                e.setCancelled(true);
                                break;
                            }else if(!isBuy || !isSell){
                                String sb = !isSell & !isBuy ? "Sellable or Buyable" : !isSell ? "Sellable" : "Buyable";
                                u.sendMessage("[c]This item is not " + sb);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            return;
        }
        MiniShop gui = MiniShopHandler.getShop(e.getView().getTitle());
        if(gui == null) return;
        if(e.getSlot() >= e.getClickedInventory().getSize()+-9)e.setCancelled(true);
        Config cfg = gui.getConfig();
        ItemStack item = e.getCurrentItem();
        String display = item.getItemMeta().getDisplayName();
        List<String> lore;
        if(item.hasItemMeta() && item.getItemMeta().hasLore())
            lore = item.getItemMeta().getLore();
        else lore = new ArrayList<>();
        if(display.contains(StringUtils.translate("[c]&lBack"))) {
            if((!gui.hasNext() && !gui.isNext())
                    || lore.contains(StringUtils.translate("[c]Return to Main page")) || !prev.containsKey(u)) {
                u.getBase().openInventory(ShopInteract.inv);
                u.getBase().openInventory(ShopInteract.inv);
                e.setCancelled(true);
                if(lore.contains(StringUtils.translate("[c]Return to Main page"))) {
                    prev.remove(u);
                }
            } else {
                u.getBase().openInventory(prev.get(u).get(prev.get(u).size()+-1));
                e.setCancelled(true);
                List<Inventory> invs = prev.get(u);
                invs.remove(invs.size()+-1);
                prev.put(u, invs);
            }
            return;
        }
        if(display.contains(StringUtils.translate("[c]&lNext"))
                &&lore.contains(StringUtils.translate("[c]Go to the next page"))
                &&e.getSlot()>=e.getClickedInventory().getSize()+-9) {
            u.getBase().openInventory(gui.getNext().getInventory());
            e.setCancelled(true);
            List<Inventory> invs = prev.containsKey(u) ? prev.get(u) : new ArrayList<>();
            invs.add(gui.getInventory());
            prev.put(u, invs);
            return;
        }
        Inventory i = null;
        for (String im : cfg.getSection("Items").getKeys(false)) {

            Material mat = MiniItem.getItem(im).getType();
            if(mat == null) {
                continue;
            }
            String format = StringUtils.getNameFromEnum(mat);
            if(StringUtils.stripColor(display).equalsIgnoreCase(StringUtils.stripColor(format))) {
                sell = cfg.getDouble("Items." + im + ".sellCost")+0;
                buy = cfg.getDouble("Items." + im + ".buyCost");
                i = EconomyHelper.createGUI(u, !lore.contains("§cNot Buyable"), !lore.contains("§cNot Sellable"), mat, sell, buy);
                break;
            }
        }
        Material mat = item.getType();
        String format = StringUtils.getNameFromEnum(mat);
        if(StringUtils.stripColor(display).equalsIgnoreCase(StringUtils.stripColor(format))) {
            if(i == null) {
                sell = cfg.getDouble("Items." + mat.name() + ".sellCost");
                buy = cfg.getDouble("Items." + mat.name() + ".buyCost");
                i = EconomyHelper.createGUI(u, !lore.contains("§cNot Buyable"), !lore.contains("§cNot Sellable"), mat, sell, buy);
            }
            u.getBase().openInventory(i);
            sets.put(u, gui);
        }
    }


}
