package net.miniverse.listeners;

import net.miniverse.core.Miniverse;
import net.miniverse.core.events.user.UserActionEvent;
import net.miniverse.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

@SuppressWarnings("all")
public class AfkEvents implements Listener {
    @EventHandler
    void onChat(AsyncPlayerChatEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if (u.isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            u.toggleAFK();
        }
        u.startAFKTimer();
    }

    @EventHandler
    void onCommand(PlayerCommandPreprocessEvent e) { User u = Miniverse.getUser(e.getPlayer());
        if (u.isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            u.toggleAFK();
        }
        u.startAFKTimer();
    }

    @EventHandler
    void onOpen(InventoryOpenEvent e) { User u = Miniverse.getUser(e.getPlayer());
        if (u.isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            u.toggleAFK();
        }
        u.startAFKTimer();
    }

    @EventHandler
    void onClick(InventoryClickEvent e) { User u = Miniverse.getUser(e.getWhoClicked());
        if (u.isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            u.toggleAFK();
        }
        u.startAFKTimer();
    }

    @EventHandler
    void onClose(InventoryCloseEvent e) { User u = Miniverse.getUser(e.getPlayer());
        if (u.isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            u.toggleAFK();
        }
        u.startAFKTimer();
    }


    @EventHandler
    void onAction(UserActionEvent e) {
        User u = e.getUser();
        if (u.getBase().getNearbyEntities(0.2D, 1.0D, 0.2D).size() > 0) return;
        if (e.getUser().isAFK()) {
            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                    Miniverse.broadCastPerm(u, "afkAnnounce", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                } else {
                    Miniverse.broadCastPerm(u, "", "[pc]"+u.getName()+"[c] Is no longer AFK");
                    u.sendMessage("[c]You are no longer AFK");
                }
            } else {
                u.sendMessage("[c]You are no longer AFK");
            }
            e.getUser().toggleAFK();
        }
        e.getUser().startAFKTimer();
    }
}
