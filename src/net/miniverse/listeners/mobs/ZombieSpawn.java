package net.miniverse.listeners.mobs;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ZombieSpawn implements org.bukkit.event.Listener {

    void onZombieSpawn(EntitySpawnEvent e) {
        if ((e.getEntity() instanceof Zombie)) {
            Random r = new Random();
            int chance = r.nextInt(100);
            Zombie z = (Zombie)e.getEntity();
            ItemStack[] armor = null;
            if (chance <= 20) {
                armor = new ItemStack[] { new ItemStack(Material.GOLDEN_BOOTS), new ItemStack(Material.GOLDEN_LEGGINGS), new ItemStack(Material.GOLDEN_CHESTPLATE), new ItemStack(Material.GOLDEN_HELMET) };

                for (int i = 0; i < armor.length; i++) {
                    ItemStack a = armor[i];
                    ItemMeta im = a.getItemMeta();
                    im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
                    im.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1, true);
                    im.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
                    im.addEnchant(Enchantment.PROTECTION_FALL, 1, true);
                    im.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
                    a.setItemMeta(im);
                    armor[i] = a;
                }
                z.setBaby(false);
                z.setCustomName(StringUtils.translate("[pc]Common[c] Zombie"));
                z.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999, 1, true));
                z.setMaxHealth(z.getMaxHealth() * 2.0D);
            } else if (chance <= 50) {
                armor = new ItemStack[] { new ItemStack(Material.IRON_BOOTS), new ItemStack(Material.IRON_LEGGINGS), new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_HELMET) };

                for (int i = 0; i < armor.length; i++) {
                    ItemStack a = armor[i];
                    ItemMeta im = a.getItemMeta();
                    im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
                    im.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 3, true);
                    im.addEnchant(Enchantment.PROTECTION_PROJECTILE, 3, true);
                    im.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
                    im.addEnchant(Enchantment.PROTECTION_FIRE, 3, true);
                    a.setItemMeta(im);
                    armor[i] = a;
                }
                z.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999, 2, true));
                z.setCustomName(StringUtils.translate("[pc]Rare[c] Zombie"));
                z.setMaxHealth(z.getMaxHealth() * 3.0D);
            }
            else if (chance <= 60) {
                armor = new ItemStack[] { new ItemStack(Material.DIAMOND_BOOTS), new ItemStack(Material.DIAMOND_LEGGINGS), new ItemStack(Material.DIAMOND_CHESTPLATE), new ItemStack(Material.DIAMOND_HELMET) };

                for (int i = 0; i < armor.length; i++) {
                    ItemStack a = armor[i];
                    ItemMeta im = a.getItemMeta();
                    im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
                    im.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 5, true);
                    im.addEnchant(Enchantment.PROTECTION_PROJECTILE, 5, true);
                    im.addEnchant(Enchantment.PROTECTION_FALL, 5, true);
                    im.addEnchant(Enchantment.PROTECTION_FIRE, 5, true);
                    a.setItemMeta(im);
                    armor[i] = a;
                }
                z.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999, 3, true));
                z.setCustomName(StringUtils.translate("[pc]Ultra[c] Zombie"));
                z.setMaxHealth(z.getMaxHealth() * 4.0D);
            } else {
                z.setCustomName(StringUtils.translate("[pc]Regular[c] Zombie"));
            }
            if (armor != null)
                z.getEquipment().setArmorContents(armor);
            z.setHealth(z.getMaxHealth());
            z.setCustomNameVisible(true);
        }
    }

    void onKill(EntityDeathEvent e)
    {
        if ((e.getEntity() instanceof Zombie)) {
            String name = e.getEntity().getName();
            if (!name.equalsIgnoreCase(StringUtils.translate("[pc]Regular[c] Zombie"))) {
                e.getDrops().clear();
                e.getDrops().addAll(Arrays.asList(e.getEntity().getEquipment().getArmorContents()));
            }
        }
    }
}
