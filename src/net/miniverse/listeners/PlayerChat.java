package net.miniverse.listeners;

import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.List;
import net.miniverse.command.MiniChatCommand;
import net.miniverse.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat
  extends MiniChatCommand
{
  public PlayerChat()
  {
    super("woot", new String[] { "kek" });
  }
  
  public void execute(User u, AsyncPlayerChatEvent e, String cl, String[] args)
  {
    e.getPlayer().sendMessage("Hola");
  }
  
  public List<String> onTab(User u, String cl, String[] args)
  {
    List<String> a = Arrays.asList(new String[] { "take", "lake" });
    List<String> f = Lists.newArrayList();
    if (args.length == 1) {
      for (String e : a) {
        f.add(e);
      }
      return f;
    }
    return super.onTab(u, cl, args);
  }
}
