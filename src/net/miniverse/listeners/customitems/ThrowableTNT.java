package net.miniverse.listeners.customitems;

import net.miniverse.core.Miniverse;
import net.miniverse.user.User;
import net.miniverse.util.Listener;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ThrowableTNT implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    void onTnt(EntityExplodeEvent e) {
        if(e.getEntity() instanceof TNTPrimed) {
            if(e.getEntity().hasMetadata("fake")){
                e.blockList().clear();
            }
        }
        if(e.isCancelled() || e.blockList() == null || e.blockList().isEmpty()) return;
        e.setCancelled(true);
        for(Block b : e.blockList()) {
            b.breakNaturally();
        }
    }

    @EventHandler
    void onPlace(BlockPlaceEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(e.getItemInHand().hasItemMeta()){
            if(u.getItemInHand() == null || u.getItemInHand().getType().name().contains("AIR") || u.getItemInHand().getType() != Material.TNT) return;
            if(StringUtils.translate(e.getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase(StringUtils.translate("[c]Throwable TNT"))) e.setCancelled(true);
            if(StringUtils.translate(u.getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase(StringUtils.translate("[c]Throwable TNT 2.0"))) e.setCancelled(true);
        }
    }

    @EventHandler
    void onClick(PlayerInteractEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(!e.getAction().name().contains("AIR") || u.getItemInHand() == null || u.getItemInHand().getType().name().contains("AIR") || u.getItemInHand().getType() != Material.TNT) return;
        if(StringUtils.translate(u.getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase(StringUtils.translate("[c]Throwable TNT 2.0"))) {
            TNTPrimed tnt = u.getLocation().getWorld().spawn(u.getLocation(), TNTPrimed.class);
            tnt.setVelocity(u.getLocation().clone().getDirection().multiply(e.getAction().name().contains("LEFT") ? NumberUtil.newRandomDouble(1.2, 2.2) : 0.7));
            tnt.setGravity(false);
            if(u.getGameMode() != GameMode.CREATIVE)
                Miniverse.removeItem(u, u.getItemInHand(), 1);
        }
        if(StringUtils.translate(u.getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase(StringUtils.translate("[c]Throwable TNT"))) {
            TNTPrimed tnt = u.getLocation().getWorld().spawn(u.getLocation(), TNTPrimed.class);
            tnt.setVelocity(u.getLocation().clone().getDirection().multiply(e.getAction().name().contains("LEFT") ? 1.2 : 0.7));
            if(u.getGameMode() != GameMode.CREATIVE)
                Miniverse.removeItem(u, u.getItemInHand(), 1);
        }
    }


}
