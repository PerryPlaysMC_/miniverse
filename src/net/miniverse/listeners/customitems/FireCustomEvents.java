package net.miniverse.listeners.customitems;

import net.minecraft.server.v1_14_R1.BlockPosition;
import net.minecraft.server.v1_14_R1.TileEntityChest;
import net.miniverse.core.MiniverseCore;
import net.miniverse.gui.brewing.BrewingRecipe;
import net.miniverse.user.User;
import net.miniverse.util.GuiUtil;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class FireCustomEvents implements org.bukkit.event.Listener {

    @EventHandler(priority=org.bukkit.event.EventPriority.NORMAL)
    public void PotionListener(InventoryClickEvent e) {
        if ((e.getSlot() <= -1) || (e.getClickedInventory() == null))
            return;
        if (e.getClickedInventory().getType() != InventoryType.BREWING)
            return;
        if (e.getSlot() == 4) return;
        if (((BrewerInventory)e.getInventory()).getIngredient() == null)
            return;
        BrewingRecipe recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
        if (recipe == null)
            return;
        if ((e.getSlot() == 0) || (e.getSlot() == 1) || (e.getSlot() == 2)) {
            recipe.getClock().setPaused(true);
            new BukkitRunnable() {
                public void run() {
                    recipe.getClock().getStand().getInventory().setContents(e.getClickedInventory().getContents());
                    recipe.getClock().getStand().getSnapshotInventory().setContents(e.getClickedInventory().getContents());
                    recipe.getClock().setPaused(false);
                }
            }.runTaskLater(MiniverseCore.getAPI(), 3L);
            return;
        }
        new BukkitRunnable() {
            public void run() {
                BrewingRecipe recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
                if (recipe == null)
                    return;
                if ((recipe.isRunning()) &&
                        (e.getSlot() == 3)) {
                    recipe.stopBrew();
                    e.setCursor(e.getCurrentItem());
                    e.setCurrentItem(null);
                    return;
                }

                recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
                if (recipe == null || recipe.isRunning())
                    return;
                recipe.startBrewing((BrewerInventory)e.getClickedInventory());
            }
        }.runTaskLater(MiniverseCore.getAPI(), 2L);
    }

    @EventHandler(priority= EventPriority.HIGHEST)
    public void potionItemPlacer(InventoryClickEvent e) {
        if ((e.getSlot() <= -1) || (e.getClickedInventory() == null))
            return;
        if (e.getClickedInventory().getType() != InventoryType.BREWING)
            return;
        if (e.getClick() != ClickType.LEFT)
            return;
        if (e.getSlot() == 4) return;
        ItemStack is = e.getCurrentItem();
        ItemStack is2 = e.getCursor().clone();
        if (is2 == null)
            return;
        if ((is2.getType() == Material.AIR) || (is2.getType() == Material.POTION) || (is.getType() == Material.POTION))
            return;
        BrewingRecipe recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
        if ((recipe != null) && (
                (e.getSlot() == 0) || (e.getSlot() == 1) || (e.getSlot() == 2))) {
            recipe.getClock().setPaused(true);
            new BukkitRunnable() {
                public void run() {
                    recipe.getClock().getStand().getInventory().setContents(e.getClickedInventory().getContents());
                    recipe.getClock().getStand().getSnapshotInventory().setContents(e.getClickedInventory().getContents());
                    recipe.getClock().setPaused(false);
                }
            }.runTaskLater(MiniverseCore.getAPI(), 3L);
            return;
        }

        if ((e.getClickedInventory().getItem(e.getSlot()) == null) || (e.getClickedInventory().getItem(e.getSlot()).getType() == Material.AIR)) {
            new BukkitRunnable() {
                public void run() {
                    e.setCursor(is);
                    e.setCurrentItem(is2);
                }
            }.runTaskLater(MiniverseCore.getAPI(), 1L);
        }
        new BukkitRunnable() {
            public void run() {
                BrewingRecipe recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
                if (recipe == null)
                    return;
                if ((recipe.isRunning()) &&
                        (e.getSlot() == 3)) {
                    recipe.stopBrew();
                    e.setCursor(e.getCurrentItem());
                    e.setCurrentItem(null);
                    return;
                }
                recipe = BrewingRecipe.getRecipe((BrewerInventory)e.getClickedInventory());
                if (recipe == null)
                    return;
                recipe.startBrewing((BrewerInventory)e.getClickedInventory());
            }

        }.runTaskLater(MiniverseCore.getAPI(), 2L);
        ((Player)e.getView().getPlayer()).updateInventory();
    }

    @EventHandler
    void onCraft(PrepareItemCraftEvent e) {
        ItemStack flint1 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(1));
        ItemStack flint2 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(2));
        ItemStack flint3 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(3));
        ItemStack flint4 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(4));
        ItemStack flint5 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(5));
        ItemStack flint6 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(6));
        ItemStack flint7 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(7));
        ItemStack flint8 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(8));
        ItemStack flint9 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(9));
        ItemStack flint10 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel " +
                NumberUtil.convertToRoman(10));
        ItemStack magnezium1 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Magnezium and steel I");

        ItemStack magnezium2 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Magnezium and steel II");

        ItemStack magnezium3 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Magnezium and steel III");

        ItemStack magnezium4 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Magnezium and steel IV");

        ItemStack magnezium5 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Magnezium and steel V");

        if (e.getInventory().contains(flint1, 2)) {
            e.getInventory().setResult(flint2);
            return; }
        if (e.getInventory().contains(flint2, 2)) {
            e.getInventory().setResult(flint3);
            return; }
        if (e.getInventory().contains(flint3, 2)) {
            e.getInventory().setResult(flint4);
            return; }
        if (e.getInventory().contains(flint4, 2)) {
            e.getInventory().setResult(flint5);
            return; }
        if (e.getInventory().contains(flint5, 2)) {
            e.getInventory().setResult(flint6);
            return; }
        if (e.getInventory().contains(flint6, 2)) {
            e.getInventory().setResult(flint7);
            return; }
        if (e.getInventory().contains(flint7, 2)) {
            e.getInventory().setResult(flint8);
            return; }
        if (e.getInventory().contains(flint8, 2)) {
            e.getInventory().setResult(flint9);
            return; }
        if (e.getInventory().contains(flint9, 2)) {
            e.getInventory().setResult(flint10);
            return; }
        if (e.getInventory().contains(flint10, 9)) {
            e.getInventory().setResult(magnezium1);
            return; }
        if (e.getInventory().contains(magnezium1, 2)) {
            e.getInventory().setResult(magnezium2);
            return; }
        if (e.getInventory().contains(magnezium2, 2)) {
            e.getInventory().setResult(magnezium3);
            return; }
        if (e.getInventory().contains(magnezium3, 2)) {
            e.getInventory().setResult(magnezium4);
            return; }
        if (e.getInventory().contains(magnezium4, 2)) {
            e.getInventory().setResult(magnezium5);
            return;
        }
    }

    @EventHandler
    void onClick(PlayerInteractEvent e) {
        ItemStack item = e.getItem();
        if ((e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) &&
                (item != null) && (!isContainer(e.getClickedBlock())) &&
                (item.getType() == Material.FLINT_AND_STEEL)) {
            java.util.Random r = new java.util.Random();
            double toGet = r.nextInt(125);
            double amt = 2.5D;
            if ((item.hasItemMeta()) && (item.getItemMeta().getLore() != null)) {
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(1)))) {
                    amt = 2.5D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(2)))) {
                    amt = 5.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(3)))) {
                    amt = 7.5D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(4)))) {
                    amt = 10.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(5)))) {
                    amt = 12.5D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(6)))) {
                    amt = 15.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(7)))) {
                    amt = 17.5D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(8)))) {
                    amt = 20.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(9)))) {
                    amt = 22.5D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Better Flint and Steel " +
                        NumberUtil.convertToRoman(10)))) {
                    amt = 25.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Magnezium and steel I"))) {
                    amt = 50.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Magnezium and steel II"))) {
                    amt = 75.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Magnezium and steel III"))) {
                    amt = 100.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Magnezium and steel IV"))) {
                    amt = 110.0D;
                }
                if (item.getItemMeta().getLore().contains(StringUtils.translate("[c]Magnezium and steel V"))) {
                    amt = 120.0D;
                }
            }
            if (toGet <= amt) {
                e.setCancelled(false);
                return;
            }
            e.setCancelled(true);
        }
    }



    HashMap<Player, Location> loc = new HashMap<>();
    HashMap<Player, GameMode> gm = new HashMap<>();

    @EventHandler
    void onInteract(PlayerInteractEvent e) {
        if ((e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) &&
                (e.getClickedBlock().getType().name().contains("CHEST")) && (!e.getClickedBlock().getType().name().contains("ENDER"))) {
            User u = net.miniverse.core.Miniverse.getUser(e.getPlayer());
            Chest chest = (Chest)e.getClickedBlock().getState();
            if (u.isVanished()) {
                GameMode pre = u.getBase().getGameMode();
                Location l = u.getLocation();
                loc.put(e.getPlayer(), l);
                u.setGamemode(GameMode.SPECTATOR);
                gm.put(e.getPlayer(), pre);
                new BukkitRunnable() {
                    public void run() {
                        FireCustomEvents.closeChest(chest);
                    }
                }.runTaskLater(MiniverseCore.getAPI(), 10L);
                return;
            }
        }
    }

    @EventHandler(priority=org.bukkit.event.EventPriority.LOWEST)
    void onClick(InventoryClickEvent e) {
        User u = net.miniverse.core.Miniverse.getUser(e.getWhoClicked().getName());
        if ((u.isVanished()) && (u.getGameMode() == GameMode.SPECTATOR))
            e.setCancelled(false);
    }

    public static void closeChest(Chest chest) {
        Location location = chest.getLocation();
        net.minecraft.server.v1_14_R1.World world = ((CraftWorld)location.getWorld()).getHandle();
        BlockPosition position = new BlockPosition(location.getX(), location.getY(), location.getZ());
        TileEntityChest tileChest = (TileEntityChest)world.getTileEntity(position);
        world.playBlockAction(position, tileChest.getBlock().getBlock(), 1, 0);
    }

    public static void forceOpenChest(Chest chest) {
        Location location = chest.getLocation();
        net.minecraft.server.v1_14_R1.World world = ((CraftWorld)location.getWorld()).getHandle();
        BlockPosition position = new BlockPosition(location.getX(), location.getY(), location.getZ());
        TileEntityChest tileChest = (TileEntityChest)world.getTileEntity(position);
        world.playBlockAction(position, tileChest.getBlock().getBlock(), 1, 1);
    }

    @EventHandler
    void playerClick(InventoryCloseEvent e) {
        new BukkitRunnable() {
            public void run() {
                if (gm.containsKey(e.getPlayer())) {
                    e.getPlayer().teleport(loc.get(e.getPlayer()));
                    loc.remove(e.getPlayer());
                    e.getPlayer().setGameMode(gm.get(e.getPlayer()));
                    gm.remove(e.getPlayer());
                }
            }
        }.runTaskLater(MiniverseCore.getAPI(), 1L);
    }

    List<Block> getNearbyBlocks(Block location, int radius) {
        List<Block> blocks = new ArrayList<>();
        for (int x = location.getX() - radius; x <= location.getX() + radius; x++) {
            for (int y = location.getY() - radius; y <= location.getY() + radius; y++) {
                for (int z = location.getZ() - radius; z <= location.getZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }

    boolean isContainer(Block b) {
        return (b.getType() == Material.CRAFTING_TABLE) || (b.getType() == Material.DISPENSER) ||
                (b.getType() == Material.DROPPER) || (b.getType() == Material.CHEST) || (b.getType() == Material.ANVIL) ||
                (b.getType() == Material.TRAPPED_CHEST) || (b.getType() == Material.HOPPER) ||
                (b.getType() == Material.ENCHANTING_TABLE) || (b.getType() == Material.BEACON) ||
                (b.getType().name().contains("DOOR"));
    }
}
