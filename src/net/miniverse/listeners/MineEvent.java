package net.miniverse.listeners;

import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.ItemUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
@SuppressWarnings("all")
public class MineEvent implements org.bukkit.event.Listener {

    public boolean editBlock(Block newB, Location start, Location loc, Material type, User u, org.bukkit.inventory.ItemStack it) {
        if ((!u.getBase().isSneaking()) || ((it.getType().getMaxDurability() > 0) && (it.getDurability() >= it.getType().getMaxDurability() + -2))) return false;
        Object ir; if ((isOre(newB)) && (newB.getType() != Material.AIR)) {
        if (newB.getType() != type) return false;
        if (it.containsEnchantment(Enchantment.SILK_TOUCH)) {
            loc.getWorld().dropItemNaturally(start, new org.bukkit.inventory.ItemStack(newB.getType()));
        } else {
            for (org.bukkit.inventory.ItemStack ia : newB.getDrops()) {
                if (!ia.getType().isBlock()) {
                    ia.setAmount(net.miniverse.util.NumberUtil.getNumDrops(it));
                    ExperienceOrb orb = (ExperienceOrb)loc.getWorld().spawn(loc, ExperienceOrb.class);
                    orb.setExperience(new Random().nextInt(10) + 105);
                } else { ia.setAmount(1); }
                loc.getWorld().dropItemNaturally(start, ia);
            }
        }
        ir = CraftItemStack.asNMSCopy(it);
        if (it.getType().getMaxDurability() > 0) {
            ((net.minecraft.server.v1_14_R1.ItemStack)ir).damage(1, u.getCraftPlayer().getHandle(), null);
            it.setDurability((short)((net.minecraft.server.v1_14_R1.ItemStack)ir).getDamage());
        }
        if ((it.getType().getMaxDurability() > 0) && (it.getDurability() >= it.getType().getMaxDurability() + -2)) {
            return false;
        }
        u.updateInventory();
        start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, newB.getBlockData());
        newB.setType(Material.AIR);
        newB.getState().update(true);
        return true;
    }
        if (newB.getType().name().contains("LEAVES")) {
            if (newB.getType() != type) return false;
            if ((isTool(it)) && (it.containsEnchantment(Enchantment.SILK_TOUCH))) {
                loc.getWorld().dropItemNaturally(start, new org.bukkit.inventory.ItemStack(newB.getType()));
            } else {
                for (ir = newB.getDrops(it).iterator(); ((Iterator)ir).hasNext();) { org.bukkit.inventory.ItemStack ia = (org.bukkit.inventory.ItemStack)((Iterator)ir).next();
                    Random dropChance = new Random();

                    loc.getWorld().dropItemNaturally(start, ia);
                }
            }
            ir = CraftItemStack.asNMSCopy(it);
            if (it.getType().getMaxDurability() > 0) {
                ((net.minecraft.server.v1_14_R1.ItemStack)ir).damage(1, u.getCraftPlayer().getHandle(), null);
                it.setDurability((short)((net.minecraft.server.v1_14_R1.ItemStack)ir).getDamage());
            }
            if ((it.getType().getMaxDurability() > 0) && (it.getDurability() == it.getType().getMaxDurability() + -2)) {
                return false;
            }
            u.updateInventory();
            start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, newB.getBlockData());
            newB.setType(Material.AIR);
            newB.getState().update(true);
            return true;
        }
        if ((newB.getType() != Material.AIR) && (isWood(newB.getType()))) {
            for (ir = newB.getDrops(it).iterator(); ((Iterator)ir).hasNext();) { org.bukkit.inventory.ItemStack ia = (org.bukkit.inventory.ItemStack)((Iterator)ir).next();
                loc.getWorld().dropItemNaturally(start, ia);
            }
            if ((isWood(newB.getType())) || (isOre(newB))) {
                start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, newB.getBlockData());
                newB.setType(Material.AIR);
                newB.getState().update(true);
            }
            return true;
        }
        for (ItemStack ia : newB.getDrops(it)) {
            loc.getWorld().dropItemNaturally(start, ia);
        }
        start.getWorld().spawnParticle(Particle.BLOCK_DUST, newB.getLocation(), 20, newB.getBlockData());
        newB.setType(Material.AIR);
        newB.getState().update(true);
        return true;
    }

    List<User> users = new ArrayList<>();

    @EventHandler
    void onShiftClick(PlayerInteractEvent e) { final User u = Miniverse.getUser(e.getPlayer());
        if (u.getItemInHand().getType() == Material.BOW) return;
        if (((!u.getItemInHand().getType().isBlock()) || (u.getItemInHand().getType() == Material.AIR)) && (e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_AIR) && (u.getBase().isSneaking())) {
            if (!users.contains(u))
                users.add(u); else {
                return;
            }
            new BukkitRunnable()
            {
                public void run()
                {
                    if (!u.getBase().isSneaking()) {
                        cancel();
                        users.remove(u);
                        return;
                    }
                    u.toggleVeinMine();
                    u.sendMessage(new String[] { "[c]VeinMine " + (u.isVeinMine() ? "Enabled" : "Disabled") });
                    users.remove(u);
                }
            }.runTaskLater(MiniverseCore.getAPI(), 60L);
        }
    }

    @EventHandler
    void onMine(final BlockBreakEvent e) {
        Block b = e.getBlock();
        final Material type = b.getType();
        if (e.getPlayer().isSneaking()) {
            if ((e.getPlayer().getGameMode() == org.bukkit.GameMode.CREATIVE) && (e.getPlayer().getInventory().getItemInMainHand().getType() != Material.GOLDEN_AXE)) return;
            final Location loc = b.getLocation();
            Material item = e.getPlayer().getItemInHand().getType();
            final org.bukkit.inventory.ItemStack it = e.getPlayer().getItemInHand();
            int r = 1;
            Player p = e.getPlayer();
            final User u = Miniverse.getUser(p);
            if (!u.isVeinMine()) return;
            Location l = e.getBlock().getLocation();
            List<Material> allowedMaterials = new ArrayList<>();
            allowedMaterials.addAll(Arrays.asList(new Material[] { Material.ACACIA_LOG, Material.BIRCH_LOG, Material.DARK_OAK_LOG, Material.JUNGLE_LOG, Material.OAK_LOG, Material.SPRUCE_LOG, Material.STRIPPED_ACACIA_LOG, Material.STRIPPED_BIRCH_LOG, Material.STRIPPED_DARK_OAK_LOG, Material.STRIPPED_JUNGLE_LOG, Material.STRIPPED_OAK_LOG, Material.STRIPPED_SPRUCE_LOG, Material.ACACIA_WOOD, Material.BIRCH_WOOD, Material.DARK_OAK_WOOD, Material.JUNGLE_WOOD, Material.OAK_WOOD, Material.SPRUCE_WOOD, Material.ACACIA_LEAVES, Material.BIRCH_LEAVES, Material.DARK_OAK_LEAVES, Material.JUNGLE_LEAVES, Material.OAK_LEAVES, Material.SPRUCE_LEAVES }));





            int max = -1;
            if ((item.name().contains("PICKAXE")) || (item.name().contains("SHOVEL"))) {
                allowedMaterials.clear();
            }
            if ((item.name().contains("SHOVEL")) || (item == Material.AIR)) {
                allowedMaterials.add(Material.GRAVEL);
                if (e.getBlock().getType() == Material.GRAVEL)
                    max = 64;
                allowedMaterials.add(Material.SAND);
                if (e.getBlock().getType() == Material.SAND)
                    max = 64;
            }
            if ((item == Material.WOODEN_PICKAXE) || (item == Material.GOLDEN_PICKAXE) || (item == Material.STONE_PICKAXE) || (item == Material.IRON_PICKAXE) || (item == Material.DIAMOND_PICKAXE)) {
                allowedMaterials.add(Material.COAL_ORE);
                allowedMaterials.add(Material.LAPIS_ORE);
                allowedMaterials.add(Material.NETHER_QUARTZ_ORE);
            }
            if ((item == Material.STONE_PICKAXE) || (item == Material.IRON_PICKAXE) || (item == Material.DIAMOND_PICKAXE)) {
                allowedMaterials.add(Material.IRON_ORE);
            }
            if ((item == Material.IRON_PICKAXE) || (item == Material.DIAMOND_PICKAXE)) {
                allowedMaterials.add(Material.GOLD_ORE);
                allowedMaterials.add(Material.DIAMOND_ORE);
                allowedMaterials.add(Material.EMERALD_ORE);
                allowedMaterials.add(Material.REDSTONE_ORE);
            }
            if (allowedMaterials.contains(b.getType())) {
                Set<Block> bs = getOres(max, b, allowedMaterials);
                final List<Block> blocks = Arrays.asList(bs.toArray(new Block[bs.size()]));
                if ((blocks == null) || (blocks.size() == 0)) {
                    return;
                }
                int erval = Miniverse.getSettings().getInt("VeinMine.Interval");
                new BukkitRunnable() {
                    int i = 0;
                    int blocksPer = Miniverse.getSettings().getInt("VeinMine.BlocksPerInterval");

                    public void run() {
                        if ((it.getType().getMaxDurability() > 0) && (it.getDurability() >= it.getType().getMaxDurability() + -2)) {
                            cancel();
                            return;
                        }
                        if ((!u.getBase().isSneaking()) || (it.getDurability() == it.getType().getMaxDurability() + -2)) {
                            cancel();
                            return;
                        }
                        if ((blocks.size() + -1 < i) || (blocks.get(i) == null) || (i > blocks.size() + -1)) {
                            cancel();
                            return;
                        }
                        try {
                            blocks.get(i);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            cancel();
                            return;
                        }
                        if (blocksPer == 1) {
                            editBlock((Block)blocks.get(i), e.getBlock().getLocation(), loc, type, u, it);
                            i += 1;
                            return;
                        }
                        for (int a = 0; a < blocksPer; a++) {
                            if ((!u.getBase().isSneaking()) || (it.getDurability() == it.getType().getMaxDurability() + -2)) {
                                cancel();
                                return;
                            }
                            Block newB = null;
                            try {
                                newB = (Block)blocks.get(i);
                            } catch (ArrayIndexOutOfBoundsException e) {
                                cancel();
                                return;
                            }
                            editBlock(newB, e.getBlock().getLocation(), loc, type, u, it);
                            i += 1;
                        }
                    }
                }.runTaskTimer(MiniverseCore.getAPI(), 0L, erval);
            }
        }
    }

    @EventHandler
    void onPlace(BlockPlaceEvent e) {
        if ((e.getItemInHand().hasItemMeta()) &&
                (e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(StringUtils.translate("[pc]" + StringUtils.getNameFromEnum(e.getItemInHand().getType()) + "[c] Scaffolding")))) {
            Location x = e.getBlockPlaced().getLocation();
            Location a = new Location(x.getWorld(), x.getBlockX(), x.getBlockY(), x.getBlockZ());
            Material type = e.getBlockPlaced().getType();
            org.bukkit.block.data.BlockData bd = e.getBlockPlaced().getBlockData();
            a.getBlock().setType(Material.AIR);
            a.getBlock().getState().update(true, false);
            Location b = distance(a.clone());
            for (int i = 0; i < 255; i++)
                if (b.getBlock().getType().isSolid()) {
                    MiniverseCore.getAPI().debug(new Object[] { "This block is solid, skipping... ", "X: " + b
                            .getBlockX() + " Y: " + b.getBlockY() + " Z: " + b.getBlockZ() });
                    b = b.add(0.0D, 1.0D, 0.0D);
                }
                else {
                    if (b.getBlockY() == a.getBlockY()) {
                        MiniverseCore.getAPI().debug(new Object[] { "Ending loop... " + b.getY() + " ae< >a " + a.getY() });
                        b.getBlock().setType(type);
                        b.getBlock().setBlockData(bd);
                        b.getBlock().getState().update(true);
                        MiniverseCore.getAPI().debug(new Object[] { "Block at: " + b.getY() + " is being changed..." });
                        break;
                    }
                    b.getBlock().setType(type);
                    b.getBlock().setBlockData(bd);
                    b.getBlock().getState().update(true);
                    MiniverseCore.getAPI().debug(new Object[] { "Block at: " + b.getY() + " is being changed..." });
                    b = b.add(0.0D, 1.0D, 0.0D);
                }
        }
    }

    Location distance(Location top) {
        Location bottom = top;

        for (int i = 0; i < 256; i++) {
            if ((bottom.subtract(0.0D, 1.0D, 0.0D).getBlock().getType().isSolid()) || (bottom.getBlock().getType().isSolid())) {
                break;
            }
            bottom = bottom.subtract(0.0D, 1.0D, 0.0D);
        }

        return bottom;
    }

    boolean isOre(Block b) {
        return isOre(b.getType());
    }

    boolean isWood(Material m) {
        return ((m.name().contains("LOG")) || (m.name().contains("WOOD"))) && (!m.name().contains("PLANKS"));
    }

    boolean isNotSolid(Material m) {
        return (m == Material.COAL_ORE) || (m == Material.DIAMOND_ORE) || (m == Material.LAPIS_ORE) || (m == Material.EMERALD_ORE) || (m == Material.REDSTONE_ORE) || (m == Material.NETHER_QUARTZ_ORE);
    }





    boolean isOre(Material m)
    {
        return m.name().endsWith("_ORE");
    }

    @EventHandler
    void onSpawnerPlace(BlockPlaceEvent e) {
        if (e.getBlockPlaced().getType() == Material.SPAWNER) {
            ItemUtil u = ItemUtil.getData(e.getItemInHand());
            if (!u.hasKey("type")) return;
            CreatureSpawner s = (CreatureSpawner)e.getBlockPlaced().getState();
            s.setSpawnedType(EntityType.valueOf(u.getString("type").toUpperCase()));
            s.getBlock().getState().update(true);
            s.update(true);
            e.getBlockPlaced().setBlockData(s.getBlockData());
            e.getBlockPlaced().getState().update(true);
        }
    }

    @EventHandler
    void onSpawnerBreak(BlockBreakEvent e) {
        User us = Miniverse.getUser(e.getPlayer());
        if ((e.getBlock().getType() == Material.SPAWNER) && (us.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH))) {
            CreatureSpawner s = (CreatureSpawner)e.getBlock().getState();
            s.getSpawnedType();
            Random r = new Random();
            int chance = r.nextInt(100) + 1;
            if (chance <= Miniverse.getSettings().getInt("Spawner.Chance")) {
                org.bukkit.inventory.ItemStack i = new org.bukkit.inventory.ItemStack(Material.SPAWNER);
                ItemUtil u = ItemUtil.setItem(i);
                String type = StringUtils.getNameFromEnum(s.getSpawnedType());
                u.setDisplayName("§c" + type + " §cSpawner").setLore(new String[] { "&eInfo:", " §2Type: &b" + type })
                        .setString("type", s.getSpawnedType().name());
                i = u.finish();
                us.getLocation().getWorld().dropItemNaturally(e.getBlock().getLocation(), i);
            }
        }
    }

    public Set<Block> getOres(int max, Block start, List<Material> allowedMaterials) {
        return getOres(max, start, allowedMaterials, new HashSet<>());
    }

    private Set<Block> getOres(int max, Block start, List<Material> allowedMaterials, Set<Block> blocks) {
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {
                for (int z = -1; z < 2; z++) {
                    if ((blocks.size() >= max) && (max != -1)) {
                        return blocks;
                    }
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if ((!blocks.contains(block)) && (allowedMaterials.contains(block.getType())) && (block.getType() == start.getType())) {
                        blocks.add(block);
                        if ((blocks.size() >= max) && (max != -1)) {
                            return blocks;
                        }
                        try {
                            String a = getOres(max, block, allowedMaterials, blocks).size() + "/" + blocks.size();
                            blocks.addAll(getOres(max, block, allowedMaterials, blocks));
                        }
                        catch (Exception localException) {}
                    }
                }
            }
        }
        return blocks;
    }


    public static List<Block> select(Location loc1, Location loc2)
    {
        List<Block> blocks = new ArrayList<>();

        int x1 = loc1.getBlockX();int y1 = loc1.getBlockY();int z1 = loc1.getBlockZ();
        int x2 = loc2.getBlockX();int y2 = loc2.getBlockY();int z2 = loc2.getBlockZ();
        int xMax, xMin;
        int zMin, zMax;
        int yMin, yMax;
        if (x1 > x2) {
            xMin = x2;
            xMax = x1;
        } else {
            xMin = x1;
            xMax = x2; }
        if (y1 > y2) {
            yMin = y2;
            yMax = y1;
        } else {
            yMin = y1;
            yMax = y2;
        }
        if (z1 > z2) {
            zMin = z2;
            zMax = z1;
        } else {
            zMin = z1;
            zMax = z2;
        }

        for (int x = xMin; x <= xMax; x++) {
            for (int y = yMin; y <= yMax; y++) {
                for (int z = zMin; z <= zMax; z++) {
                    Block b = new Location(loc1.getWorld(), x, y, z).getBlock();
                    blocks.add(b);
                }
            }
        }

        return blocks;
    }

    private boolean isTool(org.bukkit.inventory.ItemStack i)
    {
        return (i.getType().name().contains("AXE")) || (i.getType().name().contains("SHOVEL")) || (i.getType().name().contains("SWORD"));
    }
}
