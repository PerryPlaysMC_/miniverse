package net.miniverse;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutPlayerListHeaderFooter;
import net.miniverse.animations.AnimationHeader;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.animate.Animation;
import net.miniverse.animations.AnimationCheatCodeSMP;
import net.miniverse.commands.utility.CommandProfile;
import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.discord.DiscordEvents;
import net.miniverse.core.discord.MiniverseBot;
import net.miniverse.enchantment.CustomEnchant;
import net.miniverse.enchantment.EnchantHandler;
import net.miniverse.enchantment.EnchantTarget;
import net.miniverse.enchantment.Levels;
import net.miniverse.core.events.user.UserSlotChangeEvent;
import net.miniverse.gui.brewing.BrewAction;
import net.miniverse.gui.brewing.BrewingRecipe;
import net.miniverse.recipes.Man;
import net.miniverse.user.User;
import net.miniverse.user.UserBase;
import net.miniverse.util.economy.EconomyHelper;
import net.miniverse.util.economy.EconomyManager;
import net.miniverse.util.GuiUtil;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.ChatPaginator;
import org.bukkit.util.Vector;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.*;

@SuppressWarnings("all")
public class MiniversePlugin extends MiniverseCore implements PluginMessageListener {


    private static MiniversePlugin instance;
    public static Config locations = new Config("locations.yml");
    public Config discord = new Config(this, this.getFolder(), "discord.yml");
    @Override
    public void onEnable() {
        initialize("Miniverse");
        if(Bukkit.getMessenger().getIncomingChannelRegistrations("return:") == null) {
            Bukkit.getMessenger().registerIncomingPluginChannel(this, "return:", this);
        }
        CustomEnchant poison = new CustomEnchant("&aPoison", Levels.LEVELS_25_30, 1, 2, 50, EnchantTarget.WEAPON);
        CustomEnchant grapple = new CustomEnchant("&6Grapple", Levels.LEVELS_25_30, 1, 1, 50, EnchantTarget.BOW);
        CustomEnchant teleport = new CustomEnchant("&5Teleport", Levels.LEVELS_25_30, 1, 1, 50, EnchantTarget.BOW);
        CustomEnchant explosive = new CustomEnchant("&7Explosive", Levels.LEVELS_25_30, 1, 1, 50, EnchantTarget.BOW);

        grapple.addConflicts(explosive, teleport);
        teleport.addConflicts(grapple, explosive);
        explosive.addConflicts(grapple, teleport);
        instance = this;
        addCommands("net.miniverse.commands");
        loadRecipes();

        for(User u : Miniverse.getOnlineUsers()) {
            ComponentBuilder b = new ComponentBuilder("Hey");
            b.event(new ClickEvent(ClickEvent.Action.OPEN_FILE, "/Users/Family/Desktop/Servers/Test/plugins/Miniverse/chat.yml"));
            u.sendMessage(b.create());
        }

        new BrewingRecipe(true, Material.GOLDEN_PICKAXE, (inv, item, ingredient) -> {
                if(!item.hasItemMeta()) return;
                if(item.getType() != Material.POTION) return;
                ItemStack pot = new Potion(PotionType.AWKWARD).toItemStack(1);
                if(pot == null || !item.isSimilar(pot)) return;
                PotionMeta im = (PotionMeta) item.getItemMeta();
                im.addCustomEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, (60 * 5) * 20, 1, true), true);
                im.setLore(null);
                im.setDisplayName(StringUtils.translate("&ePotion of Haste"));
                List<String> l = new ArrayList<>();
                l.add(StringUtils.translate("&eHaste 2(5 min)"));
                im.setLore(l);
                im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                item.setItemMeta(im);
        });
        if(Miniverse.getManager().getPlugin("MiniverseBotPlaceHolder") != null) {
            MiniverseBot.startBot();
            MiniverseBot.addListeners(new DiscordEvents());
            MiniverseBot.addCommands("net.miniverse.core.discord");
            registerListeners(new DiscordEvents());
        }
        registerListeners("net.miniverse.listeners");
        registerListeners(this, new CommandProfile());
        (new BukkitRunnable(){
            String footer="[pc]&m&l             [pc]|&r " + "[c]CheatCode" + " [pc]|[c]&l&m             &r[c]";
            Animation animation = new AnimationHeader(0.02);
            @Override
            public void run() {
                String a = "[pc]", x = "[c]";
                for(User u : getOnline()) {
                    String s1 = "             | $" +u.getBalance()+" |             ";
                    String sp = "";
                    for(int i = 0; i < (s1.length()/2)+-9; i++) sp+=" ";
                    setPlayerList(u.getBase(),
                            "[c]&l&m             &r[pc]|[c] $" +u.getBalance()+" [pc]|[c]&l&m             &r[pc]",
                            EconomyHelper.baltopPageForHeader(u, 1)+"\n"+footer.replace("[s1]", sp).replace("[s2]", sp));
                }
                footer = animation.getCurrentString();
                footer="[c]&m&l[s1][pc]|&r " + footer.replace("[pc]", "[pc]&l").replace("[c]", "&r[c]") + " [pc]|[c]&l&m[s2]&r[c]";
            }
        }).runTaskTimer(this, 0, 4);
        (new BukkitRunnable() {
            public void run() {
                for (String x : MiniversePlugin.locations.getStringList("ChunkLoaders")) {
                    Location a = StringUtils.stringToLocation(x);
                    if (a != null && a.getBlock().getType().name().contains("SIGN")) {
                        Sign s = (Sign)a.getBlock().getState();
                        if ((s.getLine(0).equalsIgnoreCase(StringUtils.translate("[pc]ChunkLoader"))) &&
                                (s.getLine(1).equalsIgnoreCase(StringUtils.translate("[c]Keeps this chunk"))) &&
                                (s.getLine(2).equalsIgnoreCase(StringUtils.translate("[c]loaded ;D"))) &&
                                (a != null)) {
                            if (!a.getChunk().isLoaded()) {
                                a.getChunk().load();
                            }
                            if (!a.getChunk().isForceLoaded())
                                a.getChunk().load(true);
                        }
                    }
                }
            }
        }).runTaskTimer(this, 0L, 20L);
    }



    public void setPlayerList(Player player, String header, String footer){
        IChatBaseComponent hj = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + StringUtils.translate(header) + "\"}");
        IChatBaseComponent fj = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + StringUtils.translate(footer) + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = (PacketPlayOutPlayerListHeaderFooter) constructHeaderAndFooterPacket(hj, fj);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
    }

    private Object constructHeaderAndFooterPacket(Object header, Object footer){
        try{
            Object packet = PacketPlayOutPlayerListHeaderFooter.class.newInstance();
            if(header != null){
                Field field = PacketPlayOutPlayerListHeaderFooter.class.getDeclaredField("header");
                field.setAccessible(true);
                field.set(packet, header);
                field.setAccessible(false);
            }
            if(footer != null){
                Field field = PacketPlayOutPlayerListHeaderFooter.class.getDeclaredField("footer");
                field.setAccessible(true);
                field.set(packet, footer);
                field.setAccessible(false);
            }
            return packet;
        }catch (InstantiationException | IllegalAccessException | NoSuchFieldException e){
            e.printStackTrace();
        }
        return null;
    }



    @EventHandler
    void onPing(ServerListPingEvent e) {
        e.setMotd(StringUtils.translate(Miniverse.getSettings().getString("Info.Motd")).replace("§", "\u00a7"));
        e.setMaxPlayers(Integer.parseInt(Miniverse.getSettings().getString("Info.MaxPlayers").replace("{online}", ""+Miniverse.getOnlineUsers().size()+1)));
    }

    void loadRecipes() {
        ItemStack res = new ItemStack(Material.LEATHER);
        boolean is = false;
        FurnaceRecipe r1 = new FurnaceRecipe(new NamespacedKey("miniverse", UUID.randomUUID()+""),
                res, Material.ROTTEN_FLESH, 10, 20 * 4);
        Bukkit.addRecipe(r1);

        ItemStack x = GuiUtil.createItem(Material.OAK_SIGN, "[c]Chunk Loader", 3);
        ShapedRecipe recipesss = new ShapedRecipe(x);
        recipesss.shape("DDD", "GGG", "III").setIngredient('D', Material.DIAMOND)
                .setIngredient('G', Material.GOLD_INGOT)
                .setIngredient('I', Material.IRON_INGOT);
        Bukkit.addRecipe(recipesss);
        x = GuiUtil.createItem(Material.TNT, "[c]Chunk Loader", 3*9);
        recipesss = new ShapedRecipe(x);
        recipesss.shape("DDD", "GGG", "III").setIngredient('D', Material.DIAMOND_BLOCK)
                .setIngredient('G', Material.GOLD_BLOCK)
                .setIngredient('I', Material.IRON_BLOCK);
        Bukkit.addRecipe(recipesss);

        ItemStack flint1 = GuiUtil.createItem(Material.FLINT_AND_STEEL, 1, "[c]Better Flint and Steel I");
        Man.createRecipe(flint1, Material.FLINT, Material.FLINT, Material.IRON_INGOT);

        ItemStack i = GuiUtil.createItem(Material.TNT, "[c]Throwable TNT", 2);
        ShapedRecipe recipe = new ShapedRecipe(i);
        recipe.shape("   ", "STS", " S ").setIngredient('S', Material.STICK).setIngredient('T', i.getData());
       // Bukkit.addRecipe(recipe);
        ItemStack i2 = GuiUtil.createItem(Material.TNT, "[c]Throwable TNT 2.0", 2);
        i = GuiUtil.createItem(Material.TNT, "[c]Throwable TNT", 1);

        //Man.createRecipe("throwabletnt2", i2, i, i);

        ItemStack result = new ItemStack(Material.STRING, 3);
        Man.createRecipe(result, new ItemStack(Material.WHITE_WOOL, 1));
        ItemStack re = ItemUtil.setItem(new ItemStack(Material.FURNACE)).setInt("speed", 3).setDisplayName("§cUltimate Furnace").setLore("§cSpeed: §e3(Seconds to cook)").finish();
        ShapedRecipe rc = new ShapedRecipe(new NamespacedKey(this, "ultimatefurnace"), re);
        rc.shape("###","#F#","###");
        rc.setIngredient('#', Material.DIAMOND).setIngredient('F', Material.FURNACE);
        Bukkit.addRecipe(rc);
        //Level1
        ItemStack firework = new ItemStack(Material.FIREWORK_ROCKET, 3);
        FireworkMeta im = (FireworkMeta) firework.getItemMeta();
        im.setPower(1);
        firework.setItemMeta(im);
        Recipe r2 = Man.createRecipe("firework1", firework, Material.PAPER, Material.BLAZE_POWDER);
        //Level2
        ItemStack firework2 = new ItemStack(Material.FIREWORK_ROCKET, 3);
        FireworkMeta im2 = (FireworkMeta) firework.getItemMeta();
        im2.setPower(2);
        firework2.setItemMeta(im2);
        Recipe r3 = Man.createRecipe("firework2", firework2, Material.PAPER, Material.BLAZE_POWDER, Material.BLAZE_POWDER);
        //Level3
        ItemStack firework3 = new ItemStack(Material.FIREWORK_ROCKET, 3);
        FireworkMeta im3 = (FireworkMeta) firework.getItemMeta();
        im3.setPower(3);
        firework3.setItemMeta(im3);
        Recipe r4 = Man.createRecipe("firework3", firework3, Material.PAPER, Material.BLAZE_POWDER, Material.BLAZE_POWDER, Material.BLAZE_POWDER);
        //Level4
        ItemStack firework4 = new ItemStack(Material.FIREWORK_ROCKET, 3);
        FireworkMeta im4 = (FireworkMeta) firework.getItemMeta();
        im4.setPower(4);
        firework4.setItemMeta(im4);
        Recipe r5 = Man.createRecipe("firework4", firework4, Material.PAPER, Material.BLAZE_POWDER, Material.BLAZE_POWDER, Material.BLAZE_POWDER, Material.BLAZE_POWDER);
    }

    @EventHandler
    void onAdv(PlayerAdvancementDoneEvent e) {
        if(e.getAdvancement().getKey().getKey().contains("into_fire") || e.getAdvancement().getKey().getKey().contains("obtain_blaze_rod")) {
            e.getPlayer().discoverRecipe(new NamespacedKey("miniverse", "firework1"));
            e.getPlayer().discoverRecipe(new NamespacedKey("miniverse", "firework2"));
            e.getPlayer().discoverRecipe(new NamespacedKey("miniverse", "firework3"));
            e.getPlayer().discoverRecipe(new NamespacedKey("miniverse", "firework4"));
            e.getPlayer().discoverRecipe(new NamespacedKey("miniverse", "ultimatefurnace"));
        }
    }
    List<String> aliases = new ArrayList<>();

    @Override
    public void onPluginMessageReceived(String channel, Player p, byte[] message) {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));

        try {
            String sub = in.readUTF();
            if (sub.contains("command")) {
                String cmd = in.readUTF();
                cmd = (cmd.startsWith("/") ? cmd.replaceFirst("/", "") : cmd);
                Bukkit.getConsoleSender().sendMessage(StringUtils.translate("[p]Recieved command from bungee,'[pc]/" + cmd + "[c]' Executing it now."));
                getServer().dispatchCommand(getServer().getConsoleSender(), cmd);
            }
            //			if (sub.contains("listcommands")) {
            //				String cmd = in.readUTF();
            //				for(String a : cmd.split(",")) {
            //					aliases.add(a);
            //				}
            //			}
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        EnchantHandler.resetEnchantments();
        if(Miniverse.getManager().getPlugin("MiniverseBotPlaceHolder") != null) {
            MiniverseBot.stopBot();
        }
        Bukkit.resetRecipes();
    }

    @EventHandler
    void slotChange(PlayerItemHeldEvent e) {
        if(Miniverse.getUser(e.getPlayer()) != null) {
            UserSlotChangeEvent event = new UserSlotChangeEvent(Miniverse.getUser(e.getPlayer()), e.getNewSlot(), e.getPreviousSlot());
            Bukkit.getPluginManager().callEvent(event);
            e.setCancelled(event.isCancelled());
        }
    }
    private double vel = 0.100000000005;

    @EventHandler
    void slowFall(PlayerMoveEvent e) {
        Player player = e.getPlayer(), p = e.getPlayer();
        User u = Miniverse.getUser(player);
        if(u == null) {
            MiniverseCore.getAPI().addUser(new UserBase(player));
        }
        u = Miniverse.getUser(player);
        if(u != null && u.isSlowFall() && e.getFrom() != null && e.getTo() != null && e.getFrom().getY() > e.getTo().getY()
                && e.getTo().getBlock().getRelative(BlockFace.DOWN).getType() == Material.AIR
                && e.getTo().getBlock().getRelative(BlockFace.DOWN, 1).getType() == Material.AIR
                && e.getTo().getBlock().getRelative(BlockFace.DOWN, 2).getType() == Material.AIR
                && !player.isGliding() && !player.isFlying()) {
            double x = player.getVelocity().getX(), z = player.getVelocity().getZ();
            player.setVelocity(new Vector(x, -0.2, z));
            player.setFallDistance(0);
        }
    }

    public static MiniversePlugin getInstance() {
        return instance;
    }

    public static Config getLocations() {
        return locations;
    }

}
