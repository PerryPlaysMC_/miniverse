package net.miniverse.commands.admin;

import net.miniverse.MiniversePlugin;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.Run;
import org.bukkit.block.Block;
import org.bukkit.entity.TNTPrimed;

public class CommandNuke  extends MiniCommand {

    MiniversePlugin pl = MiniversePlugin.getInstance();

    public CommandNuke() {
        super("nuke");
        desc = "NUKE A PLAYER!";
        permission = "miniverse.command.nuke";
    }
    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("nuke")) return;
        if(args.length == 0) {
            if(!s.isPlayer()) {
                sendSpecifyTarget();
                return;
            }
            Nuke(s.getUser());
            sendCMessage("Nuke.self");
        }else if(args.length == 1) {
            User t = Miniverse.getUser(args[0]);
            if(validTarget(args[0], true) || sendInvalidPermission("nuke.other")) return;
            if(s.getName() == t.getName()) {
                Nuke(s.getUser());
                sendCMessage("Nuke.self");
                return;
            }
            Nuke(t);
            sendCMessage("Nuke.other-self", t.getName());
            if(check("Nuke.send-other")) {
                sendCMessage("Nuke.other-target", s.getName());
            }
        }else {
            sendCMessage("Invalidations.too-many-args");
        }
    }

    void Nuke(User u) {

        int i = 1;
        for(Block b : Miniverse.getEveryOther3Blocks(u.getLocation().clone().add(0, 100, 0), 10)) {
            if(b != null) {
                TNTPrimed tnt = Miniverse.summonTnt(b.getLocation(), 20 * 15, true, false);
                new Run(tnt, 1);
                i++;
            }
        }
    }

}
