package net.miniverse.commands.admin;

import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class CommandJson extends MiniCommand {

    public CommandJson() {
        super("json");
        desc = "It's json...";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        JSONParser parser = new JSONParser();
        String msg = formMessage(0, args.length);
        try {
            s.getBase().sendMessage(formJson(msg));
        } catch (ParseException e) {
            String lookingFor = "";
            if(!msg.endsWith("\"}")) {
                lookingFor = "\"}";
            }
            if(!msg.endsWith("}")) {
                lookingFor = "}";
            }
            s.sendMessage("Error at position " + e.getPosition() + " ->> " + msg.charAt(e.getPosition()+-1),
                    "Looking for: " + lookingFor);
            e.printStackTrace();
        }
    }

    boolean isEmpty(String s) {
        return s == null || s == "";
    }

    String formJson(String msg) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray i = null;
        if (parser.parse(msg) instanceof JSONArray) {
            i = (JSONArray) parser.parse(msg);
        }
        String ba = "";
        if (i == null) {
            JSONObject json = (JSONObject) parser.parse(msg);
            String f = "";
            String color = String.valueOf(json.get("color"));
            if (json.get("color") != null && !isEmpty(color) && color != "NULL") {
                ChatColor c = getColor(color);
                if (c != null) {
                    f += String.valueOf("§" + c.getChar());
                }
            }
            PacketPlayOutChat cae = new PacketPlayOutChat();
            String bold = String.valueOf(json.get("bold"));
            if (json.get("bold") != null && !isEmpty(bold)) {
                if (bold.equalsIgnoreCase("true") || bold.equalsIgnoreCase("yes")) {
                    f += "§l";
                    ba = "§l";
                }else{
                    ba = "";
                    f = f.replace("&l", "").replace("§l", "");
                }
            }
            String text = (String) json.get("text");
            if (!isEmpty(text)) {
                f += String.valueOf(text);
            }else if(json.get("text") == null){
                return "Error: couldn't find object \"text\"";
            }
            JSONArray extra = (JSONArray) json.get("extra");
            if (json.get("extra") != null &&
                    !extra.isEmpty()) {
                f += formJson(extra.toString());
            }
            return f;
        }
        String f = "";
        if(i != null && !i.isEmpty()) {
            for (Object a : i) {
                String b = String.valueOf(a);
                f+= ba + formJson(b);
            }
        }
        return f;
    }

    ChatColor getColor(String str) {
        if(str == null || str == "" || str == "NULL") {
            return ChatColor.WHITE;
        }
        if(ChatColor.valueOf(str.toUpperCase()) == null) {
            return ChatColor.WHITE;
        }
        ChatColor c = ChatColor.valueOf(str.toUpperCase());
        if (c.isColor()) {
            return ChatColor.valueOf(str.toUpperCase());
        }else{
            return ChatColor.WHITE;

        }
    }

}
