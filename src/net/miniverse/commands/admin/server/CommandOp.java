package net.miniverse.commands.admin.server;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;

import java.util.List;

public class CommandOp extends MiniCommand {

    public CommandOp() {
        super("op", "mop", "operator", "moperator");
        desc = "Op the promoted";
        permission = "miniverse.command.op";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) {
            if(!s.isPlayer()) {
                sendSpecifyTarget();
                return;
            }
            if(sendInvalidPermission("op.self")) return;
            if(s.getBase().isOp()) {
                sendCMessage("Op.alreadyOpped-self", s.getName());
                return;
            }
            sendCMessage("Op.selfOp");
            s.getBase().setOp(true);
        } else if(args.length == 1) {
            OfflineUser t = Miniverse.getOfflineUser(args[0]);
            if(validTarget(args[0])) return;
            if(t.getName() == s.getName() && s.isPlayer()) {
                if(t.getBase().isOp()) {
                    sendCMessage("Op.alreadyOpped-self", s.getName());
                    return;
                }
                if(sendInvalidPermission("op.self")) return;
                sendCMessage("Op.selfOp");
                t.getBase().setOp(true);
                return;
            }
            if(t.getBase().isOp()) {
                sendCMessage("Op.alreadyOpped-other", t.getName());
                return;
            }
            t.getBase().setOp(true);
            sendCMessage("Op.other-self", t.getName());
            if(check("Op.other-send")) {
                t.sendMessage(Miniverse.getSettings(), "Commands.Op.other-target", s.getName());
            }
        } else if(args.length > 1) {
            String invalidPlayers = "";
            String alreadyOpped = "";
            String ops = "";
            List<OfflineUser> possibleSends = Lists.newArrayList();
            for (int i = 0; i < args.length; i++) {
                OfflineUser t = Miniverse.getOfflineUser(args[i]);
                if(validate(args[0])) {
                    invalidPlayers += "[c], [pc]" + t.getName();
                    continue;
                }
                if(t.getBase().isOp()) {
                    alreadyOpped += "[c], [pc]" + t.getName();
                    continue;
                }
                if(t.getName() == s.getName() && s.isPlayer()) {
                    ops += "[c], [pc]You";
                    continue;
                }
                t.getBase().setOp(true);
                ops += "[c], [pc]" + t.getName();
            }
            if(!ops.isEmpty()) {
                sendCMessage("Op.others-ops", ops.replaceFirst(", ", ""));
            }
            if(!invalidPlayers.isEmpty()) {
                sendCMessage("Op.others-invalid-ops", invalidPlayers.replaceFirst(", ", ""));
            }
            if(!alreadyOpped.isEmpty()) {
                sendCMessage("Op.others-invalid-already-opped", alreadyOpped.replaceFirst(", ", ""));
            }
            if(check("Op.others-send")) {
                for (OfflineUser t : possibleSends) {
                    t.sendMessage(Miniverse.getSettings(), "Commands.Op.other-target", s.getName());
                }
            }
        }
    }
    @Override@SuppressWarnings("all")
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> a = Lists.newArrayList();
        List<String> f = Lists.newArrayList();
        if(args.length == 0) {
            return a;
        }
        String form = formMessage(0, args.length);
        for(User u : Miniverse.getOnlineUsers()) {
            if(!a.contains(u.getName()) && !u.getBase().isOp() && !form.contains(u.getName())) {
                a.add(u.getName());
            }
        }
        for(OfflineUser u : Miniverse.getOfflineUsers()) {
            if(!a.contains(u.getName()) && !u.getBase().isOp() && !form.contains(u.getName())) {
                a.add(u.getName());
            }
        }
        String last = args[args.length +-1];
        if(args.length > 0) {
            for(String off : a) {
                if(off.toLowerCase().startsWith(last.toLowerCase())) f.add(off);
            }
            return f;
        }
        return null;
    }
}
