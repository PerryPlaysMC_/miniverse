package net.miniverse.commands.admin.server;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.plugin.Manager;
import net.miniverse.user.CommandSource;
import net.miniverse.util.StringUtils;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CommandPlugins extends MiniCommand {
    public static CommandPlugins c;
    public CommandPlugins() {
        super("pl", "plugin", "plugins", "mplugins");
        this.isTab = true;
        this.isOnlineTab = false;
        desc = "Get plugin information";
        permission = "miniverse.command.plugins";
        c = this;
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        Manager m = Miniverse.getManager();
        if(args.length == 0) {
            List<String> plugins = new ArrayList<>();
            List<String> pls = new ArrayList<>();
            for(Plugin pl : m.getPlugins()) {
                if(!pls.contains(pl.getName())) {
                    pls.add(pl.getName());
                }
            }
            Message msg = new Message("[c]Enabled Plugins ([pc]" + m.getPluginSize() +
                                      "[c]/[pc]" + m.getMiniversePlugins().size() + "[c]): &7&o[Hover for more info!]");
            msg.tooltip("[c]Total Plugins enabled: [pc]" + m.getPluginSize() + "\n" +
                        "[c]Miniverse plugins enabled: [pc]" + m.getMiniversePlugins().size()+ "\n" +
                        "[c]Hover over plugins to get more info!");
            msg.then("\n");
            Collections.sort(pls);
            for(int i = 0; i < pls.size(); i++) {
                Plugin pl = m.getPlugin(pls.get(i));
                if(!plugins.contains(pl.getName())) {
                    plugins.add(pl.getName());
                    PluginDescriptionFile d = pl.getDescription();
                    String author = "None", web = "None";
                    if (d.getAuthors() != null && d.getAuthors().size() > 0) {
                        author = d.getAuthors().toString().replace("[", "").replace("]", "");
                    }
                    if (!StringUtils.isEmpty(d.getWebsite())) {
                        web = d.getWebsite();
                    }
                    msg.then("[pc]" + pl.getName()).tooltip(
                            "[c]Author(s): [pc]" + author,
                            "[c]Version: [pc]" + d.getVersion(),
                            "[c]Main class: [pc]" + d.getMain(),
                            "[c]Website: [pc]" + web,
                            "[c]Is enabled: [pc]" + pl.isEnabled(),
                            "[c]Is Miniverse: [pc]" + (MiniverseCore.getAPI().getPlugin(pl.getName()) != null));
                    if (!StringUtils.isEmpty(d.getWebsite())) {
                        msg.link((web));
                    }
                    if (i < m.getPluginSize()+-1) {
                        msg.then("[c],").then(" ");
                    }else if(i == m.getPluginSize()+-1) {
                        msg.then("[c].");
                    }
                }
            }
            msg.send(s);
            return;
        }
        if(args.length == 1) {
            if(m.getPlugin(args[0]) == null) {
                s.sendMessage("[c]Invalid plugin.");
                return;
            }
            Plugin pl = m.getPlugin(args[0]);
            s.sendMessage("[c]Please provide a sub command");
            s.sendMessage(
                    "[c]Valid commands: ",
                    " [c]-[pc]" + pl.getName() + "[c] load >> [pc]Loads the plugin",
                    " [c]-[pc]" + pl.getName() + "[c] reload >> [pc]Reloads the plugin",
                    " [c]-[pc]" + pl.getName() + "[c] unload >> [pc]Unloads the plugin"
            );
            return;
        }
        if(args.length == 2) {

            Plugin pl = m.getPlugin(args[0]);

            if(args[1].equalsIgnoreCase("reload")) {
                String p = pl.getName();
                String u = m.unload(pl);
                String l = m.load(p);
                s.sendMessage("[pc]MPL[c]: [c]Reloaded '[pc]" + args[0] + "[c]'", "[c]Unload Result: ", u, "[c]Load Result: ", l);
            }else if(args[1].equalsIgnoreCase("load")) {
                if(m.getPlugin(args[0]) != null || (m.getPlugin(args[0]) != null && m.isEnabled(m.getPlugin(args[0])))) {
                    s.sendMessage("[c]That plugin is already enabled.");
                    return;
                }
                String l = m.load(args[0]);
                s.sendMessage("[pc]MPL[c]: [c]Loaded [pc]" + args[0], "[c]Load Result: ", l);
            }else if(args[1].equalsIgnoreCase("unload")) {
                if(pl == null) {
                    s.sendMessage("[c]That plugin is already disabled.");
                    return;
                }
                if((pl != null && !m.isEnabled(pl))) {
                    s.sendMessage("[c]That plugin is already disabled.");
                    return;
                }
                String u = m.unload(pl);
                s.sendMessage("[pc]MPL[c]: [c]Unloaded [pc]" + args[0], "[c]Unload Result: ", u);
            }
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        Manager m = Miniverse.getManager();
        List<String> f = Lists.newArrayList();
        List<String> arguments = Arrays.asList("reload", "unload", "load");
        if(args.length == 1) {
            for(Plugin pl : m.getPlugins()) {
                if(pl.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                    f.add(pl.getName());
                }
            }
            return f;
        }
        if(args.length == 2) {
            for(String a : arguments) {
                if(a.toLowerCase().startsWith(args[1].toLowerCase())) {
                    f.add(a);
                }
            }
            return f;
        }

        return ImmutableList.of();
    }
}
