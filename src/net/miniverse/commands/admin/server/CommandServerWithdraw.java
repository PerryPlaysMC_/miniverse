package net.miniverse.commands.admin.server;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.util.config.Economy;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.itemutils.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class CommandServerWithdraw extends MiniCommand {
    public CommandServerWithdraw() {
        super("serverwithdraw", "serverwd", "swithdraw", "swd");
        desc = "Withdraw money from the server";
        permission = "miniverse.command.banknote.server";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(sendInvalidPermission("banknote.server")) return;
        if(args.length == 0 || (args.length ==1 && !NumberUtil.isDouble(args[0])) || args.length > 1) {
            s.sendMessage("&cError: &4Usage /" + cl.getName() + " <amount>");
            return;
        }
        if(Economy.isBalanceGreaterThanMax(NumberUtil.getMoney(args[0]))) {
            sendCMessage("Withdraw.tooMuch");
            return;
        }
        if(withdrawItem(s.getUser(), NumberUtil.getMoney(args[0]))) {
            sendCMessage("Withdraw.InventoryFull", new DecimalFormat("###,###.00").format(NumberUtil.getMoney(args[0])));
            return;
        }
        sendCMessage("Withdraw.success-server", new DecimalFormat("###,###.00").format(NumberUtil.getMoney(args[0])));
    }

    boolean withdrawItem(User u, double money) {
        boolean isFull = false;
        DecimalFormat df = new DecimalFormat("###,###.00");
        money = NumberUtil.getMoney(money+"");
        ItemStack paper = new ItemStack(Material.PAPER, 1);
        ItemUtil util = ItemUtil.setItem(paper);
        util.setDisplayName("§a§lBankNote §e§oRight Click!").setLore("§cOwner: §e§lServer", "§cWorth: §l$§a" + df.format(money));
        util.setDouble("money", money).setString("owner", "Server");
        paper = util.finish();
        HashMap<Integer, ItemStack> excess = u.getInventory().addItem(paper);
        for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
            u.getLocation().getWorld().dropItemNaturally(u.getLocation().add(0.5,0,0.5), me.getValue());
            isFull = true;
        }
        return isFull;
    }
}
