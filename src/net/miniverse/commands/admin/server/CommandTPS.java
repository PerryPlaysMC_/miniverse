package net.miniverse.commands.admin.server;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.util.TPS;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class CommandTPS extends MiniCommand {
    public CommandTPS() {
        super("mtps", "tps");
        desc = "Get the servers TPS";
        permission = "miniverse.command.tps";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        DecimalFormat f = new DecimalFormat("0.00#");
        double tps = TPS.getTPS();
        double lag = Math.round((1.0D - tps / 20.0D) * 100.0D);
        String msg = "";
        if(tps <= 21) {
            msg = "§a" + f.format(tps);
        }
        if(tps < 18) {
            msg = "§e" + f.format(tps);
        }
        if(tps < 16) {
            msg = "§c" + f.format(tps);
        }
        s.sendMessage("[c]TPS: " + msg);
        s.sendMessage("[c]Lag: " + lag);
    }
}
