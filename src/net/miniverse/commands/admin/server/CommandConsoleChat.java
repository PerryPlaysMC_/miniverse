package net.miniverse.commands.admin.server;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.CommandSourceBase;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;

public class CommandConsoleChat extends MiniCommand implements Listener {
    public CommandConsoleChat() {
        super("consolechat");
    }


    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(s.isPlayer()) {
            s.sendMessage("&cThis command is for console only.");
            return;
        }
        CommandSourceBase b = (CommandSourceBase)s;
        b.setData("chat", b.getData("chat")!=null ? !((Boolean)b.getData("chat")) : true);

        if(!((Boolean)b.getData("chat")))
            s.sendMessage("§cConsole chat Disabled! ");
        else
            s.sendMessage("§cConsole chat Enabled! ");
    }


    @EventHandler
    void onCommand(ServerCommandEvent e) {
        CommandSourceBase b = (CommandSourceBase) Miniverse.getSource(e.getSender().getName());
        if(b.getData("chat") != null&& b.getData("chat") instanceof Boolean && ((Boolean)b.getData("chat"))) {
            if(e.getCommand().startsWith("/")) {
                e.setCommand(e.getCommand().replaceFirst("/",""));
                return;
            }
            Miniverse.broadCast("[c][&cConsole[c]]: &7" + e.getCommand());
            e.setCancelled(true);
        }
    }


}
