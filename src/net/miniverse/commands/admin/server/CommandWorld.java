package net.miniverse.commands.admin.server;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.util.WorldManager;
import net.miniverse.world.CustomChunks;
import net.miniverse.world.CustomWorld;
import net.miniverse.world.DefaultGenerator;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandWorld extends MiniCommand {

	public CommandWorld() {
		super("world", "mworld");
		this.usage = "/<command> <create/teleport(tp)> <world>";
		this.desc = "Create or teleport to a world!";
		permission = "miniverse.command.world";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
	    if(sendInvalidPermission("world"))return;
		if(args.length == 3) {
			if(args[0].equalsIgnoreCase("create")) {
				if(args[1].equalsIgnoreCase("custom")) {
					sendCMessage("World.create-new-world", args[2]);
					CustomChunks c = new CustomChunks();
					World w = Bukkit.createWorld(new CustomWorld(args[2], c));
					w.getName();
					Bukkit.getWorlds().add(w);
					MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".generator", c.getClass().getPackage().getName() + "." + c.getClass().getSimpleName());
					MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".data", w.getSeed());
				}else if(args[1].equalsIgnoreCase("default")) {
					sendCMessage("World.create-new-world", args[2]);
					DefaultGenerator c = new DefaultGenerator(args[2]);
					World w = Bukkit.createWorld(c);
					Bukkit.getWorlds().add(w);
					MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".generator", c.getClass().getPackage().getName() + "." + c.getClass().getSimpleName());
					MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".data", w.getSeed());
				}
			}
		}
		if(args.length == 2) {
			if(args[0].equalsIgnoreCase("create")) {
				sendCMessage("World.create-new-world", args[1]);
				CustomChunks c = new CustomChunks();
				World w = Bukkit.createWorld(new CustomWorld(args[1], c));
				Bukkit.getWorlds().add(w);
				MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".generator", c.getClass().getPackage().getName() + "." + c.getClass().getSimpleName());
				MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".data", w.getSeed());
			}else if(args[0].equalsIgnoreCase("tp") || args[0].equalsIgnoreCase("teleport")) {
				World w = Bukkit.getWorld(args[1]);
				if(w != null) {
					if(!s.isPlayer()) {
						sendCMessage("Invalidations.specify-player");
						return;
					}
					sendCMessage("World.teleport", w.getName(), args[1]);
					s.getBase().teleport(w.getSpawnLocation());
				}
			}else if(args[0].equalsIgnoreCase("delete")) {
                World w = Bukkit.getWorld(args[1]);
                if(w == null) {
                    sendCMessage("World.invalid-world", args[1]);
                    return;
                }
                WorldManager.removeWorld(w);
                sendCMessage("World.delete-world", args[1]);
                if(MiniverseCore.getAPI().getWorlds().isSet("Worlds." + w.getName())) {
                    MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".generator", null);
                    MiniverseCore.getAPI().getWorlds().set("Worlds." + w.getName() + ".data", null);
                }
            }else {
				s.sendMessage(MiniverseCore.getAPI().getSettings(), "Commands.Invalidations.unkown-sub", usage, cl);
			}
		}
	}

	@Override
	public List<String> onTab(CommandSource s, Label cl, String[] args) throws IllegalArgumentException {
		List<String> worlds = new ArrayList<>();
		List<String> sub = Arrays.asList("tp", "teleport", "create");
		for(World w : Bukkit.getWorlds()) {
			worlds.add(w.getName());
		}
		List<String> f = new ArrayList<>();
		if(args.length == 1) {
			for(String a : sub) {
				if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
			}
			return f;
		}
		if(args.length == 2) {
			if(args[0].equalsIgnoreCase("tp") || args[0].equalsIgnoreCase("teleport")) {
				for(String a : worlds) {
					if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
				}
			}
			return f;
		}
		return super.onTab(s, cl, args);
	}

}
