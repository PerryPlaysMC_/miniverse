package net.miniverse.commands.admin.server;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;

import java.util.List;

public class CommandDeop extends MiniCommand {

    public CommandDeop() {
        super("deop", "mdeop", "deoperator", "mdeoperator");
        desc = "Deop the Demoted";
        permission = "miniverse.command.deop";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("deop", "deop.*")) return;
        if(args.length == 0) {
            if(!s.isPlayer()) {
                sendSpecifyTarget();
                return;
            }
            if(sendInvalidPermission("deop.self", "deop.*")) return;
            if(!s.getBase().isOp()) {
                sendCMessage("Op.alreadyDeopped-self", s.getName());
                return;
            }
            sendCMessage("Deop.selfDeop");
            s.getBase().setOp(false);
        } else if(args.length == 1) {
            if(sendInvalidPermission("deop.other", "deop.*")) return;
            OfflineUser t = Miniverse.getOfflineUser(args[0]);
            if(validTarget(args[0])) return;
            if(t.getName() == s.getName() && s.isPlayer()) {
                if(!t.getBase().isOp()) {
                    sendCMessage("Op.alreadyDeopped-self", s.getName());
                    return;
                }
                if(sendInvalidPermission("deop.self", "deop.*")) return;
                sendCMessage("Deop.selfDeop");
                t.getBase().setOp(false);
                return;
            }
            if(!t.getBase().isOp()) {
                sendCMessage("Deop.alreadyDeopped-other", t.getName());
                return;
            }
            t.getBase().setOp(false);
            sendCMessage("Deop.other-self", t.getName());
            if(check("Deop.other-send")) {
                t.sendMessage(Miniverse.getSettings(), "Commands.Deop.other-target", s.getName());
            }
        } else if(args.length > 1) {
            if(sendInvalidPermission("deop.other", "deop.*")) return;
            String invalidPlayers = "";
            String alreadyOpped = "";
            String Deops = "";
            List<OfflineUser> possibleSends = Lists.newArrayList();
            for (int i = 0; i < args.length; i++) {
                OfflineUser t = Miniverse.getOfflineUser(args[i]);
                if(validate(args[0])) {
                    invalidPlayers += "[c], [pc]" + t.getName();
                    continue;
                }
                if(!t.getBase().isOp()) {
                    alreadyOpped += "[c], [pc]" + t.getName();
                    continue;
                }
                t.getName();
                if(t.getName() == s.getName() && s.isPlayer()) {
                    Deops += "[c], [pc]You";
                    continue;
                }
                t.getBase().setOp(false);
                Deops += "[c], [pc]" + t.getName();
            }
            if(!Deops.isEmpty()) {
                sendCMessage("Deop.others-Deops", Deops.replaceFirst(", ", ""));
            }
            if(!invalidPlayers.isEmpty()) {
                sendCMessage("Deop.others-invalid-Deops", invalidPlayers.replaceFirst(", ", ""));
            }
            if(!alreadyOpped.isEmpty()) {
                sendCMessage("Deop.others-invalid-already-Deopped", alreadyOpped.replaceFirst(", ", ""));
            }
            if(check("Deop.others-send")) {
                for (OfflineUser t : possibleSends) {
                    t.sendMessage(Miniverse.getSettings(), "Commands.Deop.other-target", s.getName());
                }
            }
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> a = Lists.newArrayList();
        List<String> f = Lists.newArrayList();
        if(args.length == 0) {
            return a;
        }
        String form = formMessage(0, args.length);
        for(User u : Miniverse.getOnlineUsers()) {
            if(!a.contains(u.getName()) && u.getBase().isOp() && !form.contains(u.getName())) {
                a.add(u.getName());
            }
        }
        for(OfflineUser u : Miniverse.getOfflineUsers()) {
            if(!a.contains(u.getName()) && u.getBase().isOp() && !form.contains(u.getName())) {
                a.add(u.getName());
            }
        }
        String last = args[args.length +-1];
        if(args.length > 0) {
            for(String off : a) {
                if(off.toLowerCase().startsWith(last.toLowerCase())) f.add(off);
            }
            return f;
        }
        return null;
    }
}
