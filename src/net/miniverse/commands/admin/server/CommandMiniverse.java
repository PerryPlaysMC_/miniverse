package net.miniverse.commands.admin.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.miniverse.command.Label;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.util.ChatPaginator;

import com.google.common.collect.Lists;

import net.miniverse.command.MiniCommand;
import net.miniverse.command.MiniCommandHandler;
import net.miniverse.util.config.Config;
import net.miniverse.util.config.ConfigManager;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;

public class CommandMiniverse extends MiniCommand {

    public CommandMiniverse() {
        super("miniverse", "mminiverse", "mini");
        this.desc = "Get command information, reload configs.";
        permission = "miniverse.command.miniverse";
    }

    public CommandMiniverse(String name, String... aliases) {
        super(name, aliases);
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission( "miniverse")) {
            return;
        }
        List<String> cmds = new ArrayList<>();
        for(MiniCommand cmd : MiniCommandHandler.getCommands()) {
            if(!cmds.contains(cmd.getName())) {
                cmds.add(cmd.getName());
            }
        }
        Collections.sort(cmds);
        StringBuilder sb = new StringBuilder();
        for(String command : cmds) {
            MiniCommand cmd = MiniCommandHandler.getCMD(command);
            if(cmd.getDescription() != "NO DESCRIPTION SET") {
                if(cmd.getUsage() != "NO USAGE SET") {
                    sb.append("\n [c]-[pc]" + cmd.getUsage() + "[c] >> " + cmd.getDescription());
                } else {
                    sb.append(" [c]-[pc]" + cmd.getDescription());
                }
            }else {
                if(cmd.getUsage() != "NO USAGE SET") {
                    sb.append("\n [c]-[pc]" + cmd.getUsage());
                } else {
                    sb.append("\n [c]-[pc]/" + cmd.getName());
                }
            }
        }
        if(args.length > 3) {
            if(args[0].equalsIgnoreCase("set")) {
                Config cfg = ConfigManager.getConfig(args[1]);
                if(cfg == null) {
                    sendCMessage("Miniverse.reload-1-fail", args[1]);
                    return;
                }
                cfg.set(args[2], formMessage(3, args.length));
                for(MiniverseCore core : Miniverse.getPlugins()) {
                    core.reloadConfig(cfg);
                }
                sendCMessage("Miniverse.set-Path", cfg.getLocalName(), args[2], formMessage(3, args.length));
                return;
            }
        }
        if(args.length == 0) {
            ChatPaginator.ChatPage p = ChatPaginator.paginate(sb.toString().trim(), 1, Integer.MAX_VALUE, 10);
            s.sendMessage("[c]Miniverse commands: ([pc]" + p.getPageNumber() + "[c]/[pc]" + p.getTotalPages() + "[c])");
            s.sendMessage("[c]&l&m&l----------------------------------");
            s.sendMessage(p.getLines());
            s.sendMessage("[c]&l&m&l----------------------------------");
        }else if(!args[0].equalsIgnoreCase("reset") && !args[0].equalsIgnoreCase("reload")) {
            int page;
            if(!NumberUtil.isInt(args[args.length +- 1])) {
                page = 1;
                sendCMessage("Invalidations.invalid-number", page);
            }else {
                page = NumberUtil.getInt(args[args.length +- 1]);
            }
            ChatPaginator.ChatPage p = ChatPaginator.paginate(sb.toString().trim(), page, Integer.MAX_VALUE, 10);
            s.sendMessage("[c]Miniverse commands: ([pc]" + p.getPageNumber() + "[c]/[pc]" + p.getTotalPages() + "[c])");
            s.sendMessage("[c]&l&m&l----------------------------------");
            s.sendMessage(p.getLines());
            s.sendMessage("[c]&l&m&l----------------------------------");

        }else if(args.length == 2) {
            if(args[0].equalsIgnoreCase("reset")) {
                Config cfg = ConfigManager.getConfig(args[1]);
                if(cfg == null) {
                    sendCMessage("Miniverse.reset-1-fail", args[1]);
                    return;
                }
                cfg = cfg.resetConfig();

                for(MiniverseCore core : Miniverse.getPlugins()) {
                    core.reloadConfig(cfg);
                }
                sendCMessage("Miniverse.reset-1", cfg.getLocalName(), cfg.getName());
            }else if(args[0].equalsIgnoreCase("reload")) {
                if(args[1].equalsIgnoreCase("all")) {
                    List<String> cfgss = new ArrayList<String>();
                    cfgss.clear();
                    for(Config cfgs : ConfigManager.getConfigs()) {
                        if(!cfgss.contains(cfgs.getLocalName())) {
                            cfgss.add(cfgs.getLocalName());
                        }
                    }
                    for(MiniverseCore core : Miniverse.getPlugins()) {
                        core.reloadAll();
                    }
                    String cf = "None";
                    StringBuilder sb2 = new StringBuilder();
                    if(cfgss.size() > 0) {
                        for(int i = 0; i < cfgss.size(); i++) {
                            sb2.append("[pc]" + cfgss.get(i) + "[c], ");
                        }
                        sb.setLength(sb2.toString().length() +-2);
                        cf = sb2.toString().trim();
                    }
                    MiniCommandHandler.reloadCommands();
                    sendCMessage("Miniverse.reload-all", cf);
                }else if(!args[1].equalsIgnoreCase("all")) {
                    Config cfg = ConfigManager.getConfig(args[1]);
                    if(args[1].equalsIgnoreCase("commands")) {
                        MiniCommandHandler.reloadCommands();
                        SimpleCommandMap cMap = MiniverseCore.getAPI().getCommandMap();
                        Command[] commands = cMap.getCommands().toArray(new Command[cMap.getCommands().toArray().length]);
                        StringBuilder sb3 = new StringBuilder();
                        if(commands[0] instanceof MiniCommandHandler && MiniCommandHandler.getCommand(commands[0].getName()) != null && MiniCommandHandler.
                                getCommandFromAlias(commands[0].getName()) == null) {
                            sb3.append("[pc]" + commands[0].getName());
                        }
                        for(int i = 1; i < cMap.getCommands().size() +- 1; i++) {
                            Command cmd = commands[i];
                            if(cmd instanceof MiniCommandHandler
                                    && MiniCommandHandler.getCommand(cmd.getName()) != null && MiniCommandHandler.
                                    getCommandFromAlias(cmd.getName()) == null) {
                                if(sb3.length() > 0) {
                                    sb3.append("[c], [pc]" + commands[i].getName());
                                }else{
                                    sb3.append("[pc]" + commands[i].getName());
                                }
                            }
                        }
                        String commandsz = sb3.toString().trim() + "[c].";
                        sendCMessage("Miniverse.reload-commands", commandsz);
                        return;
                    }
                    if(cfg == null) {
                        sendCMessage("Miniverse.reload-1-fail", args[1]);
                        return;
                    }
                    for(MiniverseCore core : Miniverse.getPlugins()) {
                        core.reloadConfig(cfg);
                    }
                    sendCMessage("Miniverse.reload-1", cfg.getLocalName(), cfg.getName());
                }
            }
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> args1 = Arrays.asList("reload", "reset", "set");
        List<String> args2 = Lists.newArrayList();
        List<String> args3 = Lists.newArrayList();

        for(Config cfg : ConfigManager.getConfigs()) {
            if(!args2.contains(cfg.getLocalName())) {
                args2.add(cfg.getLocalName());
            }
        }

        for(Config cfg : ConfigManager.getConfigs()) {
            if(cfg != null && !cfg.getLocalName().isEmpty() && !args3.contains(cfg.getLocalName()) && cfg.getFile().exists()) {
                args3.add(cfg.getLocalName());
            }
        }

        List<String> f = Lists.newArrayList();
        if(args.length == 1) {
            for (String a : args1) {
                if(a.toLowerCase().startsWith(args[0].toLowerCase().toLowerCase())) f.add(a);
            }
            return f;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("reload")) {
                for (String a : args2) {
                    if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
                }
                return f;
            }else if(args[0].equalsIgnoreCase("reset")) {
                for (String a : args3) {
                    if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
                }
                return f;
            }else if(args[0].equalsIgnoreCase("set")) {
                for (String a : args2) {
                    if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
                }
                return f;
            }
        }
        if(args.length == 3) {
            Config cfg = ConfigManager.getConfig(args[1]);
            if(cfg == null) {
                sendCMessage("Miniverse.reload-1-fail", args[1]);
                return Lists.newArrayList();
            }
            if(!args[2].endsWith(".")) {
                ConfigurationSection se = cfg.getConfig();
                for(String a : se.getKeys(false)) {
                    if(a.toLowerCase().startsWith(args[2].toLowerCase())) f.add(a + ".");
                }
                return f;
            }
            if(args[2].endsWith(".") && cfg.getSection(args[2].substring(0, args[2].lastIndexOf("."))) != null) {
                ConfigurationSection se = cfg.getSection(args[2].substring(0, args[2].lastIndexOf(".")));
                for(String a : se.getKeys(false)) {
                    if(a.toLowerCase().startsWith(args[2].substring(args[2].lastIndexOf(".")+1))) {
                        ConfigurationSection x = cfg.getSection(args[2]+a);
                        f.add(args[2]+a+(x!=null&&x.getKeys(false).size()>0?".":""));
                    }
                }
                return f;
            }
            for(String a : cfg.getConfig().getKeys(false)) {
                if(a.toLowerCase().startsWith(args[2].toLowerCase())) f.add(a+".");
            }
            return f;
        }

        return null;
    }


}
