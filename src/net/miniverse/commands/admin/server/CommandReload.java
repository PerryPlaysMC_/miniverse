package net.miniverse.commands.admin.server;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.miniverse.command.Label;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.Event;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredListener;

import net.miniverse.MiniversePlugin;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;

public class CommandReload extends MiniCommand {

	public CommandReload() {
		super("reload", "rl");
		this.usage = "/reload | /rl";
		this.desc = "Reload the server";
		permission = "miniverse.command.reload";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission("reload")) return;
			Miniverse.broadCastPerm("miniverse.reload.see", "§4§lWarning: §cReload in progress");
			Bukkit.reload();
			Miniverse.broadCastPerm("miniverse.reload.see", "§a§lInfo: §cReload complete");
	}

}
