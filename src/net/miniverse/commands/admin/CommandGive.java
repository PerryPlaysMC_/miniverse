package net.miniverse.commands.admin;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.util.itemutils.MiniItem;
import net.miniverse.util.NumberUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandGive extends MiniCommand {

	public CommandGive() {
		super("give", "mgive", "i", "mi");
		this.desc = "Give yourself any item";
		permission = "miniverse.command.give";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		MiniverseCore mv = MiniverseCore.getAPI();
		ItemStack im = null;
		if(sendInvalidPermission("give")) return;
		if(args.length == 1) {
			if(!MiniItem.cfg.getFile().exists()) {
				new MiniItem();
			}
			int amount = 64;
			
			try {
				im = MiniItem.getItem(args[0], amount);
			} catch (Exception e) {
				s.sendMessage(MiniverseCore.getAPI().translate("Commands.Invalidations.unknown-item", args[0]));
				return;
			}
			s.getUser().getBase().getInventory().addItem(im);
			String item = StringUtils.getNameFromEnum(im.getType());
			s.sendMessage(MiniverseCore.getAPI().translate("Commands.Give.giveItem", item, 64, im.getType().name(), im.getType().name().toLowerCase()));
		}else if(args.length == 2) {
			if(!MiniItem.cfg.getFile().exists()) {
				new MiniItem();
			}
			int amount = 1;
			if(!NumberUtil.isInt(args[1])) {
				amount = 1;
				s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-number", amount, args[1]);
			}
			if(NumberUtil.isInt(args[1])) {
				amount = NumberUtil.getInt(args[1]);
			}
			try {
				im = MiniItem.getItem(args[0], amount);
			} catch (Exception e) {
				s.sendMessage(MiniverseCore.getAPI().translate("Commands.Invalidations.unknown-item", args[0]));
				return;
			}
			s.getUser().getBase().getInventory().addItem(im);
            String item = StringUtils.getNameFromEnum(im.getType());
			s.sendMessage(MiniverseCore.getAPI().translate("Commands.Give.giveItem", item, amount, im.getType().name(), im.getType().name().toLowerCase() ));
		}else if(args.length == 3) {
			if(!MiniItem.cfg.getFile().exists()) {
				new MiniItem();
			}
			int amount = 1, data = 0;
			if(!NumberUtil.isInt(args[1])) {
				amount = 1;
				s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-number", amount, args[1]);
			}
			if(NumberUtil.isInt(args[1])) {
				amount = NumberUtil.getInt(args[1]);
			}

			if(!NumberUtil.isInt(args[2])) {
				data = 1;
				s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-number", data, args[2]);
			}
			if(NumberUtil.isInt(args[2])) {
				data = NumberUtil.getInt(args[2]);
			}
			try {
				im = MiniItem.getItem(args[0], amount);
			} catch (Exception e) {
				s.sendMessage(MiniverseCore.getAPI().translate("Commands.Invalidations.unknown-item", args[0]));
				return;
			}
			s.getUser().getBase().getInventory().addItem(im);
            String item = StringUtils.getNameFromEnum(im.getType());
			s.sendMessage(MiniverseCore.getAPI().translate("Commands.Give.giveItem", item, amount, im.getType().name(), im.getType().name().toLowerCase()));
		}

	}

	private String getItemName(Material mat) {
		return getItemName(new ItemStack(mat));
	}
	private String getItemName(ItemStack im) {
		Material mat = im.getType();
		String item = mat.name().charAt(0) + mat.name().substring(1).replace("_", " ").toLowerCase();
		if(item.contains(" ")) {
			String b = item;
			item = "";
			for(String a : b.split(" ")) {
				item+= (a.charAt(0) + "").toUpperCase() + a.substring(1).toLowerCase() + " ";
			}
			item = item.trim();
		}
		if(im.hasItemMeta() && !im.getItemMeta().getDisplayName().isEmpty()) {
			item = im.getItemMeta().getDisplayName();
		}
		return item;
	}

	private static List<String> mats = new ArrayList<>();
	static {
	    for(Material m : Material.values()) mats.add(m.name().toLowerCase());
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
	    List<String> f = Lists.newArrayList();
	    if(args.length == 1) {
	        for(String x : mats) {
	            if(x.startsWith(args[0].toLowerCase())) f.add(x);
            }
            return f;
        }
        return f;
    }
}
