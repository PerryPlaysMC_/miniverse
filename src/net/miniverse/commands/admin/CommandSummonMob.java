package net.miniverse.commands.admin;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CommandSummonMob extends MiniCommand {

    public CommandSummonMob() {
        super("summonmob", "mob", "smob");
        desc = "Summon mobs easily";
        permission = "miniverse.command.mob";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("mob")) return;
        if (args.length == 0) {

            return;
        }
        if (args.length == 1) {
            summon(s, args);
        }
        if (args.length == 2) {
            if (!NumberUtil.isInt(args[1])) {
                s.sendMessage("[c]Please specify a valid number");
                return;
            }
            int x = NumberUtil.getInt(args[1]);
            for (int i = 0; i < x; i++) {
                summon(s, args);
            }
        }
    }

    void summon(CommandSource s, String[] args){
        String msg = null;
        if (args[0].contains(",")) {
            EntityType t = EntityType.valueOf(args[0].split(",")[0].toUpperCase());
            Entity ent = s.getBase().getWorld().spawnEntity(s.getBase().getLocation(), t);
            for (int i = 1; i < args[0].split(",").length; i++) {
                EntityType type;
                try {
                    type = EntityType.valueOf(args[0].split(",")[i].toUpperCase());
                } catch (Exception e) {
                    s.sendMessage("[c]Invalid mob " + args[0].split(",")[i].toUpperCase());
                    continue;
                }
                Entity a = s.getBase().getWorld().spawnEntity(ent.getLocation(), type);
                if(a instanceof Villager) {
                    msg += ((Villager)a).getProfession().name() + " ";
                }
                ent.addPassenger(a);
                ent = a;
            }
            if(msg != null && !msg.isEmpty()) {
                s.sendMessage(msg.trim());
            }
            return;
        }

        EntityType t;
        try{
            t = EntityType.valueOf(args[0].toUpperCase());
        }catch (Exception e){
            s.sendMessage("[c]Invalid mob " + args[0].toUpperCase());
            return;
        }
        Entity ent = s.getBase().getWorld().spawnEntity(s.getBase().getLocation(), t);
        if(ent instanceof Villager) {
            msg = ((Villager)ent).getProfession().name();
        }
        if(msg != null && !msg.isEmpty()) {
            s.sendMessage(msg);
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> types = Lists.newArrayList();
        for(EntityType t : EntityType.values()) {
            if(!types.contains(t.name()) && t.isSpawnable()) types.add(t.name());
        }

        if(args.length == 1) {
            for(String x : types) {
                if(x.toLowerCase().startsWith(args[0].toLowerCase())) f.add(x.toLowerCase());
            }
            return f;
        }f.add("");

        return f;
    }
}
