package net.miniverse.commands.admin.chat;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandCMDSpy extends MiniCommand {

    public CommandCMDSpy() {
        super("commandspy", "cmdspy", "cspy", "commandwatch", "cmdwatch");
        desc = "Spy on other people as they use commands";
        usage = "/<command>";
        permission = "miniverse.command.commandspy";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {


        if(args.length == 0) {
            if(sendInvalidPermission("commandspy.self", "commandspy")) return;
            if(!s.isPlayer()) {
                sendSpecifyTarget();
                return;
            }
            User u = s.getUser();

            sendCMessage("CommandSpy.toggle", u.commandSpy() ? "disabled" : "enabled");
            u.toggleCommandSpy();
        } else if(args.length == 1) {
            User u = s.getUser();
            if(args[0].equalsIgnoreCase("on")){
                sendCMessage("CommandSpy.toggle", "enabled");
                u.toggleCommandSpy(true);
                return;
            }
            if(args[0].equalsIgnoreCase("off")){
                sendCMessage("CommandSpy.toggle", "disabled");
                u.toggleCommandSpy(false);
                return;
            }

            if(sendInvalidPermission("commandspy.other"))return;
        }
    }
}
