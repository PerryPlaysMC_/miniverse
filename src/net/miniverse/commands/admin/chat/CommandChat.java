package net.miniverse.commands.admin.chat;

import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;

import java.util.Arrays;
import java.util.List;

public class CommandChat extends MiniCommand {
    public CommandChat() {
        super("chat", "c");
        desc = "Does Chat settings stuff";
        permission = "miniverse.command.chat";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("miniverse.command.chat")) return;
        if(args.length == 0) {
            sendUsage(s);
            return;
        }
        if(args.length == 1) {
            if(!args[0].equalsIgnoreCase("toggle") && !args[0].equalsIgnoreCase("toggleslow") && !args[0].equalsIgnoreCase("clear")) {
                sendUsage(s);
                return;
            }
            if(args[0].equalsIgnoreCase("clear")) {

                for(int i = 0; i < 400; i++) {
                    Miniverse.broadCast(" ");
                }
                Miniverse.broadCast("[c]Chat was cleared by " + (s.isVanished() ? "an &cAdmin" : "[pc]" + s.getName()));
                return;
            }
            if(args[0].equalsIgnoreCase("toggleslow")) {
                if(sendInvalidPermission("miniverse.command.chat.toggleslow"))
                if(!Miniverse.getChat().isSet("Settings.isSlow"))
                    Miniverse.getChat().set("Settings.isSlow", false);
                Miniverse.getChat().set("Settings.isSlow", !Miniverse.getChat().getBoolean("Settings.isSlow"));

                String r2 = Miniverse.getChat().getBoolean("Settings.isSlow") ? "Enabled" : "Disabled";
                sendCMessage("Chat.Slow.toggle", r2);
                return;
            }
            if(!Miniverse.getChat().isSet("Settings.Paused"))
                Miniverse.getChat().set("Settings.Paused", false);
            Miniverse.getChat().set("Settings.Paused", !Miniverse.getChat().getBoolean("Settings.Paused"));
            String r = Miniverse.getChat().getBoolean("Settings.Paused") ? "Paused" : "unPaused";
            sendCMessage("Chat.toggle", r);
            return;
        }
        if(args.length == 2) {
            if(sendInvalidPermission("miniverse.command.chat"))
            if(!args[0].equalsIgnoreCase("slow")) {
                sendUsage(s);
                return;
            }
            String r = args[1];
            if(r.equalsIgnoreCase("enable")) {
                Miniverse.getChat().set("Settings.isSlow", true);
            } else if(r.equalsIgnoreCase("disable")) {
                Miniverse.getChat().set("Settings.isSlow", true);
            } else {
                sendCMessage("Chat.InvalidOption");
                return;
            }
            String r2 = Miniverse.getChat().getBoolean("Settings.isSlow") ? "Enabled" : "Disabled";
            sendCMessage("Chat.Slow.toggle", r2);
            return;
        }
        if(args.length == 3) {
            if(!args[0].equalsIgnoreCase("slow") && !args[1].equalsIgnoreCase("settime")) {
                sendUsage(s);
                return;
            }
            if(!NumberUtil.isDouble(args[2])) {
                s.sendMessage("[c]Invalid number");
                return;
            }
            Miniverse.getChat().set("Settings.Slow", NumberUtil.getDouble(args[2]));
            sendCMessage("Chat.Slow.speed", NumberUtil.getDouble(args[2]));
            return;
        }
        sendUsage(s);
        //s.sendMessage("[c]/chat slow enabled/disable", "[c]/chat toggleslow", "[c]/chat slow settime <time>", "[c]/chat toggle");
    }


    void sendUsage(CommandSource s) {
        Message msg = new Message(s, "[c]&m&l      &r[pc]Chat Commands[c]&m&l      ");
        msg.then("chat", "\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional" +
                        " \n [r]<argument>[c] &7= Required" +
                        " \n [a][-s][c] &7= Silent" +
                        "\n");
        msg.then("chat.clear", "\n [c]-/[pc]chat[c] [r]clear")
                .tooltip("[c]Info: [pc]Clears 400 messages",
                        "[c]Click to insert:",
                        "[pc]/chat clear")
                .suggest("/chat clear");
        msg.then("chat", "\n [c]-/[pc]chat[c] [r]toggle")
                .tooltip("[c]Info: [pc]Pause/unPause chat",
                        "[c]Click to insert:",
                        "[pc]/chat toggle")
                .suggest("/chat toggle");
        msg.then("chat.toggleslow", "\n [c]-/[pc]chat[c] [r]toggleslow")
                .tooltip("[c]Info: [pc]Toggle the chat slow mode",
                        "[c]Click to insert:",
                        "[pc]/chat toggleslow")
                .suggest("/chat toggleslow");
        msg.then("chat", "\n [c]-/[pc]chat[c] [r]slow [o][")
                .tooltip("[c]Info: [pc]Change chat slow mode settings",
                        "[c]Click to insert:",
                        "[pc]/chat slow ")
                .suggest("/chat slow ")
                .then("[o]enable").suggest("/chat slow enable").tooltip("[c]Insert: /chat slow enable").then("/")
                .then("[o]disable").suggest("/chat slow disable").tooltip("[c]Insert: /chat slow disable").then("/")
                .then("[o]settime").suggest("/chat slow settime ").tooltip("[c]Insert: /chat slow settime").then("]");

        msg.send(s);
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> x = Arrays.asList("clear", "toggle", "toggleslow", "slow");
        List<String> x1 = Arrays.asList("enable", "disable", "settime");
        if(args.length == 1) {
            for(String a : x) {
                if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
            }
            return f;
        }
        if(args.length == 2) {
            for(String a : x1) {
                if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
            }
            return f;
        }
        return Lists.newArrayList();
    }
}
