package net.miniverse.commands.admin.chat;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandCalculate extends MiniCommand {
    public CommandCalculate() {
        super("calculate", "calc", "cc");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length>0)
            try {
                sendCMessage("Calculate.success",args[0], NumberUtil.calc(args[0]));
            }catch (Exception e) {
                sendCMessage("Calculate.fail", args[0]);
                e.printStackTrace();
            }
    }
}
