package net.miniverse.commands.admin.chat;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.Perm;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import org.bukkit.Sound;

public class CommandBroadcast extends MiniCommand {

    public CommandBroadcast() {
        super("broadcast",false,"bc","broadcastperm","bcperm");
        desc = "Broadcast messages to the players.";
        permission = "miniverse.command.chat";
    }

    @Override
    public void run(CommandSource s,Label cl,String[] args) {
        StringBuilder finalS = new StringBuilder();
        if(!StringUtils.translate(Miniverse.getSettings().getString("BroadCastPrefix")).endsWith(" ")) {
            finalS.append(StringUtils.translate(Miniverse.getSettings().getString("BroadCastPrefix"))).append(" ");
        }else finalS.append(StringUtils.translate(Miniverse.getSettings().getString("BroadCastPrefix")));

        if(!cl.getName().contains("perm")) {
            if(sendInvalidPermission("broadcast")) return;
            if(args.length == 0) {
                sendMoreArguments();
                return;
            }
            for(int i = 0; i < args.length; i++) {
                finalS.append(args[i]).append(" ");
            }
            for(User u : Miniverse.getOnlineUsers()) {
                u.getBase().playSound(u.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,10,0.1f);
            }
            Miniverse.broadCast(finalS.toString().trim());
            return;
        }
        if(sendInvalidPermission("broadcast.perm")) return;
        if(args.length == 0) {
            sendMoreArguments();
            return;
        }
        for(int i = 0; i < args.length; i++) {
            finalS.append(args[i]).append(" ");
        }
        for(User u : Miniverse.getOnlineUsers()) {
            if(u.hasPermission("admin.broadcast"))
                u.getBase().playSound(u.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,10,0.1f);
        }
        Miniverse.broadCastPerm("admin.broadcast", finalS.toString().trim());
    }
}
