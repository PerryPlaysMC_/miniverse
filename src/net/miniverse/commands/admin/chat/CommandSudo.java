package net.miniverse.commands.admin.chat;

import com.google.common.collect.Lists;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.MiniCommandHandler;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.command.Command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@SuppressWarnings("all")
public class CommandSudo extends MiniCommand {

    public CommandSudo() {
        super("sudo", "msudo");
        desc = "Perform chat actions as someone else";
        permission = "miniverse.command.sudo";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("sudo")) return;
        if(args.length > 1) {
            User t = Miniverse.getUser(args[0]);
            if(t==null) {
                sendInvalidTarget(args[0]);
                return;
            }
            String msg = formMessage(1, args.length);
            if(t.getName() != s.getName())
                if(args[1].startsWith("/")) {
                    sendCMessage("Sudo.sudo-command", t.getName(), msg, args[0]);
                }else {
                    sendCMessage("Sudo.sudo-chat", t.getName(), msg, args[0]);
                }
            t.getBase().chat(msg);
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {

        List<String> commands = new ArrayList<>();
        List<String> f = Lists.newArrayList();
        for(Command cmd : core.getCommandMap().getCommands()) {
            if(!commands.contains(cmd.getName())) {
                commands.add(cmd.getName());
            }
        }
        List<String> users = new ArrayList<>();
        for(User u : Miniverse.getOnlineUsers()) {
            if(!users.contains(u.getName())) {
                users.add(u.getName());
            }
        }
        if(args.length == 1) {
            for(String a : users) {
                if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
            }
            return f;
        }
        if(args.length == 2) {
            if(args[1].startsWith("/")) {
                for(Command cmd : core.getCommandMap().getCommands()) {
                    User u = Miniverse.getUser(args[0]);
                    if(cmd.getPermission() == "" || cmd.getPermission() == null) {
                        if(("/" + cmd.getName()).toLowerCase().startsWith(args[1].toLowerCase())) f.add("/" + cmd.getName());
                        continue;
                    }
                    if(u.hasPermission(cmd.getPermission())&&("/" + cmd.getName()).toLowerCase().startsWith(args[1].toLowerCase())) f.add("/" + cmd.getName());
                }
                Collections.sort(f);
                return f;
            }
        }
        if(args.length >= 3) {
            if(args[1].startsWith("/") && Miniverse.parseMini(args[1]) != null) {
                MiniCommand cmd = Miniverse.parseMini(args[1]);
                String[] arg = Arrays.copyOfRange(args, 2, args.length);
                User u = Miniverse.getUser(args[0]);
                if(cmd.getPermission() == null || cmd.getPermission() == "") {
                    if(cmd.onTab(s, cl, arg) != null && cmd.onTab(s, cl, arg).size() > 0) {
                        f.addAll(cmd.onTab(s, cl, arg));
                    }
                    return f;
                }
                if((u.hasPermission(cmd.getPermission())) && cmd.onTab(s, cl, arg) != null && cmd.onTab(s, cl, arg).size() > 0) {
                    f.addAll(cmd.onTab(s, cl, arg));
                }
                return f;
            }else if(args[1].startsWith("/") && Miniverse.parseCommand(args[1]) != null) {
                Command cmd = Miniverse.parseCommand(args[1]);
                String[] arg = Arrays.copyOfRange(args, 2, args.length);
                User u = Miniverse.getUser(args[0]);
                if(cmd.getPermission() == null || cmd.getPermission() == "") {
                    if(cmd.tabComplete(s.getSender(), cl.getName(), arg) != null && cmd.tabComplete(s.getSender(), cl.getName(), arg).size() > 0) {
                        f.addAll(cmd.tabComplete(s.getSender(), cl.getName(), arg));
                    }
                    return f;
                }
                if((u.hasPermission(cmd.getPermission())) && cmd.tabComplete(s.getSender(), cl.getName(), arg) != null && cmd.tabComplete(s.getSender(), cl.getName(), arg).size() > 0) {
                    f.addAll(cmd.tabComplete(s.getSender(), cl.getName(), arg));
                }
                return f;
            }else if(args[1].startsWith("/") && Miniverse.parseCommand(args[1]) != null) {
                Command cmd = Miniverse.parseCommand(args[1]);
                String[] arg = Arrays.copyOfRange(args, 2, args.length);
                User u = Miniverse.getUser(args[0]);
                if(cmd.getPermission() == null || cmd.getPermission() == "") {
                    if(cmd.tabComplete(s.getSender(), cl.getName(), arg) != null && cmd.tabComplete(s.getSender(), cl.getName(), arg).size() > 0) {
                        f.addAll(cmd.tabComplete(s.getSender(), cl.getName(), arg));
                    }
                    return f;
                }
                if((u.hasPermission(cmd.getPermission())) && cmd.tabComplete(s.getSender(), cl.getName(), arg) != null && cmd.tabComplete(s.getSender(), cl.getName(), arg).size() > 0) {
                    f.addAll(cmd.tabComplete(s.getSender(), cl.getName(), arg));
                }
                return f;
            }
        }
        return null;
    }



}
