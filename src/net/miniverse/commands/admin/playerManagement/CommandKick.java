package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.Perm;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandKick extends MiniCommand {

	public CommandKick() {
		super("kick", new Perm("miniverse.command.kick"), "mkick");
		desc = "Kick the naughty players";
		permission = "miniverse.command.kick";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission( "kick", "kickplayer", "playerkick")) return;
		if (args.length == 0) {
			sendCMessage("Invalidations.specify-player");
			return;
		}
		if (args.length >= 1) {
			User t = Miniverse.getUser(args[0]);
			if (t != null) {
				String msg = Miniverse.getSettings().getString("Commands.Kick.defaultKickMessage");
				if (args.length > 1) {
					msg = formMessage(1, args.length);
				}
				t.kick(msg);
				sendCMessage("Kick.playerKick", t.getName(), msg, args[0]);
			}
		}
	}
}