package net.miniverse.commands.admin.playerManagement;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class CommandRespawn extends MiniCommand {

	public CommandRespawn() {
		super("respawn", "rs");
		this.usage = "/respawn <player>";
		this.desc = "Respawn a player with a single commands";
		permission = "miniverse.command.respawn";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission("respawn")) return;
		if(args.length == 0) {
			s.sendMessage("[p]Please specify a player.");
			return;
		}else {
			if(args.length > 1) {
				for (User u : Miniverse.getUsers(formMessage(0, args.length))) {
					if(u == null) {
						s.sendMessage("[c]Invalid player");
						continue;
					}
					if(!u.getBase().isDead()) {
						s.sendMessage("[p] That player isn't dead!");
						continue;
					}
					u.getBase().spigot().respawn();
					s.sendMessage("[p]Respawned " + u.getName());
				}
			}else{
				User u = Miniverse.getUser(args[0]);
				if(u == null) {
					s.sendMessage("[c]Invalid player");
					return;
				}
				if(!u.getBase().isDead()) {
					s.sendMessage("[p] That player isn't dead!");
					return;
				}
				u.getBase().spigot().respawn();
				s.sendMessage("[p]Respawned " + u.getName());
			}
		}
	}

	@Override
	public List<String> onTab(CommandSource s, Label cl, String[] args) {
		List<String> users = new ArrayList<>();
		List<String> f = Lists.newArrayList();
		for(User u : Miniverse.getOnlineUsers()) {
			if(u.getBase().isDead() || u.getHealth()==0) {
				if(!users.contains(u.getName())) {
					users.add(u.getName());
				}
			}
		}
		String l = args[args.length +-1];
		if(args.length == 1) {
			for(String a : users) {
				if(a.toLowerCase().startsWith(l.toLowerCase())) f.add(a);
			}
		}

		return f;
	}
}
