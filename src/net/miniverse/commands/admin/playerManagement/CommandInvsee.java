package net.miniverse.commands.admin.playerManagement;

import net.miniverse.chat.Invalidations;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandInvsee extends MiniCommand {
    public CommandInvsee() {
        super(true, "invsee", "isee", "inventorysee");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()){sendMustBePlayer();return;}
        User u = s.getUser();
        if(sendInvalidPermission("invsee"))return;
        if(args.length == 0) {
            s.sendMessage(Invalidations.SPECIFY_PLAYER);
            return;
        }
        if(args.length == 1) {
            if(validate(args[0])) {
                sendInvalidTarget(args[0]);
                return;
            }
            if(args[0].equalsIgnoreCase(s.getName())) {
                s.sendMessage("[c]Press E");
                return;
            }
            u.getBase().openInventory(Miniverse.getUser(args[0]).getInventory());
            s.sendMessage("[c]Opening " + Miniverse.getUser(args[0]).getName());
        }else {
            s.sendMessage(Invalidations.TOO_MANY_ARGUMENTS);
        }
    }
}
