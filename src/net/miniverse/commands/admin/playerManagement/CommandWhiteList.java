package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class CommandWhiteList extends MiniCommand {


    public CommandWhiteList() {
        super("whitelist", "wl");
        desc = "Don't want everyone joining this server? Change that with /whitelist!";
        permission = "miniverse.command.whitelist";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) {
            if(sendInvalidPermission("whitelist.list", "whitelist.*"))return;
            List<String> whitelist = new ArrayList<>();
            for(String a : Miniverse.getWhiteList().getStringList("whitelisted")) {
                String player = Miniverse.getOfflinePlayer(UUID.fromString(a)).getName();
                if(!whitelist.contains(player)) whitelist.add(player);
            }
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < whitelist.size(); i++) {
                String end = "[c], [pc]";
                if(i == whitelist.size()+-1) end = "[c].";
                sb.append(whitelist.get(i)).append(end);
            }
            String list = sb.toString();
            sendCMessage("WhiteList.list", list);
        }
        else if(args.length == 1) {
            if(sendInvalidPermission("whitelist.list", "whitelist.*"))return;
            if(args[0].equalsIgnoreCase("on")) {
                if(sendInvalidPermission("whitelist.enable", "whitelist.*"))return;
                sendCMessage("WhiteList.enable");
                Miniverse.getWhiteList().set("Enabled", true);

                List<String> white = Miniverse.getWhiteList().getStringList("whitelisted");
                for(User u : Miniverse.getOnlineUsers()) {
                    if(!white.contains(u.getUUID().toString())) {
                        String kickmsg = Miniverse.getSettings().getString("Commands.Kick.kickPrefix");
                        Miniverse.getSettings().set("Commands.Kick.kickPrefix", "");
                        u.kick(Miniverse.getSettings().getString("Events.Connection.WhiteList.Join"));
                        Miniverse.getSettings().set("Commands.Kick.kickPrefix", kickmsg);
                    }
                }

            }else if(args[0].equalsIgnoreCase("off")) {
                if(sendInvalidPermission("whitelist.disable", "whitelist.*"))return;
                sendCMessage("WhiteList.disable");
                Miniverse.getWhiteList().set("Enabled", false);
            }else if(args[0].equalsIgnoreCase("list")) {
                List<String> whitelist = new ArrayList<>();
                for(String a : Miniverse.getWhiteList().getStringList("whitelisted")) {
                    String player = Miniverse.getName(UUID.fromString(a));
                    if(!whitelist.contains(player)) whitelist.add(player);
                }
                StringBuilder sb = new StringBuilder();
                for(int i = 0; i < whitelist.size(); i++) {
                    String end = "[c], [pc]";
                    if(i == whitelist.size()+-1) end = ".";
                    sb.append(whitelist.get(i)).append(end);
                }
                String list = sb.toString();
                sendCMessage("WhiteList.list", list);
                return;
            }else {
                sendCMessage("WhiteList.unknownSub");
            }
        }else if(args.length == 2) {
            if(!args[0].equalsIgnoreCase("add") && !args[0].equalsIgnoreCase("remove")) {
                sendCMessage("WhiteList.unknownSub");
                return;
            }
            List<String> white = Miniverse.getWhiteList().getStringList("whitelisted");
            OfflinePlayer p = Miniverse.getOfflinePlayer(args[1]);
            String name = Miniverse.getOfflinePlayerName(args[1]);
            if(p == null) {
                sendInvalidTarget(args[1]);
                return;
            }
            if(args[0].equalsIgnoreCase("add")) {
                if(sendInvalidPermission("whitelist.add", "whitelist.*"))return;
                if(white.contains(p.getUniqueId().toString())) {
                    sendCMessage("WhiteList.containsPlayer", name, p.getUniqueId());
                    return;
                }
                white.add(p.getUniqueId().toString());
                sendCMessage("WhiteList.addedPlayer", name, p.getUniqueId());
            }else if(args[0].equalsIgnoreCase("remove")) {
                if(sendInvalidPermission("whitelist.remove", "whitelist.*"))return;
                if(!white.contains(p.getUniqueId().toString())) {
                    sendCMessage("WhiteList.doesntContainPlayer", name, p.getUniqueId());
                    return;
                }
                white.remove(p.getUniqueId().toString());
                if(p.isOnline()) {
                    User u = Miniverse.getUser(p.getName());
                    String kickMsg = Miniverse.getSettings().getString("Commands.Kick.kickPrefix");
                    Miniverse.getSettings().set("Commands.Kick.kickPrefix", "");
                    u.kick(Miniverse.getSettings().getString("Events.Connection.WhiteList.Join"));
                    Miniverse.getSettings().set("Commands.Kick.kickPrefix", kickMsg);
                }
                sendCMessage("WhiteList.removePlayer", name, p.getUniqueId());
            }
            Miniverse.getWhiteList().set("whitelisted", white);
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> a = Arrays.asList("on", "off", "add", "list", "remove");
        List<String> f = new ArrayList<>();
        if(args.length == 1) {
            for(String ae : a) {
                if(ae.toLowerCase().startsWith(args[0].toLowerCase())) f.add(ae);
            }
            return f;
        }
        f = super.onTab(s, cl, args);
        return f;
    }
}
