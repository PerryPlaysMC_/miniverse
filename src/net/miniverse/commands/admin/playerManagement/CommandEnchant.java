package net.miniverse.commands.admin.playerManagement;

import com.google.common.collect.Lists;
import net.miniverse.chat.Invalidations;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.enchantment.CustomEnchant;
import net.miniverse.enchantment.EnchantHandler;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.ItemEnchantUtil;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class CommandEnchant extends MiniCommand {
    public CommandEnchant() {
        super("enchant");
        desc = "Enchant the item in your hand.";
        permission = "miniverse.command.enchant";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(sendInvalidPermission("enchant"))return;
        if(args.length == 0) {
            s.sendMessage(Invalidations.MORE_ARGUMENTS);
            return;
        }
        if(args.length == 1) {
            ItemStack i = s.getUser().getItemInHand();
            if(i == null || i.getType() == Material.AIR) {
                s.sendMessage("[c]Item cannot be air!");
                return;
            }
            Enchantment x = getEnchantment(args[0]);
            if(x == null) {
                s.sendMessage("[c]Invalid Enchantment");
                return;
            }
            ItemMeta im = i.getItemMeta();
            if(x instanceof CustomEnchant) {
                CustomEnchant c = (CustomEnchant) x;
                im = ItemEnchantUtil.forceAddBukkitEnchantment(i, c, c.getMaxLevel());
                i.setItemMeta(im);
            }else {
                if(im != null && im.hasEnchant(x)) {
                    im.removeEnchant(x);
                    i.setItemMeta(im);
                }
                im = ItemEnchantUtil.forceAddBukkitEnchantment(i, x, x.getMaxLevel());
            }
            i.setItemMeta(im);
            s.sendMessage("[c]Item has been enchanted with [pc]" + ItemEnchantUtil.fixName(x));
        }
        if(args.length == 2) {
            ItemStack i = s.getUser().getItemInHand();
            if(i == null || i.getType() == Material.AIR) {
                s.sendMessage("[c]Item cannot be air!");
                return;
            }
            Enchantment x = getEnchantment(args[0]);
            if(x == null) {
                s.sendMessage("[c]Invalid Enchantment");
                return;
            }
            int level = 0;
            if(!NumberUtil.isInt(args[1])) {
                s.sendMessage(Invalidations.INVALID_NUMBER);
                return;
            }
            level = NumberUtil.getInt(args[1]);
            ItemMeta im = i.getItemMeta();
            if(x instanceof CustomEnchant) {
                CustomEnchant c = (CustomEnchant) x;
                if(level <= 0) {
                    im = ItemEnchantUtil.removeEnchant(i, c, ItemEnchantUtil.getEnchant(i, c));
                    i.setItemMeta(im);
                }
                if(level > 0)
                    im = ItemEnchantUtil.forceAddEnchantment(i, c, level);
                i.setItemMeta(im);
            }else {
                if(level <= 0) {
                    im = ItemEnchantUtil.removeBukkitEnchant(i, x);
                    i.setItemMeta(im);
                }
                if(level > 0)
                    im = ItemEnchantUtil.forceAddBukkitEnchantment(i, x, level);
            }
            i.setItemMeta(im);
            s.sendMessage("[c]Item has been enchanted with [pc]" + ItemEnchantUtil.fixName(x) + "[c] lvl [pc]" + level + "[c]([pc]" + NumberUtil.convertToRoman(level) + "[c])");
        }
    }


    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> enchants = Lists.newArrayList();
        for(Enchantment e : Enchantment.values()) {
            if(!enchants.contains(StringUtils.stripColor(ItemEnchantUtil.fixName(e).replace(" ", "_").toLowerCase())))
                enchants.add(StringUtils.stripColor(ItemEnchantUtil.fixName(e).replace(" ", "_").toLowerCase()));
        }
        for(CustomEnchant e : EnchantHandler.getEnchantments()) {
            if(!enchants.contains(StringUtils.stripColor(e.getName().toLowerCase()))) enchants.add(StringUtils.stripColor(e.getName().toLowerCase()));
        }

        if(args.length == 1) {
            for(String x : enchants) {
                Enchantment e = getEnchantment(x);
                if(x.toLowerCase().startsWith(args[0].toLowerCase()) && e.canEnchantItem(s.getUser().getItemInHand())) {
                    f.add(x);
                    continue;
                }
                if(args[0].length() > 0) {
                    if(x.toLowerCase().startsWith(args[0].toLowerCase())) f.add(x);
                }
            }
            return f;
        }
        if(args.length == 2) {
            Enchantment enc = getEnchantment(args[0]);
            if(enc != null) {
                for(int i = enc.getStartLevel(); i < enc.getMaxLevel(); i++) {
                    if(!f.contains(i+""))f.add(i+"");
                }
            }
            return f;
        }

        return super.onTab(s, cl, args);
    }

    private Enchantment getEnchantment(String name) {
        if(EnchantHandler.getEnchantment(name) != null) return EnchantHandler.getEnchantment(name);
        for(Enchantment x : Enchantment.values()) {
            if(StringUtils.stripColor(x.getName()).equalsIgnoreCase(StringUtils.stripColor(name))) return x;
            if(StringUtils.stripColor(ItemEnchantUtil.fixName(x)).equalsIgnoreCase(StringUtils.stripColor(name))) return x;
            if(StringUtils.stripColor(ItemEnchantUtil.fixName(x).replace(" ", "")).equalsIgnoreCase(StringUtils.stripColor(name))) return x;
            if(StringUtils.stripColor(ItemEnchantUtil.fixName(x).replace(" ", "_")).equalsIgnoreCase(StringUtils.stripColor(name))) return x;
        }
        if(Enchantment.getByName(name) != null) return Enchantment.getByName(name);

        return null;
    }
}
