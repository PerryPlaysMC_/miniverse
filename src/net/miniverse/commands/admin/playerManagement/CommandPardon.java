package net.miniverse.commands.admin.playerManagement;

import com.google.common.collect.Lists;
import net.miniverse.chat.Invalidations;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;

import java.util.List;

public class CommandPardon extends MiniCommand {
    public CommandPardon() {
        super("pardon", "unban");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) { s.sendMessage(Invalidations.SPECIFY_PLAYER);return; }if(args.length > 1) { s.sendMessage(Invalidations.TOO_MANY_ARGUMENTS);return; }
        if(validTarget(args[0], false)) return;
        OfflineUser u = Miniverse.getOfflineUser(args[0]);
        if(Miniverse.getBan(u.getUUID()) == null) {
            s.sendMessage("[c]That player is not banned.");
            return;
        }
        Miniverse.pardonPlayer(u.getUUID());
        s.sendMessage("[pc]" + u.getName() + "[c] has been unbanned");
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> users = Lists.newArrayList();
        for(OfflineUser u : Miniverse.getOfflineUsers()) {
            if(Miniverse.getBan(u.getUUID()) != null) users.add(u.getName());
        }
        if(args.length == 1) {
            for(String x : users) {
                if(x.toLowerCase().startsWith(args[0].toLowerCase())) f.add(x);
            }
            f.isEmpty();
            return f;
        }
        return f;
    }
}
