package net.miniverse.commands.admin.playerManagement;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.Perm;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.GameMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("all")
public class CommandGamemode extends MiniCommand {

    public CommandGamemode() {
        super("gamemode", new Perm("miniverse.command.gamemode"), "mgamemode", "gmc", "gms", "gma", "gmsp", "gm", "mgmc", "mgms", "mgma", "mgmsp", "mgm");
        this.desc = "Switch your gamemode easily";
        this.usage = "/<command>";
        permission = "miniverse.command.gamemode";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(checkString(cl.getName(), "gamemode", "mgamemode", "gm", "mgm")) {
            if(args.length >= 1) {
                if(args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("creative")) {
                    setGamemode(s, GameMode.CREATIVE, cl, args);
                }
                else if(args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("survival")) {
                    setGamemode(s, GameMode.SURVIVAL, cl, args);
                }
                else if(args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("adventure")) {
                    setGamemode(s, GameMode.ADVENTURE, cl, args);
                }
                else if(args[0].equalsIgnoreCase("sp") || args[0].equalsIgnoreCase("spectator")) {
                    setGamemode(s, GameMode.SPECTATOR, cl, args);
                }else {
                    sendCMessage("Gamemode.unknown-gamemode");
                }
            }else {
                GameMode gm = s.getBase().getGameMode();
                String gamemode = gm.name().charAt(0) + gm.name().substring(1).toLowerCase();
                sendCMessage("Gamemode.currentGamemode", gamemode);
            }
        }
        if(cl.isIgnoreCase("gms") || cl.isIgnoreCase("mgms")) {
            if(sendInvalidPermission( "gamemode.survival", "gamemode.*")) return;
            setGamemodeGM(s, GameMode.SURVIVAL, args);
        }
        if(cl.isIgnoreCase("gmsp") || cl.isIgnoreCase("mgmsp")) {
            if(sendInvalidPermission( "gamemode.spectator", "gamemode.*")) return;
            setGamemodeGM(s, GameMode.SPECTATOR, args);
        }
        if(cl.isIgnoreCase("gma") || cl.isIgnoreCase("mgma")) {
            if(sendInvalidPermission( "gamemode.adventure", "gamemode.*")) return;
            setGamemodeGM(s, GameMode.ADVENTURE, args);
        }
        if(cl.isIgnoreCase("gmc") || cl.isIgnoreCase("mgmc")) {
            if(sendInvalidPermission( "gamemode.creative", "gamemode.*")) return;
            setGamemodeGM(s, GameMode.CREATIVE, args);
        }
    }

    void setGamemode(CommandSource s, GameMode gm, Label cl, String[] args) {
        String gamemode = gm.name().charAt(0) + gm.name().substring(1).toLowerCase();
        if(args.length == 1) {
            if(sendInvalidPermission("gamemode." + gm.name().toLowerCase(), "gamemode.*")) return;
            if(!s.isPlayer()) {
                sendCMessage("Invalidations.specify-player");
                return;
            }
            if(s.getBase().getGameMode() == gm) {
                sendCMessage("Gamemode.gamemode-same", gamemode, gm.name());
                return;
            }
            sendCMessage("Gamemode.gamemode-updated", gamemode, gm.name());
            s.getUser().setGamemode(gm);
        }else if(args.length == 2) {
            User t = Miniverse.getUser(args[1]);
            if(t == null) {
                sendCMessage("Invalidations.invalid-player", args[1]);
                return;
            }
            if(t.getName() == s.getName()) {
                if(s.getBase().getGameMode() == gm) {
                    sendCMessage("Gamemode.gamemode-same", gamemode, gm.name());
                    return;
                }
                sendCMessage("Gamemode.gamemode-updated", gamemode, gm.name());
                s.getUser().setGamemode(gm);
            }else {
                if(t.getBase().getGameMode() == gm) {
                    sendCMessage("Gamemode.gamemode-same-target", t.getName(), gamemode);
                    return;
                }
                t.getBase().setGameMode(gm);
                sendCMessage("Gamemode.gamemode-updated-target", t.getName(), gamemode, gm.name());
                if(Miniverse.getSettings().getBoolean("Commands.Gamemode.send-update-message-to-target")) {
                    t.sendMessage(Miniverse.getSettings(), "Commands.Gamemode.gamemode-updated-target-message", s.getName(), gamemode, gm.name());
                }
            }
        }
    }

    void setGamemodeGM(CommandSource s, GameMode gm, String[] args) {
        String gamemode = gm.name().charAt(0) + gm.name().substring(1).toLowerCase();
        if(args.length == 0) {
            if(!s.isPlayer()) {
                sendCMessage("Invalidations.specify-player");
                return;
            }
            if(s.getBase().getGameMode() == gm) {
                sendCMessage("Gamemode.gamemode-same", gamemode, gm.name());
                return;
            }
            sendCMessage("Gamemode.gamemode-updated", gamemode, gm.name());
            s.getUser().setGamemode(gm);
        }
        if(args.length == 1) {
            User t = Miniverse.getUser(args[0]);
            if(t == null) {
                sendCMessage("Invalidations.invalid-player", args[0]);
                return;
            }
            if(t.getName() == s.getName()) {
                if(s.getBase().getGameMode() == gm) {
                    sendCMessage("Gamemode.gamemode-same", gamemode, gm.name());
                    return;
                }
                sendCMessage("Gamemode.gamemode-updated", gamemode, gm.name());
                s.getUser().setGamemode(gm);
            }else {
                if(t.getBase().getGameMode() == gm) {
                    sendCMessage("Gamemode.gamemode-same-target", t.getName(), gamemode);
                    return;
                }
                t.getBase().setGameMode(gm);
                sendCMessage("Gamemode.gamemode-updated-target", t.getName(), gamemode, gm.name());
                if(Miniverse.getSettings().getBoolean("Commands.Gamemode.send-update-message-to-target")) {
                    t.sendMessage(Miniverse.getSettings(), "Commands.Gamemode.gamemode-updated-target-message", s.getName(), gamemode, gm.name());
                }
            }
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> users = new ArrayList<>();
        List<String> gamemodes = Arrays.asList("creative", "survival", "spectator", "adventure");
        List<String> f = Lists.newArrayList();
        if (args.length == 0) {
            return ImmutableList.of();
        }
        for(User u : core.getOnlineUsers()) {
            if(u != null && !users.contains(u.getName())) {
                users.add(u.getName());
            }
        }
        if(args.length == 1) {
            if(cl.isIgnoreCase("gm", "gamemode", "mgm", "mgamemode"))
                for(String a : gamemodes) {
                    if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
                }
                else
                for(String a : users) {
                    if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
                }

            return f;
        }
        String lastWord = args[(args.length - 1)];
        if(cl.isIgnoreCase("gm", "gamemode", "mgm", "mgamemode") && args.length == 2)
        for(String a : users) {
            if(a.toLowerCase().startsWith(lastWord.toLowerCase())) f.add(a);
        }
        return f;
    }

    boolean checkString(String labelToCheck, String... aliases) {
        for(String alias : aliases) {
            if(labelToCheck.equalsIgnoreCase(alias)) return true;

        }
        return false;
    }

}
