package net.miniverse.commands.admin.playerManagement;

import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.TimeUtil;

import java.util.Date;

public class CommandBan extends MiniCommand {
    public CommandBan() {
        super("ban", "mban", "exile", "banish", "mbanish", "mexile", "shun", "tempban");
        desc = "BAN A PLAYER";
        permission = "miniverse.command.ban";
        this.isOnlineTab=false;
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("ban")) return;
        Config ban = Miniverse.getBannedPlayers();
        if(args.length == 0) {
            sendUsage(s);
            return;
        }
        if(cl.isIgnoreCase("tempban")) {

            if(args.length >= 3) {
                OfflineUser t = Miniverse.getOffline(args[0]);
                if(validTarget(args[0], false)) {
                    sendInvalidTarget(args[0]);
                    return;
                }
                String a = formMessage(1, args.length);
                boolean isSilent = a.contains("-s");
                String r = isSilent ? formMessage(1, args.length).split("-s")[0] : a;
                String time = args[args.length +-1];
                long banTimestamp;
                try {
                    banTimestamp = TimeUtil.parseDateDiff(args[args.length + -1], true);
                } catch (Exception e) {
                    s.sendMessage(e.getMessage());
                    return;
                }
                String reason = TimeUtil.removeTimePattern(Miniverse.format(Miniverse.getSettings(),"BanReasonPrefix", TimeUtil.formatDateDiff((banTimestamp)), r));

                if(t.getBase().isOnline()) {
                    User u = Miniverse.getUser(t.getName());
                    u.kick(s, reason, true);
                }
                Miniverse.BanPlayer(Miniverse.BanType.NAME, s.getName(), t.getUUID(), r, banTimestamp);
                if(isSilent) {
                    Miniverse.broadCastPerm("ban.silent.see", "[pc]" + s.getName() + "[c] Silently Banned: [pc]" + t.getName() + "\n" +
                                                              "[c]For: " + TimeUtil.removeTimePattern(r) + "\nExpires: " + TimeUtil.formatDate(new Date(banTimestamp)));
                }else {
                    Miniverse.broadCast("[pc]" + s.getName() + "[c] Banned: [pc]" + t.getName() + "\n" +
                                        "[c]For: " + TimeUtil.removeTimePattern(r) + "\nExpires: " + TimeUtil.formatDate(new Date(banTimestamp)));
                }
            }else{
                sendUsage(s);
            }
            return;
        }
        if(args.length == 1) {
            OfflineUser t = Miniverse.getOffline(args[0]);
            if(validTarget(args[0], false)) {
                sendInvalidTarget(args[0]);
                return;
            }
            String r = Miniverse.getSettings().getString("DefaultBanReason");
            String reason = Miniverse.format(Miniverse.getSettings(),"BanReasonPrefix", "forever", r);
            Miniverse.BanPlayer(Miniverse.BanType.NAME, s.getName(), t.getUUID(), r);
            if(t.getBase().isOnline()) {
                User u = Miniverse.getUser(t.getName());
                u.kick(s, reason, true);
            }
        }
        if(args.length >= 2) {
            OfflineUser t = Miniverse.getOffline(args[0]);
            if(validTarget(args[0], false)) {
                sendInvalidTarget(args[0]);
                return;
            }
            String a = formMessage(1, args.length);
            boolean isSilent = a.endsWith("-s");
            String r = isSilent ? formMessage(1, args.length).split("-s")[0] : a;
            String reason = Miniverse.format(Miniverse.getSettings(),"BanReasonPrefix", "forever", r);
            Miniverse.BanPlayer(Miniverse.BanType.NAME, s.getName(), t.getUUID(), r);
            if(t.getBase().isOnline()) {
                User u = Miniverse.getUser(t.getName());
                u.kick(s, reason, true);
            }
            if(isSilent) {
                Miniverse.broadCastPerm("ban.silent.see", "[pc]"+s.getName() + "[c] Banned: [pc]" + t.getName() + "\n'"+reason + "'");
            }else {
                Miniverse.broadCast("[pc]"+s.getName() + "[c] Banned: [pc]" + t.getName() + "\n'"+reason + "'");
            }
        }
    }

    void sendUsage(CommandSource s) {
        Message msg = new Message(s,"[c]&m&l      &r[pc]BanData Commands[c]&m&l      ");
        msg.then("ban","\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional" +
                       " \n [r]<argument>[c] &7= Required" +
                       " \n [a][-s][c] &7= Silent" +
                       "\n");
        msg.then("ban","\n [c]-/[pc]banish[c] [r]<Player> [o][reason] [o][-s][pc]")
                .tooltip("[c]Info: [pc]BanData a player from the server!",
                        "[c]Click to insert:",
                        "[pc]/banish [r]<Player> [o][reason]")
                .suggest("/banish <player> [reason]");
        msg.then("tempban","\n [c]-/[pc]tempban[c] [r]<Player> [o][reason] [o][-s] [r]<time>m,w,d,min[pc]")
                .tooltip("[c]Info: [pc]BanData a player from the server!",
                        "[c]Click to insert:",
                        "[pc]/tempban [r]<Player> [o][reason] [o][-s] [r]<time>m,w,d,min[pc]")
                .suggest("/tempban <player> [reason] [-s] <time>m,w,d,min");

        msg.send(s);
    }



}
