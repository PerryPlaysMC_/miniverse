package net.miniverse.commands.admin.playerManagement;

import net.minecraft.server.v1_14_R1.BlockPosition;
import net.miniverse.chat.Invalidations;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.user.kits.Kit;
import net.miniverse.user.kits.KitManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class CommandKit extends MiniCommand {
    public CommandKit() {
        super("kit", "mkit");
        desc = "Receive a kit from the /createkit command";
        permission = "miniverse.command.kit";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 1) {
            if(sendInvalidPermission("miniverse.kits", "kit." + args[0].toLowerCase())) return;
            Kit k = KitManager.kitByName(args[0]);
            if(k == null) {
                sendCMessage("Kit.invalid", args[0]);
                return;
            }
            List<ItemStack> c = k.getContents();
            HashMap<Integer, ItemStack> excess = s.getUser().getInventory().addItem(c.<ItemStack>toArray(new ItemStack[0]));
            if(excess.entrySet().size() > 0) {
                for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
                    s.getUser().getBase().getWorld().dropItem(s.getBase().getLocation().add(0.5, 0.5, 0.5), me.getValue());
                }
            }
            sendCMessage("Kit.receive", k.getName(), args[0]);
//            Block b = s.getBase().getTargetBlock((HashSet<Material>)null, 5);
//            int x = b.getX(), y = b.getY(), z = b.getZ();
//            s.getUser().getCraftPlayer().getHandle().playerInteractManager.a(new BlockPosition(x,y,z));
        }else if(args.length == 2) {
            User t = Miniverse.getUser(args[1]);
            Kit k = KitManager.kitByName(args[0]);
            if(sendInvalidPermission("kit.other")) return;
            if(k == null) {
                sendCMessage("Kit.invalid", args[0]);
                return;
            }
            if(validTarget(args[1])||t==null) {
                sendInvalidTarget(args[1]);
                return;
            }
            if(!s.isPlayer() || t.getName() != s.getName()) {
                if(sendInvalidPermission("kit." + k.getName().toLowerCase())) return;
                HashMap<Integer, ItemStack> excess = t.getInventory().addItem(k.getContents().toArray(new ItemStack[k.getContents().size()]));
                if(excess.entrySet().size() > 0) {
                    for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
                        t.getBase().getWorld().dropItem(s.getBase().getLocation().add(0.5, 0.5, 0.5), me.getValue());
                    }
                }

                sendCMessage("Kit.send", k.getName(), t.getName(), args[0], args[1]);
                if(Miniverse.getSettings().getBoolean("Commands.Kit.send-other?"))
                    sendCMessage("Kit.send-other", k.getName(), s.getName(), args[0], args[1]);
                return;
            }
            if(t.getName() == s.getName()) {
                HashMap<Integer, ItemStack> excess = s.getUser().getInventory().addItem(k.getContents().toArray(new ItemStack[k.getContents().size()]));
                if(excess.entrySet().size() > 0) {
                    for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
                        s.getUser().getBase().getWorld().dropItem(s.getBase().getLocation().add(0.5, 0.5, 0.5), me.getValue());
                    }
                }
                sendCMessage("Kit.receive", k.getName(), args[0]);
            }
        }else if(args.length == 0) {
            KitManager.getKits(s).send(s);
        }else{
            s.sendMessage(Invalidations.TOO_MANY_ARGUMENTS);
        }
    }



}
