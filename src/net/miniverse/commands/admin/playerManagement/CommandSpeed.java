package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;

public class CommandSpeed extends MiniCommand {

	public CommandSpeed() {
		super("speed", "sp", "mspeed", "msp");
		desc = "Move at the speed of light or slower than a snail with this command";
		permission = "miniverse.command.speed";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission("speed")) return;
		if(args.length == 0) {
			sendCMessage("Invalidations.more-arguments");
			return;
		}
		if(args.length == 1) {
			if(sendInvalidPermission("speed")) return;
			if(!s.isPlayer()) {
				sendCMessage("Invalidations.specify-player", args[0]);
				return;
			}
			int speed = 1;
			if(args[0].equalsIgnoreCase("reset")) {
				s.getBase().setFlySpeed(speed / 10.0F);
				speed = 2;
				s.getBase().setWalkSpeed(speed / 10.0F);
				speed = 1;
				return;
			}
			if(!NumberUtil.isInt(args[0])) {
				sendCMessage("Invalidations.invalid-number", speed);
				return;
			}
			speed = NumberUtil.getInt(args[0]);
			if(!s.getBase().isFlying()) {
				s.getBase().setWalkSpeed(speed / 10.0F);
				sendCMessage("Speed.setWalkSpeed", speed);
			}else {
				s.getBase().setFlySpeed(speed / 10.0F);
				sendCMessage("Speed.setFlySpeed", speed);
			}
		}
	}

}
