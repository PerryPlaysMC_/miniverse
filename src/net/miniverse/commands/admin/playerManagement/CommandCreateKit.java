package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.Perm;
import net.miniverse.user.CommandSource;
import net.miniverse.user.kits.Kit;
import net.miniverse.user.kits.KitManager;

import java.util.Arrays;

public class CommandCreateKit extends MiniCommand {

    public CommandCreateKit(){
        super("createkit", new Perm("miniverse.command.createkit"), "ck", "mcreatekit", "mck");
        desc = "Create a kit for people to use";
        permission = "miniverse.command.createkit";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(sendInvalidPermission("createkit")) return;
        if(args.length == 1) {
            Kit k = KitManager.kitByName(args[0]);
            if(k != null) {
                sendCMessage("Kit.already-exists", KitManager.kitByName(args[0]).getName(), args[0]);
                return;
            }
           k = KitManager.addKit(args[0], Arrays.asList(s.getBase().getInventory().getContents()));
            sendCMessage("Kit.create", k.getName(), args[0]);
            KitManager.loadKits();
        }else{
            sendTooManyArguments();
        }
    }
}
