package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandFly extends MiniCommand {
    public CommandFly() {
        super("fly");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) {
            if(mustBePlayer()) return;
            if(sendInvalidPermission("fly", "fly.toggle"))return;
            s.getUser().getBase().setAllowFlight(!s.getUser().getBase().getAllowFlight());
            String msg = s.getUser().getBase().getAllowFlight() ? "Enabled" : "Disabled";
            sendCMessage("Fly.toggle-Self", msg);
        }
        if(args.length == 1) {
            if(validTarget(args[0], true)) return;
            User u = Miniverse.getUser(args[0]);
            if(s.getName().equalsIgnoreCase(u.getName())) {
                if(sendInvalidPermission("fly", "fly.toggle")) return;
                u.getBase().setAllowFlight(!s.getUser().getBase().getAllowFlight());
                String msg = u.getBase().getAllowFlight() ? "Enabled" : "Disabled";
                sendCMessage("Fly.toggle-Self", msg);
                return;
            }
            if(sendInvalidPermission("fly.other", "fly.toggle.other")) return;
            u.getBase().setAllowFlight(!u.getBase().getAllowFlight());
            String msg = u.getBase().getAllowFlight() ? "Enabled" : "Disabled";
            sendCMessage("Fly.toggle-Other", msg, u.getName());
        }
    }
}
