package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class CommandVanish extends MiniCommand {

	public CommandVanish() {
		super("vanish", "v");
		desc = "Become a spy and VANISH";
		permission = "miniverse.command.vanish";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission("vanish")) return;
		s.setVanish(!s.isVanished());
		String vanish = (s.isVanished() ? "Vanished" : "unVanished");
		s.sendMessage("[c]You have been [pc]" + vanish);
	}

}
