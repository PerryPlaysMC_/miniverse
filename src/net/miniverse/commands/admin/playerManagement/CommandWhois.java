package net.miniverse.commands.admin.playerManagement;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;

public class CommandWhois extends MiniCommand {
    public CommandWhois() {
        super("whois");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) {
            sendMoreArguments();
            return;
        }
        User u = Miniverse.getUserByAll(args[0]);
        if(u == null) {
            sendInvalidTarget(args[0]);
            return;
        }
        String groups = u.getGroups() != null && u.getGroups().size() > 0 && u.getGroups().get(0)!=null ? u.getGroups().get(0).getName() : "";
        for(int i = 1; i < u.getGroups().size(); i++) {
            Group g = u.getGroups().get(i);
            groups+="[c], [pc]"+g.getName();
        }
        if(groups.isEmpty()) groups = "No groups";

        s.sendMessage(
                "[c]----Name: " + u.getOriginalName(),
                "[c]UUID: [pc]" + u.getUUID(),
                "[c]isOp: [pc]" + u.getBase().isOp(),
                "[c]isAFK: [pc]" + u.isAFK(),
                "[c]veinMine: [pc]" + u.isVeinMine(),
                "[c]vanished: [pc]" + u.isVanished(),
                "[c]Gamemode: [pc]" + StringUtils.getNameFromEnum(u.getGameMode()),
                "[c]Balance: $[pc]" + u.getBalance(),
                "[c]Prefix: [pc]" + u.getPrefix(),
                "[c]PrimaryGroup: [pc]" + (u.getPrimaryGroup() != null ? u.getPrimaryGroup().getName() : "No Group"),
                "[c]Groups: [pc]" + groups);
    }
}
