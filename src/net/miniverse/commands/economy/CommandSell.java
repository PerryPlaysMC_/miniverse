package net.miniverse.commands.economy;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.economy.EconomyHelper;
import net.miniverse.gui.shops.ItemValues;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class CommandSell extends MiniCommand {
    public CommandSell() {
        super("sell", "sellall", "sellhand", "sellallhand");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        User u = s.getUser();
        if(cl.isIgnoreCase("sellall")) run(s, new Label("sell"), new String[]{"all"});
        if(cl.isIgnoreCase("sellallhand")) run(s, new Label("sell"), new String[]{"allhand"});
        if(cl.isIgnoreCase("sellhand")) run(s, new Label("sell"), new String[]{"hand"});
        if(cl.isIgnoreCase("sell")) {
            if(args.length != 1) {
                s.sendMessage("[c]Usage: /sell [all/hand/allhand]");
                return;
            }
            if(args[0].equalsIgnoreCase("hand")) {
                if(u.getItemInHand().getType() == Material.AIR || u.getItemInHand().hasItemMeta()) {
                    s.sendMessage("[c]Invalid item");
                    return;
                }
                ItemValues val = MiniShopHandler.getValue(u.getItemInHand());
                if(val == null) {
                    s.sendMessage("[c]Item is not sellable");
                    return;
                }
                if(u.balanceTooBig(u.getItemInHand().getAmount()*val.getSell())) {
                    s.sendMessage("[c]Selling this will exceed your current balance");
                    return;
                }
                s.sendMessage("[c]Sold [pc]" + StringUtils.getNameFromEnum(u.getItemInHand().getType())
                              + "[c] For $[pc]" + NumberUtil.format(u.getItemInHand().getAmount()*val.getSell()));
                EconomyHelper.sellItem(u, false, u.getItemInHand().getAmount(), val.getSell(), u.getItemInHand().getType());
            }else if(args[0].equalsIgnoreCase("all")) {
                boolean hasSold1 = false;
                for(ItemStack a : u.getInventory().getStorageContents()) {
                    if(a == null || a.getType() == Material.AIR || a.hasItemMeta()) continue;
                    ItemValues val = MiniShopHandler.getValue(a);
                    if(val==null)continue;
                    if(u.balanceTooBig(a.getAmount()*val.getSell())) {
                        continue;
                    }
                    s.sendMessage("[c]Sold all [pc]" + StringUtils.getNameFromEnum(a.getType())
                                  + "[c] For $[pc]" + NumberUtil.format(a.getAmount()*val.getSell()));
                    EconomyHelper.sellItem(u, false, a.getAmount(), val.getSell(), a.getType());
                    hasSold1 = true;
                }
                if(!hasSold1)
                    s.sendMessage("[c]You don't have any sellable items.");
            }else if(args[0].equalsIgnoreCase("allHand")) {
                if(u.getItemInHand().getType() == Material.AIR) {
                    s.sendMessage("[c]Invalid item");
                    return;
                }
                ItemValues val = MiniShopHandler.getValue(u.getItemInHand());
                if(val == null || u.getItemInHand().hasItemMeta()) {
                    s.sendMessage("[c]Item is not sellable");
                    return;
                }
                if(u.balanceTooBig(EconomyHelper.getItems(u, u.getItemInHand().getType())*val.getSell())) {
                    s.sendMessage("[c]Selling this will exceed your current balance");
                    return;
                }
                s.sendMessage("[c]Sold all [pc]" + StringUtils.getNameFromEnum(u.getItemInHand().getType())
                              + "[c] For $[pc]" + NumberUtil.format(EconomyHelper.getItems(u, u.getItemInHand().getType())*val.getSell()));
                EconomyHelper.sellAll(u, false, val.getSell(), u.getItemInHand());
            }
        }

    }
}
