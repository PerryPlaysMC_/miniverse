package net.miniverse.commands.economy;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.events.ShopInteract;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.listeners.ShopAddItemsEvents;
import net.miniverse.user.CommandSource;
import net.miniverse.util.itemutils.MiniItem;
import org.bukkit.inventory.ItemStack;

public class CommandShop extends MiniCommand {

	public CommandShop() {
		super("shop");
		desc = "Opens the shop GUI!";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("edit")) {
				if(mustBePlayer())return;
				if(sendInvalidPermission("shop.additem")) return;
				ShopAddItemsEvents.Show(s.getUser());
			}else if(args[0].equalsIgnoreCase("reload")) {
				if(sendInvalidPermission("shop.reload")) return;
				s.sendMessage("[c]Reloaded shops");
				MiniShopHandler.reload();
			}else {
				if(sendInvalidPermission("shop.reload")) return;
				s.sendMessage("[c]Correct usage: /shop reload");
				if(sendInvalidPermission("shop.additem")) return;
				s.sendMessage("[c]Correct usage: /shop item add <itemname/hand> <sellprice> <buyprice>");
			}
		}else if(args.length == 5) {
			if(args[0].equalsIgnoreCase("item") && args[1].equalsIgnoreCase("add")) {
				ItemStack im = null;
				try {
					im = MiniItem.getItem(args[2]);
				} catch (Exception e) {
					sendCMessage("Invalidations.unknown-item", args[2]);
					return;
				}
			}
		}else{
			ShopInteract.showInventory(s.getUser());
		}
	}

}
