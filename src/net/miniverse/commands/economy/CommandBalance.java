package net.miniverse.commands.economy;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;

public class CommandBalance extends MiniCommand {

	public CommandBalance() {
		super("balance", "bal", "mbalance", "mbal", "money", "mon");
		this.desc = "Get your balance!";
		permission = "miniverse.command.eco.bal";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission("eco.bal")) {
			return;
		}
		if(!s.isPlayer()) {
			s.sendMessage("[c]Your balance is ∞");
			return;
		}
		s.sendMessage("[c]Your balance: $[pc]" + s.getUser().getBalance());
		
	}
	
}
