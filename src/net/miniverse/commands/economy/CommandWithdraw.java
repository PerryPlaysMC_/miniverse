package net.miniverse.commands.economy;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;

import java.text.DecimalFormat;

public class CommandWithdraw extends MiniCommand {
    public CommandWithdraw() {
        super("withdraw", "wd");
        desc = "Take some money out of your account and put it into a note!";
        permission = "miniverse.command.banknote";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(sendInvalidPermission("banknote")) return;
        if(args.length == 0 || (args.length ==1 && !NumberUtil.isDouble(args[0])) || args.length > 1) {
            s.sendMessage("&cError: &4Usage /withdraw <amount>");
            return;
        }
        if(!s.getUser().hasEnough(NumberUtil.getMoney(args[0]))) {
            sendCMessage("Withdraw.notEnoughMoney");
            return;
        }
        if(s.getUser().withdrawItem(NumberUtil.getMoney(args[0]))) {
            sendCMessage("Withdraw.InventoryFull", NumberUtil.format(NumberUtil.getMoney(args[0])));
            return;
        }
        sendCMessage("Withdraw.success", NumberUtil.format(NumberUtil.getMoney(args[0])));
    }
}
