package net.miniverse.commands.economy;

import java.text.DecimalFormat;
import java.util.Map.Entry;

import net.miniverse.command.Label;
import net.miniverse.util.NumberUtil;
import org.bukkit.util.ChatPaginator;

import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.util.economy.EconomyManager;

public class CommandBalanceTop extends MiniCommand {
	public CommandBalanceTop() {
		super("balancetop", "baltop", "bt", "mbalancetop", "mbaltop", "mbt");
		this.desc = "Get the top balances!";
		permission = "miniverse.command.eco.baltop";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission( "eco.baltop")) {
			return;
		}
		if(args.length == 0) {
			if(sendInvalidPermission( "eco.baltop")) {
				return;
			}
			baltopPage(s, 1);
		}else if(args.length > 0) {
			if(sendInvalidPermission( "eco.baltop")) {
				return;
			}
			int pageNumber;
			try {
				pageNumber = Integer.parseInt(args[args.length +-1]);
			}catch (Exception e) {
				pageNumber = 1;
			}
			baltopPage(s, pageNumber);
		}
	}

	void baltopPage(CommandSource s, int pageNumber) {
		StringBuilder sb = new StringBuilder();
		int index = 1;
		for(Entry<String, Double> e : net.miniverse.util.MapSorting.reversedValues(EconomyManager.getBaltop())) {
			sb.append(("[c] ([pc]" + index + "[c]) -" + ("[pc]" + e.getKey() + "[c]: $[pc]" + NumberUtil.format(e.getValue())) + "\n"));
			index++;
		}
		ChatPaginator.ChatPage p = ChatPaginator.paginate(sb.toString().trim(), pageNumber, Integer.MAX_VALUE, 10);
		s.sendMessage("[c]Balance Tops: [c]([pc]" + p.getPageNumber() + "[c]/[pc]" + p.getTotalPages() + "[c])");
		s.sendMessage("[c]&l&m-----------------------------");
		s.sendMessage(p.getLines());
		s.sendMessage("[c]&l&m-----------------------------");
		
	}

}
