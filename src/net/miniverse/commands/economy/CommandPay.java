package net.miniverse.commands.economy;

import java.text.DecimalFormat;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.NumberUtil;

public class CommandPay extends MiniCommand {

	public CommandPay() {
		super("pay", "mpay");

		this.desc = "Pay someone some Money";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(args.length == 2) {
			double money = 0;
			if(!NumberUtil.isDouble(args[1])) {
				money = 0;
				s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-number", money);
			}else {
				money = NumberUtil.getMoney(args[1]);
			}
			OfflineUser t = (Miniverse.getOfflineUser(args[0]) == null ? Miniverse.getUser(args[0]) : Miniverse.getOfflineUser(args[0]));
			if(t==null) {
				s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-player", args[0]);
				return;
			}
			if(s.isPlayer()) {
				User u = s.getUser();
				if(money < Miniverse.getSettings().getDouble("Commands.Economy.minPay")) {
					s.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-too-small", money, Miniverse.getSettings().getDouble("Commands.Economy.minPay"));
					return;
				}
				if(u.hasEnough(money)) {
					u.withDrawMoney(money);
					t.depositMoney(money);
					if(Miniverse.getSettings().getBoolean("Commands.Economy.pay-sendTarget")) {
						t.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-success-target", s.getName(), t.getName(), NumberUtil.format(money), args[1]);
					}
					s.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-success", t.getName(), NumberUtil.format(money), args[1]);
				}else {
					s.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-fail", NumberUtil.format(money), u.getBalance());
				}

			}else {
				t.depositMoney(money);
				if(Miniverse.getSettings().getBoolean("Commands.Economy.pay-sendTarget")) {
					t.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-success-target", s.getName(), t.getName(), NumberUtil.format(money), args[1]);
				}
				s.sendMessage(Miniverse.getSettings(), "Commands.Economy.pay-success", t.getName(), NumberUtil.format(money), args[1]);
			}
		}
	}

}
