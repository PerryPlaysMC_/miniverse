package net.miniverse.commands.economy;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.util.config.Economy;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.util.economy.EconomyManager;
import net.miniverse.util.NumberUtil;
import org.bukkit.util.ChatPaginator;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

public class CommandEco extends MiniCommand{

	public CommandEco() {
		super("eco", "meco", "economy", "meconomy");
		this.desc = "Add/Remove money from someones account, Set someones balance, Get someones balance";
		permission = "miniverse.command.eco";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		double money = 1;
		if(!s.hasPermission("eco")) {
			sendCMessage("Invalidations.invalid-permission", name, "miniverse.command.eco");
		}
		if(args.length == 0) {
			sendCMessage("Invalidations.more-arguments");
			return;
		}
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("top") || args[0].equalsIgnoreCase("topbal") || args[0].equalsIgnoreCase("topbalances") || args[0].equalsIgnoreCase("baltop")) {
				if(sendInvalidPermission( "miniverse.command.eco.baltop")) return;
				baltopPage(s, 1);
			}
		}else if(args.length == 2) {
			if(args[0].equalsIgnoreCase("top") || args[0].equalsIgnoreCase("topbal") || args[0].equalsIgnoreCase("topbalances") || args[0].equalsIgnoreCase("baltop")
			   || args[0].equalsIgnoreCase("BalanceTop")) {
				if(sendInvalidPermission( "miniverse.command.eco.baltop")) return;
				int pageNumber = NumberUtil.getInt(args[1]);
				baltopPage(s, pageNumber);
			}else if(args[0].equalsIgnoreCase("get")){
				if(sendInvalidPermission( "miniverse.command.eco.get")) return;
				OfflineUser t = (Miniverse.getOfflineUser(args[1]) == null ? Miniverse.getUser(args[1]) : Miniverse.getOfflineUser(args[1]));
				if(t==null) {
					sendCMessage("Invalidations.invalid-player", args[1]);
					return;
				}
				sendCMessage("Economy.getInfo", t.getName(), t.getBalance(), args[1], t.getBalanceRaw());
			}
		}else if(args.length == 3) {
			if(s.hasPermission("eco.set", "eco.add", "eco.remove")) {
				if(!NumberUtil.isDouble(args[2])) {
					money = 0;
					sendCMessage("Invalidations.invalid-number", money);
					return;
				}else {
					money = NumberUtil.getMoney(args[2]);
				}
			}
			if(args[0].equalsIgnoreCase("set")) {
				if(sendInvalidPermission( "miniverse.command.eco.set")) return;
				OfflineUser t = (Miniverse.getOfflineUser(args[1]) == null ? Miniverse.getUser(args[1]) : Miniverse.getOfflineUser(args[1]));
				if(t==null) {
					sendCMessage("Invalidations.invalid-player", args[1]);
					return;
				}
				if(Economy.isMaxBalance() && Economy.isBalanceGreaterThanMax(money)) {
					sendCMessage("Economy.exceeds-maxBalance", money, t.getName());
					return;
				}
				t.setBal(money);
				sendCMessage("Economy.setBal", t.getName(), NumberUtil.format(money), args[1], money);
			}else if(args[0].equalsIgnoreCase("remove")) {
				if(sendInvalidPermission( "miniverse.command.eco.remove")) return;
				OfflineUser t = (Miniverse.getOfflineUser(args[1]) == null ? Miniverse.getUser(args[1]) : Miniverse.getOfflineUser(args[1]));
				if(t==null) {
					sendCMessage("Invalidations.invalid-player", args[1]);
					return;
				}
				if(!t.hasEnough(money)) {
					sendCMessage("Economy.not-Enough-Money", t.getName(), money);
					return;
				}
				t.withDrawMoney(money);
				sendCMessage("Economy.withDraw", t.getName(), NumberUtil.format(money), t.getBalance(), args[1], money);
				sendCMessage("Economy.newBalance", t.getName(), t.getBalance(), args[1], t.getBalanceRaw());
			}else if(args[0].equalsIgnoreCase("add")) {
				if(sendInvalidPermission( "miniverse.command.eco.add")) return;
				OfflineUser t = (Miniverse.getOfflineUser(args[1]) == null ? Miniverse.getUser(args[1]) : Miniverse.getOfflineUser(args[1]));
				if(t==null) {
					sendCMessage("Invalidations.invalid-player", args[1]);
					return;
				}
				if((t.getBalanceRaw() + money) > Miniverse.getSettings().getDouble("Economy.maxBal")
				   && Miniverse.getSettings().getBoolean("Economy.maxBalance")) {
					sendCMessage("Economy.exceeds-maxBalance", money, t.getName(), t.getBalance(), t.getBalanceRaw() + money);
					return;
				}
				t.depositMoney(money);
				sendCMessage("Economy.deposit", t.getName(), NumberUtil.format(money), t.getBalance(), args[1], money);
				sendCMessage("Economy.newBalance", t.getName(), t.getBalance(), args[1], t.getBalanceRaw());
			}
		}
	}

	void baltopPage(CommandSource s, int pageNumber) {

		DecimalFormat df = new DecimalFormat("###,##0.##");
		StringBuilder sb = new StringBuilder();
		int index = 1;
		for(Entry<String, Double> e : net.miniverse.util.MapSorting.reversedValues(EconomyManager.getBaltop())) {
			sb.append("[c] ([pc]").append(index).append("[c]) -").append("[pc]").append(e.getKey()).append("[c]: $[pc]").append(NumberUtil.format(e.getValue())).append("\n");
			index++;
		}
		ChatPaginator.ChatPage p = ChatPaginator.paginate(sb.toString().trim(), pageNumber, Integer.MAX_VALUE, 10);
		s.sendMessage("[c]Balance Tops: [pc]" + p.getPageNumber() + "[c]/[pc]" + p.getTotalPages());
		s.sendMessage("[c]&l&m-----------------------------");
		s.sendMessage(p.getLines());
		s.sendMessage("[c]&l&m-----------------------------");
	}

	@Override
	public List<String> onTab(CommandSource s, Label cl, String[] args) {
		List<String> a = Arrays.asList("baltop", "set", "add", "remove", "get");
		List<String> f = Lists.newArrayList();
		if(!s.hasPermission("eco")) {
			sendCMessage("Invalidations.invalid-permission", cl, "miniverse.command.eco");
			return f;
		}
		if(args.length == 1) {
			for(String e : a) {
				if(s.hasPermission("eco." + e)) {
					if(e.toLowerCase().startsWith(args[0].toLowerCase())) f.add(e);
				}
			}
			return f;
		}
		if(args.length == 2 && !args[0].equalsIgnoreCase("baltop")) {
			return super.onTab(s, cl, args);
		}
		return f;
	}


}
