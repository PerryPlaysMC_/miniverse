package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CommandFix extends MiniCommand {
    public CommandFix() {
        super("fix", "fixall");
        setDescription("Fix the item in your hand!");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(cl.isIgnoreCase("fixall")) {
            s.getUser().getBase().chat("/fix all");
            return;
        }
        User u = s.getUser();
        if(args.length == 0) {
            if(u.getItemInHand() == null
               || u.getItemInHand().getType().getMaxDurability() == 0
               || u.getItemInHand().getType() == Material.AIR
               || u.getItemInHand().getType().isBlock()
               || u.getItemInHand().getDurability() == 0) {
                s.sendMessage("[c]Invalid item");
                return;
            }
            if(!u.hasEnough(1000)) {
                s.sendMessage("[c]You do not have enough money, You need $1,000");
                return;
            }
            u.withDrawMoney(1000);
            u.getItemInHand().setDurability((short) 0);
            s.sendMessage("[c]Item repaired $[pc]1,000[c] Was withdrawn from your account");
            return;
        }
        if(args.length == 1) {
            if(!args[0].equalsIgnoreCase("all")){
                s.sendMessage("[c]Usage:\n [c]-/[pc]fix [o][all]");
                s.sendMessage("[c] -/[pc]fixall");
                return;
            }
            List<ItemStack> fixes = new ArrayList<>();
            for(ItemStack i : u.getInventory().getContents()) {
                if(i == null
                   || i.getType().getMaxDurability() == 0
                   || i.getType() == Material.AIR
                   || i.getType().isBlock()
                   || i.getDurability() == 0) continue;
                fixes.add(i);
            }
            if(!u.hasEnough(fixes.size()*1000)) {
                s.sendMessage("[c]You do not have enough money, You need $[pc]" + new DecimalFormat("###,###.##").format(fixes.size()*1000));
                return;
            }
            for(ItemStack i : u.getInventory().getContents()) {
                if(i == null
                   || i.getType().getMaxDurability() == 0
                   || i.getType() == Material.AIR
                   || i.getType().isBlock()
                   || i.getDurability() == 0) continue;
                i.setDurability((short)0);
            }
            u.withDrawMoney(fixes.size()*1000);
            s.sendMessage("[c]Fixed all items. $[pc]" + new DecimalFormat("###,###.##").format(fixes.size()*1000) + " [c]Was withdrawn from your account");

        }else {
            s.sendMessage("[c]Usage:\n [c]-/[pc]fix [o][all]");
            s.sendMessage("[c] -/[pc]fixall");
        }
    }
}
