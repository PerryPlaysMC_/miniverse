package net.miniverse.commands.utility.home;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.home.Home;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandSetHome extends MiniCommand {
    public CommandSetHome() {
        super("sethome", "homeset");
        desc = "Sets a home for players to teleport to.";
        permission = "miniverse.command.sethome";
    }
    List<String> invalidCharacters;
    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(invalidCharacters == null) {
            invalidCharacters = Arrays.asList("?", ":", ">", "<", "!", "/", ".", ",", "\"", ";", "'", "`", "~", "|");
        }
        if(!s.isPlayer()) return;
        if(sendInvalidPermission("sethome"))return;
        if(args.length == 1) {
            for(String a : invalidCharacters) {

                if(args[0].contains(a.toLowerCase())) {
                    sendCMessage("Home.setHome.invalidHomeName", a.toLowerCase());
                    return;
                }
            }
            Home home = null;
            if(s.getUser().getHome(args[0]) != null) {
                home = s.getUser().getHome(args[0]).setLocation(s.getUser().getLocation());
            }else {
                if(Miniverse.getSettings().getBoolean("Homes.maxHomes")&&!s.hasPermission("homes.bypasslimit")) {
                    String e = "";
                    List<String> g = new ArrayList<>();
                    for(String a : Miniverse.getSettings().getSection("Homes.groups").getKeys(false)) g.add(a);
                    List<String> groups = g;
                    int highest = 0;
                    for(int i = 1; i < g.size(); i++) {
                        if(s.hasPermission("homes."+groups.get(i)) &&
                           (Miniverse.getSettings().getInt("Homes.groups."+groups.get(i)))
                           < (Miniverse.getSettings().getInt("Homes.groups."+groups.get(highest)))) highest = i;
                    }
                    String a = g.get(highest);
                    if(s.hasPermission("homes." + a)) {
                        e = a;
                    }
                    if(e == "") {
                        if(s.getUser().getHomes().size() >= Miniverse.getSettings().getInt("Homes.default")) {
                            sendCMessage("Home.setHome.maxHomes");
                            return;
                        }
                    }else {
                        if(s.getUser().getHomes().size() >= Miniverse.getSettings().getInt("Homes.groups."+e)) {
                            sendCMessage("Home.setHome.maxHomes");
                            return;
                        }
                    }
                }
                home = s.getUser().setHome(args[0]);
            }
            sendCMessage("Home.setHome.set", args[0]
                    , home.getLocation().getBlockX()
                    , home.getLocation().getBlockY()
                    , home.getLocation().getBlockZ ());
        }
    }
    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        if(args.length == 1) {
            for(Home h : s.getUser().getHomes()) {
                if(h.getName().toLowerCase().startsWith(args[0].toLowerCase())) f.add(h.getName());
            }
        }
        return f;
    }
}
