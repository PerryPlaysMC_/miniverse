package net.miniverse.commands.utility.home;

import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.user.home.Home;

import java.util.List;

public class CommandHome extends MiniCommand {


    public CommandHome() {
        super("home", "homes");
        desc = "Teleport to a home from /sethome!";
        permission = "miniverse.command.home";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("home")) return;
        if(args.length == 0) {
            if(!s.isPlayer()) {
                sendSpecifyTarget();
                return;
            }
            if((!cl.isIgnoreCase("homes") || s.getUser().getHomes().size() == 1) && s.getUser().getHomes().size() > 0) {
                if(s.getUser().getHome("home") != null) {
                    run(s, cl, new String[] {"home"});
                    return;
                }
                if(s.getUser().getHome("h") != null) {
                    run(s, cl, new String[] {"h"});
                    return;
                }
            }
            sendHomes(s.getUser());
            return;
        }
        if(args.length == 1) {
            if(!s.isPlayer()) {
                User t = Miniverse.getUser(args[0]);
                sendHomes(t);
                return;
            }
            if(args[0].contains(":")) {
                if(sendInvalidPermission("home.other")) return;
                OfflineUser t = Miniverse.getOfflineUser(args[0].split(":")[0]);
                if(t == null) {
                    sendInvalidTarget(args[0]);
                    return;
                }
                if(args[0].endsWith(":") || t.getHome(args[0].split(":")[1]) == null) {
                    sendHomes(t);
                    return;
                }
                s.getUser().teleport(t.getHome(args[0].split(":")[1]).getLocation());
                sendCMessage("Home.Home.teleport-Other-Home", t.getName(), t.getHome(args[0].split(":")[1]).getName());
                return;
            }
            if(s.getUser().getHome(args[0]) == null) {
                sendCMessage("Home.Home.InvalidHome", args[0]);
                return;
            }
            s.getUser().teleport(s.getUser().getHome(args[0]).getLocation());
            sendCMessage("Home.Home.teleport", s.getUser().getHome(args[0]).getName());
        }else {
            s.sendMessage("[c]Too many arguments");
        }
    }

    void sendHomes(OfflineUser t) {
        if(t.getName() == s.getName() && t.getHomes().size() == 0) {
            sendCMessage("Home.Home.no-Homes");
            return;
        }
        if(t.getName() != s.getName() && t.getHomes().size() == 0) {
            sendCMessage("Home.Home.no-Homes-Other", t.getName());
            return;
        }
        String e = Miniverse.getSettings().getString("Commands.Home.Home.getHomesListFormat");
        String h = Miniverse.format(Miniverse.getSettings(),"Commands.Home.Home.getHomesTitle", t.getHomes().size()),
                msg = h.endsWith("\n") ? h : h+"\n";
        Message homes = new Message(msg);
        for(int i = 0; i < t.getHomes().size(); i++) {
            String a = e.split("\\{0}")[1];
            if(i == t.getHomes().size()) {
                a = "[c].";
            }
            Home home = t.getHomes().get(i);
            homes.then(e.split("\\{0}")[0] + home.getName());
            homes.tooltip(Miniverse.format("Commands.Home.Home.getHomesHover", home.getName(), home.getLocation().getBlockX()
                    , home.getLocation().getBlockY(), home.getLocation().getBlockZ()
            ));
            if(Miniverse.getSettings().getBoolean("Commands.Home.Home.runCommand")) {
                homes.command(Miniverse.format("Commands.Home.Home.getHomesCommand", home.getName()));
            }else {
                homes.suggest(Miniverse.format("Commands.Home.Home.getHomesCommand", home.getName()));
            }
            homes.then(a);
        }
        homes.send(s);
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        if(args.length == 1) {
            if(args[0].contains(":")) {
                String[] sp = args[0].split(":");
                OfflineUser t = Miniverse.getOfflineUser(sp[0]);
                if(t == null) {
                    sendInvalidTarget(sp[0]);
                    return f;
                }
                if(sp.length == 1) {
                    for (Home h : t.getHomes()) {
                            f.add(sp[0] + ":" + h.getName());
                    }
                    return f;
                }
                for (Home h : t.getHomes()) {
                    if(h.getName().toLowerCase().startsWith(sp[1].toLowerCase()))
                        f.add(sp[0] + ":" + h.getName());
                }
                return f;
            }else {
                for(Home h : s.getUser().getHomes()) {
                    if(h.getName().toLowerCase().startsWith(args[0].toLowerCase())) f.add(h.getName());
                }
                for(User u : Miniverse.getOnlineUsers()) {
                    if(u.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                        if(u.getName().toLowerCase().startsWith(args[0].toLowerCase())) return f;
                    }
                }
                if(f.isEmpty()) {
                    if(sendInvalidPermission("home.other")) return f;
                    for(User u : Miniverse.getOnlineUsers()) {
                        if(u.getName().toLowerCase().startsWith(args[0].toLowerCase())) f.add(u.getName());
                    }
                }
                return f;
            }
        }
        return f;
    }
}
