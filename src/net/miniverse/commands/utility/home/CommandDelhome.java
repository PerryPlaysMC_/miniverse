package net.miniverse.commands.utility.home;

import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.home.Home;

import java.util.List;

public class CommandDelhome extends MiniCommand {
    public CommandDelhome() {
        super("delhome", "homedel", "deletehome");
        desc = "Delete a home made from /sethome!";
        permission = "miniverse.command.delhome";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(!s.isPlayer()) return;
        if(sendInvalidPermission("delhome"))return;
        if(args.length == 0) {
            sendHomes(s.getUser());
            return;
        }
        Home home = null;
        if(s.getUser().getHome(args[0]) == null) {
            sendCMessage("Home.Home.InvalidHome", args[0]);
            return;
        }
        if(s.getUser().getHome(args[0]) != null) {
            home = s.getUser().getHome(args[0]);
            s.getUser().delHome(args[0]);
        }
        sendCMessage("Home.DelHome.del", args[0]
                , home.getLocation().getBlockX()
                , home.getLocation().getBlockY()
                , home.getLocation().getBlockZ ());
    }

    void sendHomes(OfflineUser t) {
        if(t.getName() == s.getName() && t.getHomes().size() == 0) {
            sendCMessage("Home.DelHome.no-Homes");
            return;
        }
        if(t.getName() != s.getName() && t.getHomes().size() == 0) {
            sendCMessage("Home.DelHome.no-Homes-Other", t.getName());
            return;
        }
        String e = Miniverse.getSettings().getString("Commands.Home.DelHome.getHomesListFormat");
        String h = Miniverse.getSettings().getString("Commands.Home.DelHome.getHomesTitle"),
                msg = h.endsWith("\n") ? h : h+"\n";
        Message homes = new Message(msg);
        for(int i = 0; i < t.getHomes().size(); i++) {
            String a = e.split("\\{0}")[1];
            if(i == t.getHomes().size()) {
                a = "[c].";
            }
            Home home = t.getHomes().get(i);
            homes.then(e.split("\\{0}")[0] + home.getName());
            homes.tooltip(Miniverse.format("Commands.Home.DelHome.getHomesHover",
                    home.getName(), home.getLocation().getBlockX()
                    , home.getLocation().getBlockY(), home.getLocation().getBlockZ()
            ));
            if(Miniverse.getSettings().getBoolean("Commands.Home.DelHome.runCommand")) {
                homes.command(Miniverse.format("Commands.Home.DelHome.getHomesCommand", home.getName()));
            }else {
                homes.suggest(Miniverse.format("Commands.Home.DelHome.getHomesCommand", home.getName()));
            }
            homes.then(a);
        }
        homes.send(s);
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        if(args.length == 1) {
            for(Home h : s.getUser().getHomes()) {
                if(h.getName().toLowerCase().startsWith(args[0].toLowerCase())) f.add(h.getName());
            }
        }
        return f;
    }


}
