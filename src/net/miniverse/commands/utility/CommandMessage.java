package net.miniverse.commands.utility;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.chat.ClickEvent;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("all")
public class CommandMessage extends MiniCommand{

    private HashMap<String, String> replies;

    public CommandMessage() {
        super("message", "msg", "tell", "r", "reply"
                , "mmessage", "mmsg", "mtell", "mr", "mreply", "w", "whisper"
                , "minecraft:message", "minecraft:msg", "minecraft:tell", "minecraft:r", "minecraft:reply", "minecraft:w", "minecraft:whisper");
        replies = new HashMap<>();
        desc = "Message someone!";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(cl.isIgnoreCase("r", "reply", "mr", "mtell", "mreply", "minecraft:r", "minecradt:reply")) {
            if(args.length > 0) {

                CommandSource r = s.getReplyTo();
                if(r == null || (r.isPlayer() && !r.getUser().getBase().isOnline())) {
                    sendCMessage("Invalidations.invalid-player");
                    return;
                }
                String msg = formMessage(0, args.length);
                String x = Miniverse.format("Commands.Message.Sender.Message", msg, r.getName(), s.getName());;
                Message message = new Message(x.contains(" "+msg) ? x.split(" "+msg)[0] : x.split(msg)[0]);
                if(Miniverse.getSettings().isSet("Commands.Message.Sender.hover"))
                    message.tooltip(Miniverse.format("Commands.Message.Sender.hover", msg, r.getName(), s.getName()));
                if(Miniverse.getSettings().isSet("Commands.Message.Sender.insert"))
                    message.suggest(StringUtils.stripColor(Miniverse.format("Commands.Message.Sender.insert", msg, r.getName(), s.getName())));
                message.then(x.contains(" " + msg) ? " " : "");
                message.then(true, msg);
                message.send(s);

                String rsp = Miniverse.format("Commands.Message.Target.Message", msg, r.getName(), s.getName());
                Message respond = new Message(rsp.contains(" " + msg) ? rsp.split(" " + msg)[0] : rsp.split(msg)[0]);
                if(Miniverse.getSettings().isSet("Commands.Message.Target.hover"))
                    respond.tooltip(Miniverse.format("Commands.Message.Target.hover", msg, r.getName(), s.getName()));
                if(Miniverse.getSettings().isSet("Commands.Message.Target.insert"))
                    respond.suggest(StringUtils.stripColor(Miniverse.format("Commands.Message.Target.insert", msg, r.getName(), s.getName())));
                respond.then(rsp.contains(" " + msg) ? " " : "");
                respond.then(true, msg);
                respond.send(r);
                return;
            }else {
                sendCMessage("Invalidations.more-arguments");
            }
            return;
        }
        if(args.length > 1) {
            CommandSource r = Miniverse.getSource(args[0]);

            if(r == null) {
                sendCMessage("Invalidations.invalid-player");
                return;
            }

            String msg = formMessage(1, args.length);
            String x = Miniverse.format("Commands.Message.Sender.Message", msg, r.getName(), s.getName());;
            Message message = new Message(x.contains(" "+msg) ? x.split(" "+msg)[0] : x.split(msg)[0]);
            if(Miniverse.getSettings().isSet("Commands.Message.Sender.hover"))
                message.tooltip(Miniverse.format("Commands.Message.Sender.hover", msg, r.getName(), s.getName()));
            if(Miniverse.getSettings().isSet("Commands.Message.Sender.insert"))
                message.suggest(StringUtils.stripColor(Miniverse.format("Commands.Message.Sender.insert", msg, r.getName(), s.getName())));
            message.then(x.contains(" " + msg) ? " " : "");
            message.then(true, msg);
            message.send(s);

            String rsp = Miniverse.format("Commands.Message.Target.Message", msg, r.getName(), s.getName());
            Message respond = new Message(rsp.contains(" " + msg) ? rsp.split(" " + msg)[0] : rsp.split(msg)[0]);
            if(Miniverse.getSettings().isSet("Commands.Message.Target.hover"))
                respond.tooltip(Miniverse.format("Commands.Message.Target.hover", msg, r.getName(), s.getName()));
            if(Miniverse.getSettings().isSet("Commands.Message.Target.insert"))
                respond.suggest(StringUtils.stripColor(Miniverse.format("Commands.Message.Target.insert", msg, r.getName(), s.getName())));
            respond.then(rsp.contains(" " + msg) ? " " : "");
            respond.then(true, msg);
            respond.send(r);
            s.setReplyTo(r);
            r.setReplyTo(s);
        }else {
            sendCMessage("Invalidations.more-arguments");
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> users = new ArrayList<>();
        for(User u : Miniverse.getOnlineUsers()) {
            if(!users.contains(u.getName())) {
                users.add(u.getName());
            }
        }
        if(!users.contains("Console")) {
            users.add("Console");
        }
        if(args.length >= 1) {
            for(String a : users) {
                if(a.toLowerCase().startsWith(args[args.length +-1].toLowerCase())) f.add(a);
            }
            return f;
        }

        return null;
    }
}
