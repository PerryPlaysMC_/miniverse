package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.inventory.Recipe;

public class CommandProfile extends MiniCommand implements Listener {

	public CommandProfile() {
		super("profile", "pro");
		desc = "Opens your profile page!";
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		s.getUser().getSuper().setInventory();
		s.getUser().getBase().openInventory(s.getUser().getProfileInven());
	}
	
	@EventHandler
	void onInvenClick(InventoryClickEvent e) {
		User u = Miniverse.getUser(e.getWhoClicked().getName());
		if(e.getSlot() <=-1)return;
		if(u != null) {
			u.onInventoryClick(e);
		}
	
	}

}
