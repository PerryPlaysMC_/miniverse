package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandAFK extends MiniCommand {
    public CommandAFK() {
        super("afk");
        setDescription("Toggle AFK");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer())return;
        User u = s.getUser();
        u.toggleAFK();
        (new BukkitRunnable(){
            boolean x = false;
            @Override
            public void run() {
                if(x) return;
                x = true;
                if(u.isAFK()) {
                    s.sendMessage("[c]You are " + (u.isAFK() ? "now AFK" : "no longer AFK"));
                }

            }
        }).runTaskLater(MiniverseCore.getAPI(), 1);
        u.startAFKTimer();
    }
}
