package net.miniverse.commands.utility;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.util.UUIDTypeAdapter;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.PlayerData;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.UUID;

public class CommandNickName extends MiniCommand {

	public CommandNickName() {
		super("nick","nickname", "mnick", "mnickname");
		this.desc = "Give yourself a nickname!";
		permission = "miniverse.command.nickname";
	}

	private HashMap<CommandSource, UUID> names = new HashMap<>();

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(sendInvalidPermission( "nickname")) {
			return;
		}
		if(args.length == 0) {
			s.sendMessage("[c]Not enough args");
			return;
		}
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("off")) {
				if(!s.getUser().getConfig().isSet("PreviousUUID") || s.getUser().getConfig().getString("PreviousUUID").isEmpty()) {
					s.sendMessage("[c]You need to have a nickname first.");
					return;
				}
				User u = s.getUser();
				s.sendMessage("[c]"+u.getOriginalName() + "-" + u.getBase().getName());
				GameProfile g;
				UUID prev;
				try {
					prev = UUID.fromString(u.getConfig().getString("PreviousUUID"));
					g = new GameProfile(prev, PlayerData.getName(prev));
					s.getUser().getConfig().set("PreviousUUID", "");
				} catch (Exception e) {
					e.printStackTrace();
					s.sendMessage("[c]Error");
					return;
				}
				setSkin(g, prev);
				u.setProfile(g);
				setName(u, g.getName());
				for(User us : Miniverse.getOnlineUsers()) {
					us.getBase().hidePlayer(MiniverseCore.getAPI(), u.getBase());
					us.getBase().showPlayer(MiniverseCore.getAPI(), u.getBase());
				}
				MiniverseCore.getAPI().loadPlayersAndPermissions();
				return;
			}
			if(!s.getUser().getConfig().isSet("PreviousUUID") || s.getUser().getConfig().getString("PreviousUUID").isEmpty()) {
				s.getUser().getConfig().set("PreviousUUID", s.getBase().getUniqueId().toString());
			}
			User u = s.getUser();
			OfflinePlayer of = Miniverse.getOfflinePlayer(args[0]);
			UUID ofID = (of == null ? u.getUUID() : of.getUniqueId());
			GameProfile g = new GameProfile(ofID, args[0]);
			setSkin(g, ofID);
			u.setProfile(g);
			setName(u, args[0]);
			for(User us : Miniverse.getOnlineUsers()) {
				us.getBase().hidePlayer(MiniverseCore.getAPI(), u.getBase());
				us.getBase().showPlayer(MiniverseCore.getAPI(), u.getBase());
			}
			MiniverseCore.getAPI().loadPlayersAndPermissions();
		}
		if(args.length >= 2) {
			if(args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("yes")) {
				User u = s.getUser();
				OfflinePlayer of = Miniverse.getOfflinePlayer(args[0]);
				UUID ofID = (of == null ? u.getUUID() : of.getUniqueId());
				GameProfile g = u.getCraftPlayer().getProfile();
				setSkin(g, ofID);
				u.setProfile(g);
				setName(u, args[0]);
				for(User us : Miniverse.getOnlineUsers()) {
					us.getBase().hidePlayer(MiniverseCore.getAPI(), u.getBase());
					us.getBase().showPlayer(MiniverseCore.getAPI(), u.getBase());
				}
			}
		}
	}

	void setName(User u, String n) {
		String name = ChatColor.translateAlternateColorCodes('&', n);
		u.getBase().setCustomName(name);
		u.getBase().setDisplayName(name);
		u.getBase().setPlayerListName(name);
		u.getBase().setCustomNameVisible(true);
	}

	public static boolean setSkin(GameProfile profile, UUID uuid) {
		try {
			HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false",
					UUIDTypeAdapter.fromUUID(uuid))).openConnection();
			if (connection!=null && connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
				String reply = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
				String skin = reply.split("\"value\":\"")[1].split("\"")[0];
				String signature = reply.split("\"signature\":\"")[1].split("\"")[0];
				profile.getProperties().put("textures", new Property("textures", skin, signature));
				return true;
			} else {
				System.out.println("Connection could not be opened (Response code " + connection.getResponseCode() + ", " + connection.getResponseMessage() + ")");
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
