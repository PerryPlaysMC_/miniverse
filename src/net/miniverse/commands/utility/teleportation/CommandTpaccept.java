package net.miniverse.commands.utility.teleportation;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandTpaccept extends MiniCommand {
    public CommandTpaccept() {
        super("tpaccept", "teleportaccept");
        desc = "Accept a pending teleport request!";
        permission = "miniverse.command.tpa.accept";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("tpa.accept"))return;
        if(mustBePlayer())return;
        if(s.getUser().getTpa() == null) {
            sendCMessage("Tpa.invalid-User");
            return;
        }
        User t = s.getUser().getTpa();
        sendCMessage("Tpa.accepted-sender", t.getName());
        sendCMessage(t, "Tpa.accepted-other", s.getName());
        t.teleport(s.getUser().getLocation());
        t.setTpa(null);
        s.getUser().setTpa(null);
    }
}
