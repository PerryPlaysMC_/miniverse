package net.miniverse.commands.utility.teleportation;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandTpaDeny extends MiniCommand {
    public CommandTpaDeny() {
        super("tpdeny", "tpadeny", "teleportdeny");
        desc = "Deny a pending teleport request!";
        permission = "miniverse.command.tpa.deny";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("tpa.accept"))return;
        if(mustBePlayer())return;
        if(s.getUser().getTpa() == null) {
            sendCMessage("Tpa.invalid-User");
            return;
        }
        User t = s.getUser().getTpa();
        sendCMessage("Tpa.denied-sender", t.getName());
        sendCMessage(t, "Tpa.denied-other", s.getName());
        t.setTpa(null);
        s.getUser().setTpa(null);
    }
}
