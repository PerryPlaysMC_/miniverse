package net.miniverse.commands.utility.teleportation;

import com.google.common.collect.Lists;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.util.exceptions.InvalidIntException;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.CommandUtil;
import net.miniverse.util.NumberUtil;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandTeleport extends MiniCommand {

    public CommandTeleport() {
        super(false,"teleport", "tp");
        desc = "Teleport to people or locations easily.";
        permission = "miniverse.command.teleport";
    }



    @Override@SuppressWarnings("all")
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission( "teleport", "tp")) return;
        if(args.length == 0) {
            sendMoreArguments();
            return;
        }
        User t = Miniverse.getUser(args[0]);

        User t2 = null;
        if(args.length > 1) {
            t2 = Miniverse.getUser(args[1]);
        }
        if(args.length == 1) {
            if(!s.isPlayer()) {
                s.sendMessage("[c]Specify another player");
                return;
            }
            if(args[0].startsWith("@")) {
                Entity[] e = CommandUtil.getTargets(s.getSender(), args[0]);
                for(Entity i : e) s.getUser().teleport(i.getLocation());
                return;
            }
            if(t == null) {
                sendInvalidTarget(args[0]);
                return;
            }
            if(t.getName() == s.getName()) {
                sendCMessage("Teleport.cancelled", t.getName());
                return;
            }
            if(Miniverse.getSettings().getBoolean("Commands.Teleport.send-target")) {
                t.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target", s.getName());
            }
            s.getUser().teleport(t.getLocation());
            sendCMessage("Teleport.sender", t.getName(), args[0]);
        }else if(args.length == 2) {


            if(args[0].startsWith("@")) {
                if(t2 == null) {
                    sendInvalidTarget(args[1]);
                    return;
                }
                Entity[] e = CommandUtil.getTargets(s.getSender(), args[0]);
                for(Entity i : e) i.teleport(t2.getLocation());
                if(t2.getName() == s.getName()) {
                }
                return;
            }

            if(t == null) {
                sendInvalidTarget(args[0]);
                return;
            }
            if(t2 == null) {
                sendInvalidTarget(args[1]);
                return;
            }
            boolean sendToSender = true;
            if(t2.getName() == s.getName() && t.getName() == s.getName()) {
                sendCMessage("Teleport.cancelled", t.getName(), t2.getName());
                return;
            }
            if(t.getName() == s.getName()) {
                if(Miniverse.getSettings().getBoolean("Commands.Teleport.send-target")) {
                    t2.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target", s.getName());
                }
                sendCMessage("Teleport.sender", t2.getName(), args[1]);
                sendToSender = false;
            }
            if(t2.getName() == s.getName()) {
                if(Miniverse.getSettings().getBoolean("Commands.Teleport.send-target")) {
                    t.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target", s.getName());
                }
                sendCMessage("Teleport.sender-twoTargets", t.getName(), "You");
                sendToSender = false;
            }
            if(t2.getName() == t.getName()) {
                sendCMessage("Teleport.cancelled", t.getName(), t2.getName());
                return;
            }
            if(Miniverse.getSettings().getBoolean("Commands.Teleport.send-targets")) {
                t.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target-1", s.getName(), t2.getName());
                t2.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target-2", s.getName(), t.getName());
            }
            t.teleport(t2.getLocation());
            if(sendToSender) {
                sendCMessage("Teleport.sender-twoTargets", t.getName(), t2.getName());
            }
        }else if(args.length == 3) {
            if(!s.isPlayer()) {
                s.sendMessage("[c]Specify a player");
                return;
            }
            Location loc = null;
            try {
                loc = retrieveLocation(s.getUser(), args[0], args[1], args[2]);
            } catch (InvalidIntException e) {
                sendCMessage("Invalidations.invalid-coord", e.getNumber());
                return;
            }
            DecimalFormat f = new DecimalFormat("0.000");
            String x = f.format(loc.getX()), y = f.format(loc.getY()), z = f.format(loc.getZ());
            s.getUser().teleport(loc);
            sendCMessage("Teleport.sender-coords",
                    loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(),
                    x, y, z,
                    loc.getX(), loc.getY(), loc.getZ());
        }else if(args.length == 4) {
            if(t == null) {
                sendInvalidTarget(args[0]);
                return;
            }
            if(t.getName() == s.getName()) {
                Location loc = null;
                try {
                    loc = retrieveLocation(s.getUser(), args[1], args[2], args[3]);
                } catch (InvalidIntException e) {
                    sendCMessage("Invalidations.invalid-coord", e.getNumber());
                    return;
                }
                DecimalFormat f = new DecimalFormat("0.000");
                String x = f.format(loc.getX()), y = f.format(loc.getY()), z = f.format(loc.getZ());
                s.getUser().teleport(loc);
                sendCMessage("Teleport.sender-coords",
                        loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(),
                        x, y, z,
                        loc.getX(), loc.getY(), loc.getZ());
                return;
            }
            Location loc = null;
            try {
                loc = retrieveLocation(t, args[1], args[2], args[3]);
            } catch (InvalidIntException e) {
                sendCMessage("Invalidations.invalid-coord", e.getNumber());
                return;
            }
            DecimalFormat f = new DecimalFormat("0.000");
            String x = f.format(loc.getX()), y = f.format(loc.getY()), z = f.format(loc.getZ());
            t.teleport(loc);
            sendCMessage("Teleport.sender-target-coords", t.getName(), 
                    loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(),
                    x, y, z,
                    loc.getX(), loc.getY(), loc.getZ());

            if(Miniverse.getSettings().getBoolean("Commands.Teleport.send-target")) {
                t.sendMessage(Miniverse.getSettings(), "Commands.Teleport.target-coords", s.getName(),
                        loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(),
                        x, y, z,
                        loc.getX(), loc.getY(), loc.getZ());
            }
        }
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> users = new ArrayList<>();
        List<String> f = Lists.newArrayList();
        if(s.isPlayer()) {
            User u = s.getUser();
            List<String> adds = Arrays.asList("@a", "@e", "@s", "~", "~ ~", "~ ~ ~");
            List<String> arguments = Lists.newArrayList();
            if(args.length <= 2) {
                for (String a : adds) {
                    arguments.add(a);
                }
            }
            for(User us : core.getOnlineUsers()) {
                if(us != null && !arguments.contains(us.getName())) {
                    arguments.add(us.getName());
                }
            }
            if(args.length == 1) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
                }
            }
            if(args.length == 2) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
                }
            }
            if(args.length == 3) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[2].toLowerCase())) f.add(a);
                }
            }
            if(args.length == 4) {
                for(String a : arguments) {
                    if(a.toLowerCase().startsWith(args[3].toLowerCase())) f.add(a);
                }
            }
            return f;
        }
        return super.onTab(s, cl, args);
    }
    
    Location retrieveLocation(User u, String strX, String strY, String strZ) throws InvalidIntException {
        double x, y, z;
        if(strX.contains("~") && !strX.equalsIgnoreCase("~")) {
            if(NumberUtil.isDouble(strX.split("~")[1])) {
                x = u.getX() + NumberUtil.getDouble(strX.split("~")[1]);
            }else {
                x = u.getLocation().getX();
            }
        }else if(!strX.contains("~")) {
            if(NumberUtil.isDouble(strX)) {
                x = NumberUtil.getDouble(strX);
            }else {
                throw new InvalidIntException(strX);
            }
        }else {
            x = u.getLocation().getX();
        }

        if(strY.contains("~") && !strY.equalsIgnoreCase("~")) {
            if(NumberUtil.isDouble(strY.split("~")[1])) {
                y = u.getY() + NumberUtil.getDouble(strY.split("~")[1]);
            }else {
                y = u.getLocation().getY();
            }
        }else if(!strY.contains("~")) {
            if(NumberUtil.isDouble(strY)) {
                y = NumberUtil.getDouble(strY);
            }else {
                throw new InvalidIntException(strY);
            }
        }else {
            y = u.getLocation().getY();
        }

        if(strZ.contains("~") && !strZ.equalsIgnoreCase("~")) {
            if(NumberUtil.isDouble(strZ.split("~")[1])) {
                z = u.getZ() + NumberUtil.getDouble(strZ.split("~")[1]);
            }else {
                z = u.getLocation().getZ();
            }
        }else if(!strZ.contains("~")) {
            if(NumberUtil.isDouble(strZ)) {
                z = NumberUtil.getDouble(strZ);
            }else {
                throw new InvalidIntException(strZ);
            }
        }else {
            z = u.getLocation().getZ();
        }

       return new Location(u.getLocation().getWorld(), x, y, z, u.getLocation().getYaw(), u.getLocation().getPitch());
    }

}
