package net.miniverse.commands.utility.teleportation;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.util.NumberUtil;
import org.bukkit.Location;

public class CommandWild extends MiniCommand {
    public CommandWild() {
        super("wild");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer())return;
        int x = s.getUser().getX()+ NumberUtil.newRandomInt(1500);
        int z = s.getUser().getX()+ NumberUtil.newRandomInt(1500);
        int y = s.getUser().getLocation().getWorld().getHighestBlockYAt(x,z)+3;

        s.getUser().teleport(new Location(s.getUser().getLocation().getWorld(), x, y,z, s.getUser().getLocation().getYaw(), s.getUser().getLocation().getPitch()));
        s.sendMessage("[c]Teleported to X:[pc]" + x + "[c] Y:[pc]" + y + "[c] Z:[pc]" + z);
    }
}
