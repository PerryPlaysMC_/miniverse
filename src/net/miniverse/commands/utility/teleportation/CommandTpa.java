package net.miniverse.commands.utility.teleportation;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.TimeUtil;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandTpa extends MiniCommand {
    public CommandTpa() {
        super("tpa", "teleporta");
        desc = "Send a teleport request to someone!";
        permission = "miniverse.command.tpa.send";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(sendInvalidPermission("tpa.send"))return;
        if(mustBePlayer())return;
        if(args.length == 0) { sendSpecifyTarget();return; }
        if(args.length>1) { sendTooManyArguments();return; }
        User t = Miniverse.getUser(args[0]);
        if(validTarget(args[0], true)) return;
        t.setTpa(s.getUser());
        sendCMessage("Tpa.sendSource", args[0], TimeUtil.format(Miniverse.getSettings().getInt("Commands.Tpa.timeout"), true));
        sendCMessage(t, "Tpa.sendTarget", s.getName(), TimeUtil.format(Miniverse.getSettings().getInt("Commands.Tpa.timeout"), true));
        runLater(() -> {
            if(t.getTpa()!=null) {
                t.setTpa(null);
                s.sendMessage("[c]Tpa timed out, took too long");
            }
        }, 20 * Miniverse.getSettings().getInt("Commands.Tpa.timeout"));
    }
}
