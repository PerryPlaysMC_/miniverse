package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;

import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CommandRename extends MiniCommand {

	public CommandRename() {
		super("rename", "mrename");
		this.desc = "Rename any item easily";
		permission = "miniverse.command.itemrename";
	}

	public CommandRename(String name, String... aliases) {
		super(name, aliases);
	}

	@Override
	public void run(CommandSource s, Label cl, String[] args) {
		if(mustBePlayer()) return;
		User u = s.getUser();
		if(sendInvalidPermission("itemrename")) return;
		if(u.getItemInHand().getType() == Material.AIR || u.getItemInHand() == null) {
			s.sendMessage("[c]Invalid item");
			return;
		}

		if(args.length == 1) {
			s.sendMessage("[c]More arguments");
			return;
		}

		if(args.length >= 2) {
			if(args[0].equalsIgnoreCase("lore")) {
				String lore = formMessage(1, args.length);
				List<String> lorelist = new ArrayList<>();
				String back = "\\-";
				if(lore.contains(back)) {
					for (String l : lore.split(back)) {
						lorelist.add(StringUtils.translate(l));
					}
				}else{
					lorelist.add(StringUtils.translate(lore));
				}
				ItemStack i = u.getItemInHand();
				ItemMeta im = i.getItemMeta();
				im.setLore(lorelist);
				i.setAmount(i.getAmount());
				im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				i.setItemMeta(im);
				StringBuilder sb = new StringBuilder();
				for(String a : lorelist) {
					sb.append("[c]- [pc]" + a + "\n");
				}
				String msg = sb.toString().trim();
				s.sendMessage("[c]Lore set to: \n" + msg);
			}else if(args[0].equalsIgnoreCase("name")) {
				String lore = formMessage(1, args.length);
				ItemStack i = u.getItemInHand();
				ItemMeta im = i.getItemMeta();
				i.setAmount(i.getAmount());
				im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				String msg = lore;
				im.setDisplayName(StringUtils.translate(msg));
				i.setItemMeta(im);
				s.sendMessage("[c]Name set to: " + msg);
			}
			return;
		}


	}

}
