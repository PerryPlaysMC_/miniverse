package net.miniverse.commands.utility;

import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.Poll;
import net.miniverse.util.PollManager;
import net.miniverse.util.TimeUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandPoll extends MiniCommand {
    public CommandPoll() {
        super("poll");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(args.length == 0) {
            sendMoreArguments();
            return;
        }
        if(args.length == 1) {
            sendHelp(s);
            return;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("end")) {
                if(sendInvalidPermission("poll.end")) return;
                Poll p = PollManager.getPoll(args[1]);
                if(p == null) {
                    s.sendMessage("[c]Invalid poll");
                    return;
                }
                p.setTime(1);
                s.sendMessage("[c]Ending poll '[pc]"+PollManager.getPoll(args[1]).getName() + "[c]'");
                return;
            }
            sendHelp(s);
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("vote")) {
                Poll p = PollManager.getPoll(args[1]);
                if(p == null) {
                    s.sendMessage("[c]Invalid poll");
                    return;
                }
                if(!p.hasTopic(args[2])) {
                    s.sendMessage("[c]Invalid topic");
                    return;
                }
                p.vote(s.getUser(), args[2]);
                s.sendMessage("[c]Voted '[pc]"+args[2]+"[c]' for poll '[pc]"+p.getName()+"[c]'");
                return;
            }
            if(args[0].equalsIgnoreCase("settime")) {
                if(sendInvalidPermission("poll.settime")) return;
                Poll p = PollManager.getPoll(args[1]);
                if(p == null) {
                    s.sendMessage("[c]Invalid poll");
                    return;
                }
                if(!NumberUtil.isLong(args[2])) {
                    s.sendMessage("[c]Invalid number");
                    return;
                }
                long time = NumberUtil.getLong(args[2]);
                p.setTime(time);
                s.sendMessage("[c]Time set to '[pc]"+TimeUtil.getFormatedTime(time, true)+"[c]' for poll '[pc]"+p.getName()+"[c]'");
                return;
            }
            if(args[0].equalsIgnoreCase("create")) {
                if(sendInvalidPermission("poll.create")) return;
                s.sendMessage("[c]More topics");
                return;
            }
            sendHelp(s);
        }
        if(args.length >3) {
            if(args[0].equalsIgnoreCase("create")) {
                if(sendInvalidPermission("poll.create")) return;
                if(PollManager.getPoll(args[1])!=null){
                    s.sendMessage("[c]That poll already exists");
                    return;
                }
                List<String> topics = new ArrayList<>();
                for(int i = 2; i < args.length;i++) {
                    topics.add(args[i]);

                }
                PollManager.addPoll(new Poll(args[1], 300, topics));

                Message msg = new Message("[c]Poll '[pc]" + args[1] + "[c]' Active! (" + TimeUtil.getFormatedTime(300, true) + ")");

                for(String a : topics) {
                    msg.then("\n &b-").then("&3"+a).tooltip("&3Click to vote for &b" + a).command("/poll vote " + args[1] + " " + a);
                }
                for(User u : Miniverse.getOnlineUsers()) msg.send(u);
                msg.send(Miniverse.getConsole());

                return;
            }
            sendHelp(s);
        }
    }

    void sendHelp(CommandSource s) {
        s.sendMessage("[c]Help Menu:");
        if(s.hasPermission("poll.create")) s.sendMessage("&b -/poll [a]create [r]<name> [r]<topics>");
        if(s.hasPermission("poll.settime")) s.sendMessage("&b -/poll [a]settime [r]<name> [r]<time>");
        s.sendMessage("&b -/poll [a]vote [r]<name> [r]<topic>");
        if(s.hasPermission("poll.end")) s.sendMessage("&b -/poll [a]end [r]<name>");
        s.sendMessage("&b -/poll [a]help");
    }


    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> polls = Lists.newArrayList();

        for(Poll poll : PollManager.getActivePolls()) polls.add(poll.getName());
        List<String> args1 = Lists.newArrayList();
        if(s.hasPermission("poll.create")) args1.add("create");
        if(s.hasPermission("poll.end")) args1.add("end");
        if(s.hasPermission("poll.settime")) args1.add("settime");
        args1.add("vote");
        args1.add("help");
        if(args.length == 1) {
            for(String a : args1) if(a.toLowerCase().startsWith(args[0].toLowerCase())) f.add(a);
            return f;
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("help")) {
                return f;
            }
            if(!args1.contains(args[0]))return f;
            for(String a : polls) if(a.toLowerCase().startsWith(args[1].toLowerCase())) f.add(a);
            return f;
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("vote")) {
                Poll p = PollManager.getPoll(args[1]);
                if(p==null||p.getTopics()==null) {
                    s.sendMessage("[c]Invalid poll");
                    return f;
                }
                for(String a : p.getTopics()) {
                    if(a.toLowerCase().startsWith(args[2].toLowerCase())) f.add(a);
                }
                return f;
            }
            return f;
        }
        return f;
    }
}
