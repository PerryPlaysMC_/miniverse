package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.permissions.GroupManager;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandList extends MiniCommand {
    public CommandList() {
        super("list", "who");
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        HashMap<Group, List<String>> users = new HashMap<>();
        for(Group g : GroupManager.getSorted()) {
            for(User u : Miniverse.getOnlineUsers()) {
                if(u.getGroups().contains(g)) {
                    List<String> x = (users.get(g) != null ? users.get(g) : new ArrayList<>());
                    x.add(u.getBase().getDisplayName());
                    users.put(g, x);
                }
            }
        }
        for(Map.Entry<Group, List<String>> g : users.entrySet()) {
            s.sendMessage(
                    "[c]" + g.getKey().getName()
                    + "(" + g.getValue().size() + "): \n- " + StringUtils.formatList(true, true, g.getValue()));
        }
    }
}
