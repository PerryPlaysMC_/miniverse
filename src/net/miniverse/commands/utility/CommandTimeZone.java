package net.miniverse.commands.utility;

import com.google.common.collect.Lists;
import net.miniverse.chat.Message;
import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;

import java.util.*;

public class CommandTimeZone extends MiniCommand {
    HashMap<String, String> map = (new HashMap<>());
    public CommandTimeZone() {
        super("timezone");
        map.put("ACT", "Australia/Darwin");
        map.put("AET", "Australia/Sydney");
        map.put("AGT", "America/Argentina/Buenos_Aires");
        map.put("ART", "Africa/Cairo");
        map.put("AST", "America/Anchorage");
        map.put("BET", "America/Sao_Paulo");
        map.put("BST", "Asia/Dhaka");
        map.put("CAT", "Africa/Harare");
        map.put("CNT", "America/St_Johns");
        map.put("CST", "America/Chicago");
        map.put("CTT", "Asia/Shanghai");
        map.put("EAT", "Africa/Addis_Ababa");
        map.put("ECT", "Europe/Paris");
        map.put("IET", "America/Indiana/Indianapolis");
        map.put("IST", "Asia/Kolkata");
        map.put("JST", "Asia/Tokyo");
        map.put("MIT", "Pacific/Apia");
        map.put("NET", "Asia/Yerevan");
        map.put("NST", "Pacific/Auckland");
        map.put("PLT", "Asia/Karachi");
        map.put("PNT", "America/Phoenix");
        map.put("PRT", "America/Puerto_Rico");
        map.put("PST", "America/Los_Angeles");
        map.put("SST", "Pacific/Guadalcanal");
        map.put("VST", "Asia/Ho_Chi_Minh");
        map.put("EST", "-05:00");
        map.put("MST", "-07:00");
        map.put("HST", "-10:00");
    }

    String x(String f) {
        String r = "";
        for(int i = 0; i<f.split("/").length;i++) {
            String x = f.split("/")[i];
            if(x.contains("_")) {
                r+=(x.charAt(0)+"").toUpperCase()
                   +x.split("_")[0].toLowerCase().replaceFirst(x.charAt(0)+"", (x.charAt(0)+"").toUpperCase())
                   +x.split("_")[1].toLowerCase().replaceFirst(x.split("_")[1].charAt(0)+"", (x.split("_")[1].charAt(0)+"").toUpperCase())
                   +(i == f.split("/").length+-1?"":"/");
                continue;
            }
            r+=(x.charAt(0)+"").toUpperCase()+x.substring(1).toLowerCase()+(i == f.split("/").length+-1?"":"/");
        }
        return r;
    }
    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer())return;
        if(args.length == 0) {
            s.sendMessage("[c]Your TimeZone is: [pc]" + s.getUser().getConfig().getString("Settings.TimeZone"));
            return;
        }
        if(args.length == 1) {
            sendUsage();
            return;
        }
        if(args.length == 2) {
            if(!args[0].equalsIgnoreCase("set")) {
                sendUsage();
                return;
            }
            if(!map.containsKey(args[1].toUpperCase()) && !map.containsValue(x(args[1]))) {
                s.sendMessage("[c]Invalid timezone");
                return;
            }
            s.getUser().getConfig().set("Settings.TimeZone", args[1].toUpperCase());
            s.sendMessage("[c]Your TimeZone has been set to '[pc]" + args[1] + "[c]'");
            return;
        }
        sendUsage();
    }

    @Override
    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        List<String> f = Lists.newArrayList();
        List<String> x = Lists.newArrayList();
        x.addAll(map.keySet());
        if(args.length == 1) {
            if("set".startsWith(args[0].toLowerCase()))f.add("set");
            return f;
        }
        else if(args.length == 2) {
            for(String z : x) {
                if(z.toLowerCase().startsWith(args[1].toLowerCase()))f.add(z);
            }
        }else if(args.length > 2)f.add("");
        return f;
    }

    void sendUsage() {
        Message msg = new Message(s, "[c]&m&l       &r[pc]TimeZone SubCommands[c]&m&l      ");
        msg.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional" +
                 " \n [r]<argument>[c] &7= Required" +
                 "\n");


        msg.then("\n[c]-/[pc]timezone[c] [r]set <timezone>")
                .tooltip("[c]Info: [pc]BanData a player from the server!",
                        "[c]Click to insert:",
                        "[pc]/timezone [r]set <timezone>")
                .suggest("/timezone set <timezone>");
        msg.send(s);
    }

}
