package net.miniverse.commands.utility;

import net.miniverse.command.Label;
import net.miniverse.command.MiniCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import net.miniverse.user.User;

public class CommandPowerTool extends MiniCommand {
    public CommandPowerTool() {
        super("powertool", "pt", "powert", "ptool");
        desc = "Assign a command to yhe item in your hand.";
        permission = "miniverse.command.powertool";
    }

    @Override
    public void run(CommandSource s, Label cl, String[] args) {
        if(mustBePlayer()) return;
        if(sendInvalidPermission("powertool"))return;
        User u = s.getUser();
        if(u.getItemInHand() == null || u.getItemInHand().getType().name().contains("AIR")) {
            s.sendMessage("[c]Invalid item");
            return;
        }
        if(args.length == 0) {
            if(u.isSet(u.getItemInHand().getType())) {
                s.sendMessage("[c]Removing PowerTool '[pc]/" + u.getCommand(u.getItemInHand().getType()) + "[c]'");
                u.removeTool(u.getItemInHand().getType());
                return;
            }
            s.sendMessage("[c]Please specify Command");
            return;
        }
        if(Miniverse.parseCommand(args[0]) == null && Miniverse.parseCmdFull(args[0]) == null) {
            s.sendMessage("[c]Invalid command");
            return;
        }
        u.setCommand(u.getItemInHand().getType(), formMessage(0, args.length).replaceFirst("/",""));
        s.sendMessage("[c]Setting PowerTool '[pc]/" + formMessage(0, args.length).replaceFirst("/","") + "[c]'");
    }
}
